package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.OrderCreateCallBack;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.ResponseModel;


public class OrderCreatingApiHelper extends AsyncTask<Void,String, ResponseModel> {
    OrderCreateCallBack callBack;
    private ResponseModel response = new ResponseModel(0,false,"Failed to connect ! Please check your internet connection and try again");
    private Context context;
    private OrderModel orderModel;

    public OrderCreatingApiHelper(Context context,OrderCreateCallBack callBack,OrderModel orderModel) {
        this.callBack = callBack;
        this.context = context;
        this.orderModel = orderModel;
    }

    @Override
    protected ResponseModel doInBackground(Void... voids) {
        ResponseModel response = null;
        if (orderModel.serverId > 0) {
            response = new OrderApi(context).updateOrder(orderModel);
        } else {
            response = new OrderApi(context).createOrder(orderModel);
        }
        this.response = response;
        return  response;

    }

    @Override
    protected void onPostExecute(ResponseModel response) {
        super.onPostExecute(response);
        callBack.onOrderCreate(response);
    }
}
