package com.novo.tech.redmangopos.callback;


import com.novo.tech.redmangopos.model.ResponseModel;

public interface OrderCreateCallBack{
    void onOrderCreate(ResponseModel response);
}
