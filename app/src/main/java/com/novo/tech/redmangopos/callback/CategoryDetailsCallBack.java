package com.novo.tech.redmangopos.callback;


import com.novo.tech.redmangopos.model.CategoryDetailsModel;

import java.util.ArrayList;
/**
 * This class created by alamin
 */
public interface CategoryDetailsCallBack {
    void onCategoryDetailsResponse(ArrayList<CategoryDetailsModel> categoryDetailsModelArrayList);
}
