package com.novo.tech.redmangopos.model;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CartItem implements Serializable {

    public String uuid, description, type, shortName;
    public List<ComponentSection> componentSections;
    public List<CartItem> extra;
    public double price,offerPrice;
    public double subTotal, total;
    public int quantity, quantityOrg, printOrder,localId;
    public String comment,category;
    public boolean discountable,offerAble,offered;
    public String kitchenItem;
    public boolean isLocalItem;
    public String paymentMethod;

    public CartItem(int localId,String uuid, String description, String shortName, @Nullable List<ComponentSection> componentSections, double price,double offerPrice, @Nullable double subTotal, @Nullable double total, int quantityOrg, int quantity,String comment,String type,int printOrder,boolean discountable,boolean offerAble,boolean offered, String kitchenItem,String category,List<CartItem> extra, boolean isLocalItem, String paymentMethod) {
        this.localId = localId;
        this.uuid = uuid;
        this.description = description;
        this.shortName = shortName;
        this.componentSections = componentSections;
        this.price = price;
        this.offerPrice = offerPrice;
        this.subTotal = subTotal;
        this.total = total;
        this.quantityOrg = quantityOrg;
        this.quantity = quantity;
        this.comment = comment;
        this.type = type;
        this.printOrder = printOrder;
        this.discountable = discountable;
        this.offerAble = offerAble;
        this.offered = offered;
        this.kitchenItem = kitchenItem;
        this.category = category;
        this.extra = extra;
        this.isLocalItem = isLocalItem;
        this.paymentMethod = paymentMethod;
    }

    public static CartItem fromJSON(JSONObject data) {
        List<ComponentSection> componentSections = new ArrayList<>();
        List<CartItem> extra = new ArrayList<>();

        if (data.optJSONArray("components") != null) {
            JSONArray jsonArray = data.optJSONArray("components");
            for (int i = 0; i < jsonArray.length(); i++) {
                componentSections.add(ComponentSection.fromJSON(jsonArray.optJSONObject(i)));
            }
        }

        if (data.optJSONArray("extra") != null) {
            JSONArray jsonArray = data.optJSONArray("extra");
            for (int i = 0; i < jsonArray.length(); i++) {
                extra.add(CartItem.fromJSON(jsonArray.optJSONObject(i)));
            }
        }

        return new CartItem(
                data.optInt("localId",0),
                data.optString("product_uuid"),
                data.optString("description"),
                data.optString("short_name"),
                componentSections,
                data.optDouble("price"),
                data.optDouble("offerPrice"),
                data.optDouble("subTotal"),
                data.optDouble("Total"),
                data.optInt("quantity"),
                data.optInt("quantity"),
                data.optString("comment"),
                data.optString("product_type",""),
                data.optInt("print_order",0),
                data.optBoolean("discountable",false),
                data.optBoolean("offerAble",false),
                data.optBoolean("offered",false),
                data.optString("kitchen_item","0"),
                data.optString("category",""),
                extra,
                true,
                "CASH"
        );
    }


    public static CartItem fromOnlineJSON(JSONObject data, String paymentMethod) {
        String category = "";
        String itemName = "";
        String itemDescription = "";
        String productType = data.optString("product_type","");
        double _subTotal = 0.0;
        int printOrder = 0;
        double _total = data.optDouble("net_amount",0);
        List<ComponentSection> componentSections = new ArrayList<>();

        if (data.optJSONArray("items") != null) {
            JSONArray jsonArray = data.optJSONArray("items");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.optJSONObject(i);
                if(obj!=null){
                    componentSections.add(new ComponentSection(
                            obj.optString("relation_group"),
                            obj.optString("relation_group"),
                            new ArrayList<>(),
                            Component.fromJSON(obj.optJSONObject("product"),obj)
                    ));
                }

            }
        }

        for (ComponentSection section : componentSections){
            _subTotal+=section.selectedItem.subTotal;
        }
        _subTotal = _total;
        double price = 0;
        double offerPrice = 0;
        String kitchenItem = "0";
        JSONObject object = data.optJSONObject("product");
        if(object!=null){
            if(object.optJSONObject("price") != null){
                price = object.optJSONObject("price").optDouble("price");
            }
        }
        try {
            if (object != null) {
                if (object.optJSONObject("properties") != null) {
                    printOrder = object.optJSONObject("properties").optInt("print_order", 0);
                    category = object.optJSONObject("properties").optString("categories_epos", "");
                    offerPrice = object.optJSONObject("properties").optDouble("offer_price", 0.0);
                    String kitchenItemStr = object.optJSONObject("properties").optString("kitchen_item", "0");
                    if (kitchenItemStr.equals("1")) {
                        kitchenItem = kitchenItemStr;
                    }
                }
            }
            if(productType.equals("DYNAMIC")){
                itemName = data.optString("product_short_name","N/A");
                itemDescription = itemName;

            }else {
                itemDescription = Objects.requireNonNull(data.optJSONObject("product")).optString("description");
                itemName = Objects.requireNonNull(data.optJSONObject("product")).optString("short_name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return new CartItem(
                data.optInt("order_item_id"),
                data.optString("product_uuid"),
                itemDescription,
                itemName,
                componentSections,
                price,
                offerPrice,
                _subTotal/data.optInt("units",1),
                _total,
                data.optInt("units"),
                data.optInt("units"),
                data.optString("comment"),
                data.optString("product_type",""),
                printOrder,
                data.optBoolean("discountable",false),
                false,false, kitchenItem,
                category,new ArrayList<>(), false,
                paymentMethod
        );
    }
    public static JSONObject toJSONCartItem(Product itemModel,@Nullable List<ComponentSection> sec,int quantity,double subTotal,boolean discountable,boolean offerAble,boolean offered,String category) {
        JSONObject jsonObject = new JSONObject();
        JSONArray _sectionList =new JSONArray();
        int print_order = 0;
        List<ComponentSection> _sections = new ArrayList<>();
        List<Component> componentArrayList = itemModel.componentList;
        try {
            for (Component component : componentArrayList) {
                ComponentSection group = null;
                for (ComponentSection section : _sections) {
                    if (section.sectionValue.equals(component.relationGroup)) {
                        group = section;
                    }
                }
                if (group != null) {
                    group.components.add(component);
                } else {
                    group = new ComponentSection(component.relationGroup, component.relationGroup, new ArrayList<>(), null);
                    group.components.add(component);
                    _sections.add(group);
                }

            }
            for (ComponentSection section : _sections) {
                section.selectedItem = section.components.get(0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if(itemModel.properties != null){
            print_order = itemModel.properties.print_order;
        }

        try {
            jsonObject.putOpt("product_uuid", itemModel.productUUid);
            jsonObject.putOpt("short_name", itemModel.shortName);
            jsonObject.putOpt("description", itemModel.description);
            jsonObject.putOpt("price", itemModel.price);
            jsonObject.putOpt("offerPrice", itemModel.offerPrice);
            jsonObject.putOpt("subTotal", subTotal);
            jsonObject.putOpt("Total", subTotal);
            jsonObject.putOpt("quantity", quantity);
            jsonObject.putOpt("components", _sectionList);
            jsonObject.putOpt("product_type", itemModel.productType);
            jsonObject.putOpt("print_order", print_order);
            jsonObject.putOpt("localId", 0);
            jsonObject.putOpt("discountableTotal", discountable);
            jsonObject.putOpt("discountable", itemModel.discountable);
            jsonObject.putOpt("offerAble", offerAble);
            jsonObject.putOpt("offered", offered);
            jsonObject.putOpt("category", category);
            jsonObject.putOpt("extra", new ArrayList<>());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}

