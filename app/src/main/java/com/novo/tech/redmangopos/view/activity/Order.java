package com.novo.tech.redmangopos.view.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.OrderListAdapter;


import com.novo.tech.redmangopos.callback.NetworkCallBack;


import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.receivers.NetworkChangeReceiver;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.socket.JsonToOrderModel;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.socket.emitter.Emitter;

public class Order extends AppCompatActivity implements  NetworkCallBack {
    RecyclerView recyclerView;
    OrderListAdapter adapter;
    List<OrderModel> allOrders = new ArrayList<>();
    TextView noDataFound,dashBoardType,noInternet;
    LinearLayout backButton;

    ProgressDialog progress;
    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver mNetworkReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        recyclerView = findViewById(R.id.dashboardOrderRecyclerView);
        noDataFound = findViewById(R.id.dashboardNotDataFound);
        dashBoardType = findViewById(R.id.dashboardOrderType);
        noInternet = findViewById(R.id.noInternet);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        backButton = findViewById(R.id.backButton);

        /**set recyclerview style "a"*/
        recyclerView.setAlpha(0f);
        recyclerView.setTranslationY(50);
        recyclerView.animate().alpha(1f).translationYBy(-50).setDuration(700);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put(SocketHandler.ORDER_LIST, new JSONArray());
        } catch (Exception e) {
            e.printStackTrace();
        }

        BaseApp.webSocket.off(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business);
        BaseApp.webSocket.on(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject jsonObject = (JSONObject) args[0];
                Log.e("orderList==>", jsonObject.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            JSONArray jsonArray = (JSONArray) jsonObject.get(SocketHandler.ORDER_LIST);
                            if (jsonArray.length() > 0) {
                                allOrders.clear();
                                for (int i=0;i<jsonArray.length();i++) {
                                    allOrders.add(JsonToOrderModel.toModel(jsonArray.getJSONObject(i)));
                                }
                            }
                            BaseApp.orderList = allOrders;
                            displayOrderList();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        findViewById(R.id.socketConStatus).setVisibility(View.VISIBLE);
                    }
                });
            }
        });


        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();
    }

    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case "INTERNET_STATE_CHANGE":
                        boolean connected = intent.getBooleanExtra("state", false);
                        if (connected) {
                            noInternet.setVisibility(View.GONE);
                        } else {
                            noInternet.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("INTERNET_STATE_CHANGE"));
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        displayOrderList();
    }


    public boolean orderExist(List<OrderModel> orders, int dbId) {
        boolean exist = false;
        for (OrderModel model: orders) {
            if (model.db_id == dbId) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    public List<OrderModel> getOrdersByBookingId(int bookingId) {
        List<OrderModel> list = new ArrayList<>();
        for (OrderModel model: allOrders) {
            if (model.bookingId != 0 && model.bookingId == bookingId) {
                list.add(model);
            }
        }
        return list;
    }

    public void displayOrderList(){
        Log.e("AllOrders==>", allOrders.size() + "");
//        List<OrderModel> orders = new ArrayList<>();
//        List<OrderModel> tableOrders = new ArrayList<>();
//        for (OrderModel orderModel: allOrders) {
//            if (orderModel.bookingId != 0) {
//                List<OrderModel> ordersByBookingId = getOrdersByBookingId(orderModel.bookingId);
//                Log.e("bookingOrders==>", ordersByBookingId.size() + "");
//                boolean isLocalExist = false;
//                for (OrderModel model: ordersByBookingId) {
//                    if (!model.order_channel.equals("ONLINE")) {
//                        boolean exist = orderExist(tableOrders, model.db_id);
//                        if (!exist) {
//                            tableOrders.add(model);
//                        }
//                        isLocalExist = true;
//                        break;
//                    }
//                }
//                if (!isLocalExist && ordersByBookingId.size() > 0) {
//                    OrderModel orderData = null;
//                    for (OrderModel model: ordersByBookingId) {
//                        if (model.paymentMethod.equals("CASH")) {
//                            orderData = model;
//                            break;
//                        }
//                    }
//                    if (orderData != null) {
//                        boolean exist = orderExist(tableOrders, orderData.db_id);
//                        if (!exist) {
//                            tableOrders.add(orderData);
//                        }
//                    } else {
//                        boolean exist = orderExist(tableOrders, ordersByBookingId.get(0).db_id);
//                        if (!exist) {
//                            tableOrders.add(ordersByBookingId.get(0));
//                        }
//                    }
//                }
//            } else {
//                orders.add(orderModel);
//            }
//        }
//        orders.addAll(tableOrders);
//        Log.e("AllOrders==>", allOrders.size() + "");
//        Log.e("MergedOrders==>", orders.size() + "");

//        DBOrderManager orderManager = new DBOrderManager(getApplicationContext());
//        allOrders.addAll(orderManager.getActiveOrder());

//        allOrders.sort((a,b)-> Integer.compare(b.order_id,a.order_id));

        allOrders.sort((a,b)->{
            long aDate  = Long.parseLong(a.orderDate.length()>12? DateTimeHelper.formatOnlyDBDate(a.orderDate):a.orderDate.replaceAll("/","").replaceAll("-",""));
            long bDate  = Long.parseLong(b.orderDate.length()>12?DateTimeHelper.formatOnlyDBDate(b.orderDate):b.orderDate.replaceAll("/","").replaceAll("-",""));
            if(aDate == bDate){
                return Integer.compare(b.order_id,a.order_id);
            }else{
                return Long.compare(bDate,aDate);
            }
        });

        adapter = new OrderListAdapter(Order.this, allOrders);
        recyclerView.setAdapter(adapter);

        if(allOrders.size()==0){
            noDataFound.setVisibility(View.VISIBLE);
        }else{
            noDataFound.setVisibility(View.GONE);
        }
    }

    void openOrderPage(String orderType){
        Intent in=new Intent(Order.this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType",orderType);
        in.putExtras(b);
        startActivity(in);
    }
    public void openOrderPage(OrderModel orderModel){
        Intent in=new Intent(Order.this,OrderCreate.class);
        Bundle b = new Bundle();
        b.putSerializable("orderModel", orderModel);
        in.putExtras(b);
        startActivity(in);
    }

    public void openOnlineOrderDetailsPage(OrderModel orderModel){
        Intent in=new Intent(Order.this, OnlineCustomerOrderDetails.class);
        Bundle b = new Bundle();
        b.putSerializable("orders", (Serializable) BaseApp.orderList);
        b.putSerializable("orderData",orderModel);
        in.putExtras(b);
        startActivity(in);
    }
//    void checkDataOnline(){
//        ProgressDialog progress = new ProgressDialog(this);
//        progress.setMessage("Loading");
//        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
//        progress.show();
//
//
//        OrderChecker sync = new OrderChecker(Order.this, new OrderCallBack() {
//            @Override
//            public void onOrderResponse(List<OrderModel> orderList) {
//                progress.dismiss();
//                if(orderList==null)
//                    orderList = new ArrayList<>();
//                allOrders = orderList;
//                displayOrderList();
//            }
//        });
//        sync.execute();
//
//    }

    @Override
    public void onNetworkStateChange(boolean connected) {
        Log.d("mmmmm", "onNetworkStateChange: "+connected);
    }
}