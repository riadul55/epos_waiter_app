package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.ComponentSection;

import java.text.DecimalFormat;
import java.util.List;

public class SubCartListAdapter extends RecyclerView.Adapter<SubCartListAdapter.OrderListViewHolder> {
    Context mCtx;
    List<ComponentSection> componentSections;
    double total = 0;

    public SubCartListAdapter(Context mCtx, List<ComponentSection> componentSections) {
        this.mCtx = mCtx;
        this.componentSections = componentSections;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_cart_component, parent, false);
        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        String componentName = componentSections.get(position).selectedItem.shortName;

        if(componentSections.get(position).selectedItem.subComponentSelected!= null){
            componentName+="\n"+componentSections.get(position).selectedItem.subComponentSelected.shortName;
        }
        Animation animation;
        animation = AnimationUtils.loadAnimation(mCtx, R.anim.move);
        holder.component_item.startAnimation(animation);
        holder.sl.setText(String.valueOf(position+1));
        holder.sectionName.setText(componentSections.get(position).sectionValue);
        holder.componentName.setText(componentName);
        holder.price.setText("+£ "+String.format("%.2f",componentSections.get(position).selectedItem.subTotal));

    }

    @Override
    public int getItemCount() {
        return componentSections.size();
    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView sl,sectionName,componentName,price;
        RelativeLayout component_item;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            sl = itemView.findViewById(R.id.subCartListModelSL);
            sectionName = itemView.findViewById(R.id.subCartListModelSectionName);
            componentName = itemView.findViewById(R.id.subCartListModelSectionName);
            price = itemView.findViewById(R.id.subCartListModelPrice);
            component_item = itemView.findViewById(R.id.component_item);
        }
    }
}