package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.ProductUpdateResponseCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class ProductDetailsUpdateHelper extends AsyncTask<String, Void,String> {

    String product_uuid;
    String sort_order;
    String short_name;
    String categories;
    String available;
    boolean discountable;
    String description;
    ProductUpdateResponseCallBack callBack;
    String UpdateMsg="";
    public ProductDetailsUpdateHelper(String product_uuid, String sort_order, String short_name, String categories, String available, boolean discountable, String description, ProductUpdateResponseCallBack callBack) {
        this.product_uuid = product_uuid;
        this.sort_order = sort_order;
        this.short_name = short_name;
        this.categories = categories;
        this.available = available;
        this.discountable = discountable;
        this.description = description;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... strings) {
        return updateAllData( product_uuid, sort_order, short_name, categories, available, discountable, description);
    }
    private String updateAllData(String product_uuid, String sort_order, String short_name, String categories, String available, boolean discountable, String description) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());

        JSONObject productInfoJson = new JSONObject();
        JSONObject propertyInfoJson = new JSONObject();
        try {
            propertyInfoJson.put("available",available);
            propertyInfoJson.put("categories",categories);
            propertyInfoJson.put("sort_order",sort_order);

            productInfoJson.put("short_name",short_name);
            productInfoJson.put("product_type","ITEM");
            productInfoJson.put("discountable",discountable);
            productInfoJson.put("description",description);
            productInfoJson.put("last_update",today);
            productInfoJson.put("properties",propertyInfoJson);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, productInfoJson.toString());
        Log.d("jsorn", productInfoJson.toString());
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uuid)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code()==400){
                UpdateMsg = "success";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return UpdateMsg;
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.onProductUpdateResponse(UpdateMsg);
        super.onPostExecute(s);
    }
}
