package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.view.activity.OnlineCustomerOrderDetails;

public class OnlineCustomerOrderDetailsConsumerInfo extends Fragment implements View.OnClickListener {
    TextView firstName,lastName,email,phone,houseNumber,streetName,town,postCode,comments;
    CustomerModel customerModel;
    OnlineCustomerOrderDetailsAction actionFragment;
    OnlineCustomerOrderDetailsCart cartFragment;
    LinearLayout btnAddTableOrder, btnPrint;
    OrderModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_customer_info, container, false);

        model = (OrderModel) requireArguments().getSerializable("orderModel");
        actionFragment = (OnlineCustomerOrderDetailsAction)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCenter);
        cartFragment = (OnlineCustomerOrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        customerModel = model.customer;

        firstName = v.findViewById(R.id.orderDetailsConsumerFirstName);
        lastName = v.findViewById(R.id.orderDetailsConsumerLastName);
        email = v.findViewById(R.id.orderDetailsConsumerEmail);
        phone = v.findViewById(R.id.orderDetailsConsumerPhone);
        houseNumber = v.findViewById(R.id.orderDetailsConsumerBuilding);
        streetName = v.findViewById(R.id.orderDetailsConsumerStreet);
        town = v.findViewById(R.id.orderDetailsConsumerCity);
        postCode = v.findViewById(R.id.orderDetailsConsumerZip);
        btnAddTableOrder = v.findViewById(R.id.orderDetailsConsumerAddTableOrder);
        btnAddTableOrder.setOnClickListener(this);
        btnPrint = v.findViewById(R.id.orderDetailsConsumerPrint);
        btnPrint.setOnClickListener(this);

        comments = v.findViewById(R.id.commentsText);
        if(customerModel != null){
            if(customerModel.profile!= null){
                firstName.setText(firstName.getText()+customerModel.profile.first_name);
                lastName.setText(lastName.getText()+customerModel.profile.last_name);
                email.setText(email.getText()+customerModel.profile.email);
                phone.setText(phone.getText()+customerModel.profile.phone);
            }
            if(customerModel.addresses!=null){
                if(customerModel.addresses.size()!=0){
                    if(customerModel.addresses.get(0).properties!=null) {
                        CustomerAddressProperties pro = customerModel.addresses.get(0).properties;
                        houseNumber.setText(houseNumber.getText()+pro.building);
                        streetName.setText(streetName.getText()+pro.street_number+" "+pro.street_name);
                        town.setText(town.getText()+pro.city);
                        postCode.setText(postCode.getText()+pro.zip);
                    }
                }
            }
        }
        comments.setText(model.comments);

        if (model.bookingId != 0 && model.tableBookingModel != null &&
                (model.paymentMethod.equals("CARD") || model.paymentMethod.equals("CASH")) &&
            model.order_channel.equals("ONLINE")
        ) {
            btnAddTableOrder.setVisibility(View.VISIBLE);
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == btnAddTableOrder.getId()) {
            if (model.paymentMethod.equals("CASH")) {
                ((OnlineCustomerOrderDetails) requireActivity()).openOrderEditPage(model);
            } else {
                ((OnlineCustomerOrderDetails) requireActivity()).openOrderPageWithTable(model);
            }
        } else if(v.getId() == btnPrint.getId()){
            try {
                CashMemoHelper.printSendCustomer(model, model.items);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}