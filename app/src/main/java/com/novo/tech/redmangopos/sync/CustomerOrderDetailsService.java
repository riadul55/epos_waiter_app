package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.CustomerOrderListCallBack;
import com.novo.tech.redmangopos.callback.MyCallBack;

import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;

public class CustomerOrderDetailsService extends AsyncTask<String, Integer, String>{
    Context context;
    String orderID;
    MyCallBack callBack;
    OrderModel order;

    public CustomerOrderDetailsService(Context context, String orderID, MyCallBack callBack){
        this.context = context;
        this.orderID= orderID;
        this.callBack = callBack;
    }
    @Override
    protected String doInBackground(String... strings) {
        getOrderList();
        return null;
    }
    void getOrderList(){
        OkHttpClient client = new OkHttpClient();
        String token = SharedPrefManager.getSessionToken(context);
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+orderID)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession",token==null?"":token)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.d("mmmm", "getOrderList: "+response.code());
            if(response.code() == 200){
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    order = OrderModel.fromJSON(jsonObject);

                } catch (Exception e) {
                    Log.d("mmmmm", "getOrderData: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.d("mmmmm", "failedToUpdate: RE "+response.code());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callBack.OrderCallBack(order);
    }
}
