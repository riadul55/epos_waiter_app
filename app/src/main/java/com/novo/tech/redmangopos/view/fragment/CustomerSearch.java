package com.novo.tech.redmangopos.view.fragment;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CustomerSearchAdapter;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.storage.DBCustomerManager;

import java.util.ArrayList;
import java.util.List;

public class CustomerSearch extends Fragment {
    //private static CustomerModel customerModel;
    CustomerSearchAdapter adapter;
    public EditText customerNamePhone;
    ListView listView;
    Button addPhone;
    public static CustomerModel customerModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_customer_select, container, false);
        customerNamePhone = v.findViewById(R.id.customerSearchTextBox);
        customerNamePhone.setEnabled(false);
        customerNamePhone.setEnabled(true);
        listView = v.findViewById(R.id.customerSearchListView);
        listView.setAlpha(0f);
        listView.setTranslationY(50);
        addPhone = v.findViewById(R.id.phoneAdd);
        listView.animate().alpha(1f).translationYBy(-50).setDuration(900);

        setCustomerListView();
        customerNamePhone.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                setCustomerListView();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        addPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerInput customerInput = (CustomerInput) ((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
                assert customerInput != null;
                customerInput.phone.setText(customerNamePhone.getText());
            }
        });

        return v;
    }

    public void setCustomerListView(){
        adapter = new CustomerSearchAdapter(getContext(),getCustomerList(customerNamePhone.getText().toString()));
        listView.setAdapter(adapter);
    }
    List<CustomerModel> getCustomerList(String namePhone){
        DBCustomerManager dbCustomerManager = new DBCustomerManager(getContext());
        List<CustomerModel> allData = dbCustomerManager.getAllCustomerData();
        List<CustomerModel> filteredData = new ArrayList<>();
        for (CustomerModel model: allData) {
            if(model.profile!=null){
                if(namePhone == null){
                    filteredData = allData;
                }else if(namePhone.isEmpty()){
                    filteredData = allData;
                }else if(model.profile.first_name.toLowerCase().contains(namePhone.toLowerCase()) || model.profile.last_name.toLowerCase().contains(namePhone.toLowerCase()) || model.profile.phone.toLowerCase().contains(namePhone.toLowerCase()) || model.email.toLowerCase().contains(namePhone.toLowerCase())){
                    filteredData.add(model);
                }
            }

        }
        return filteredData;
    }
    public void backToOrder(CustomerModel model){
        Intent intent = new Intent();
        intent.putExtra("customer",model);
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }

/*
    public static void addNewCustomerFromDialog(CustomerModel model) {
        {
            //final Dialog dialog = new Dialog((FragmentActivity) getContext());
            Dialog dialog = new Dialog((FragmentActivity) context);
            dialog.setContentView(R.layout.dialog_create_new_customer);
            dialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

         */
/*   List<CustomerAddress> customerAddressList = new ArrayList<CustomerAddress>();
            customerAddressList.add((CustomerAddress) model.addresses);
    /*




            final EditText et_first_name = (EditText) dialog.findViewById(R.id.et_first_name);
            final EditText et_last_name = (EditText) dialog.findViewById(R.id.et_last_name);
            final EditText et_phn_number = (EditText) dialog.findViewById(R.id.et_phn_number);
            final EditText et_email_address = (EditText) dialog.findViewById(R.id.et_email_address);
            final EditText et_zip_name = (EditText) dialog.findViewById(R.id.postalCode);

            final EditText et_street_name = (EditText) dialog.findViewById(R.id.et_street_name);
            final EditText et_building_number = (EditText) dialog.findViewById(R.id.et_building_number);
            final EditText et_state = (EditText) dialog.findViewById(R.id.et_state);
            final EditText et_city = (EditText) dialog.findViewById(R.id.et_city);

            final Button btn_save = (Button) dialog.findViewById(R.id.btn_save);
            final Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
            final ImageButton ib_close = (ImageButton) dialog.findViewById(R.id.ib_close);


            AutoCompleteTextView zipCodeText = dialog.findViewById(R.id.postalCode);

            if (model != null)
            if (model.profile!=null) {
                et_first_name.setText(model.profile.first_name);
                et_last_name.setText(model.profile.last_name);
                et_phn_number.setText(model.profile.phone);
                et_email_address.setText(model.profile.email);

                if(model.addresses != null) {
                    if (model.addresses.size() != 0) {
                        CustomerAddress address = model.addresses.get(0);
                        if (address.properties != null) {
                            zipCodeText.setText(address.properties.zip);
                            et_street_name.setText(address.properties.street_name);
                            et_building_number.setText(address.properties.building);
                            et_state.setText(address.properties.state);
                            et_city.setText(address.properties.city);
                        }
                    }
                }
            }
            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(et_first_name.getText().toString().isEmpty()){
                        et_first_name.setError("required");
                    }else if(et_phn_number.getText().toString().isEmpty()){
                        et_phn_number.setError("required");
                    }else{
                        DBCustomerManager dbCustomerManager = new DBCustomerManager(context);
                        CustomerProfile profile = new CustomerProfile(
                                et_phn_number.getText().toString(),
                                et_last_name.getText().toString(),
                                et_first_name.getText().toString(),
                                et_email_address.getText().toString()
                        );
                        CustomerAddressProperties addressProperties = new CustomerAddressProperties(
                                et_zip_name.getText().toString(),
                                "UK",
                                et_city.getText().toString(),
                                "",
                                et_state.getText().toString(),
                                et_building_number.getText().toString(),
                                et_street_name.getText().toString()
                        );
                        CustomerAddress address = new CustomerAddress(
                                null,
                                "INVOICE",
                                addressProperties
                        );
                        List<CustomerAddress> addressList = new ArrayList<>();
                        addressList.add(0,address);
                        CustomerModel customer = new CustomerModel(
                                -1,
                                null,
                                et_email_address.getText().toString(),
                                et_email_address.getText().toString(),
                                profile,
                                addressList,false
                        );

                        if(customerModel == null){
                            int id = dbCustomerManager.customerExist(et_phn_number.getText().toString());
                            if(id < 1){
                                dbCustomerManager.addNewCustomer(customer,false);
                               // clear();
                                customerModel = null;
                                et_first_name.setText("");
                                et_last_name.setText("");
                                et_phn_number.setText("");
                                et_email_address.setText("");
                                et_building_number.setText("");
                                et_street_name.setText("");
                                et_city.setText("");
                                zipCodeText.setText("");
                                et_state.setText("");
                                dialog.cancel();
                            }else{
                                Toast.makeText(context,"Phone Number Already Exist",Toast.LENGTH_LONG).show();
                            }

                        }else{
                            customer.dbId = customerModel.dbId;
                            if(customerModel.consumer_uuid != null){
                                customer.consumer_uuid = customerModel.consumer_uuid;
                                if(customerModel.addresses.size()!=0){
                                    if(customerModel.addresses.get(0).address_uuid!= null){
                                        customer.addresses.get(0).address_uuid = customerModel.addresses.get(0).address_uuid;
                                    }
                                }
                            }
                            dbCustomerManager.updateCustomer(customer,false);
                           // clear();
                            customerModel = null;
                            et_first_name.setText("");
                            et_last_name.setText("");
                            et_phn_number.setText("");
                            et_email_address.setText("");
                            et_building_number.setText("");
                            et_street_name.setText("");
                            et_city.setText("");
                            et_zip_name.setText("");
                            et_state.setText("");
                            dialog.cancel();

                        }

                    }
                    //customerSearch.setCustomerListView();


                }
            });
            ib_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.show();
            dialog.getWindow().setAttributes(lp);
        }
    }
*/

}