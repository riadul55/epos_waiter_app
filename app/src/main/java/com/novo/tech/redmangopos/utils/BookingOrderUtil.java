package com.novo.tech.redmangopos.utils;

import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.OrderModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BookingOrderUtil {
    public static List<CartItem> getMergedItems(OrderModel orderModel, List<OrderModel> orderList) {
        List<CartItem> cartItems = new ArrayList<CartItem>();
        for (OrderModel model: orderList) {
            if (model.bookingId == orderModel.bookingId) {
                for (CartItem cartItem: model.items) {
                    boolean exist = alreadyExist(cartItem, cartItems);
                    if (exist) {
                        for (int i=0;i<cartItems.size();i++) {
                            CartItem cartItem1 = cartItems.get(i);
                            if (Objects.equals(cartItem1.uuid, cartItem.uuid)) {
                                cartItem1.quantity+=cartItem.quantity;
                                cartItems.set(i, cartItem1);
                                break;
                            }
                        }
                    } else {
                        cartItems.add(cartItem);
                    }
                }
            }
        }
        return cartItems;
    }


    public static boolean alreadyExist(CartItem item, List<CartItem> cartItemList){
        List<Boolean> anyExist = new ArrayList<>();
        int j = 0;
        for(CartItem cartItem:cartItemList){
            anyExist.add(false);
            if(cartItem.uuid.equals(item.uuid)){
                if(item.componentSections.size() == 0){
                    anyExist.set(j,true);
                    break;
                }else if(item.componentSections.size()!=cartItem.componentSections.size()){
                    anyExist.set(j,false);
                    break;
                }else{
                    List<Boolean> data = new ArrayList<>();
                    for(int i = 0;i<cartItem.componentSections.size();i++){
                        data.add(false);
                        ComponentSection section1 = cartItem.componentSections.get(i);
                        ComponentSection section2 = item.componentSections.get(i);
                        if(section1.sectionValue.equals(section2.sectionValue)){
                            if(section1.selectedItem!= null && section2.selectedItem != null){
                                if(section1.selectedItem.productUUid.equals(section2.selectedItem.productUUid)){
                                    if (section1.selectedItem.subComponents.size() != 0) {
                                        if (section1.selectedItem.subComponentSelected.productUUid.equals(section2.selectedItem.subComponentSelected.productUUid)) {
                                            data.set(i,true);
                                        }
                                    } else {
                                        data.set(i,true);
                                    }
                                }
                            }
                        }

                    }
                    anyExist.set(j,true);
                    for(boolean b : data){
                        if(!b){
                            anyExist.set(j,false);
                            break;
                        }
                    }
                }
            }
            j++;
        }
        boolean finalExist = false;
        for(boolean b : anyExist){
            if(b){
                finalExist = true;
                break;
            }
        }
        return finalExist;
    }
}
