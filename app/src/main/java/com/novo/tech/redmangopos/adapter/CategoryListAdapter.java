package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.view.fragment.CategoryAndProduct;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryListViewHolder> {

    Context mCtx;
    List<PropertyItem> categoryList;
    String selectedCategory="";

    public CategoryListAdapter(Context mCtx, List<PropertyItem> categoryList,String selected) {
        this.mCtx = mCtx;
        this.categoryList = categoryList;
        if(selected!=null){
            if(selected.toLowerCase().equals("all")){
                if(categoryList.size()>0){
                    selectedCategory = categoryList.get(0).value;
                }
            }else
                selectedCategory = selected;
        }else if(categoryList.size()>0){
            selectedCategory = categoryList.get(0).value;
        }
    }

    @NonNull
    @Override
    public CategoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_category, parent, false);
        return new CategoryListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListViewHolder holder, int _position) {
        holder.setIsRecyclable(false);
        PropertyItem category = categoryList.get(_position);
//        Glide.with(mCtx)
//                .load(SharedPrefManager.getBaseUrl(mCtx)+"config/product/property/category/"+category.value+"/file")
//                .error(mCtx.getDrawable(R.drawable.no_image))
//                .into(holder.catImage).onLoadFailed(
//                        mCtx.getDrawable(R.drawable.no_image));

        if (category != null) {
            holder.categoryTitle.setText(String.valueOf(category.display_name));
            CategoryAndProduct productFragment = (CategoryAndProduct) ((FragmentActivity) mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            holder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#39A6A3")));
                            break;
                        case MotionEvent.ACTION_UP:
                            if(category.value.equals(selectedCategory)) {
                                holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#DC143C")));
                            }else{
                                holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#E47F05")));
                            }
                            productFragment.getCategoryProduct(category.value);
                            selectedCategory = category.value;
                            notifyDataSetChanged();
                            break;
                    }
                    return true;
                }

            });
        }
        if(category.value.equals(selectedCategory)){
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#DC143C")));
        }else{
            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#39A6A3")));
        }

    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    static class CategoryListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView categoryTitle;

        public CategoryListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.categoryModelImage);
            categoryTitle = itemView.findViewById(R.id.categoryModelTitle);
        }
    }
}
