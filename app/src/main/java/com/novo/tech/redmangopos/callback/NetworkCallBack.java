package com.novo.tech.redmangopos.callback;

public interface NetworkCallBack {
    void onNetworkStateChange(boolean connected);
}
