package com.novo.tech.redmangopos.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.ShiftModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DBShiftManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBShiftManager(Context c) {
        context = c;
    }

    public DBShiftManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }
    public ShiftModel getCurrentShift(){
        ShiftModel currentShift = null;
        Cursor cursor = database.rawQuery("SELECT * from shift where ifnull(closeTime,'')=''",null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                int submitted = cursor.getInt(cursor.getColumnIndex("cloudSubmitted"));
                String _opTime = cursor.getString(cursor.getColumnIndex("openTime"));
                Date openTime = null;
                try {
                    openTime = new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault()).parse(_opTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                currentShift = new ShiftModel(id,submitted,openTime,null);
            }
        }
        return currentShift;
    }
    public String getLastShift(){
        String lastOpenedTime = null;
        Cursor cursor = database.rawQuery("SELECT * from shift order by id DESC",null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                lastOpenedTime = cursor.getString(cursor.getColumnIndex("closeTime"));
            }
        }
        return lastOpenedTime;
    }
    public int openShift(){
        int shiftNo=-1;
        ContentValues contentValue = new ContentValues();
        contentValue.put("openTime", DateTimeHelper.getDBDateAndTime());
        contentValue.put("cloudSubmitted", false);
        long id = database.insert(DatabaseHelper.SHIFT_TABLE_NAME,null,contentValue);
        shiftNo = (int) id;
        return shiftNo;
    }
    public void closeShift(int id){
        ContentValues contentValue = new ContentValues();
        contentValue.put("closeTime", DateTimeHelper.getDBDateAndTime());
        database.update(DatabaseHelper.SHIFT_TABLE_NAME,contentValue,"id=?",new String[]{String.valueOf(id)});
    }
}
