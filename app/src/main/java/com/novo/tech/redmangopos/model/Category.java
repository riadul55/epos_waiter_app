package com.novo.tech.redmangopos.model;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Category implements Serializable {
    String keyName,value,displayName,description,fileName,fileType;
    int sortOrder;

    public String getKeyName() {
        return keyName;
    }

    public String getValue() {
        return value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public Category(String keyName, String value, String displayName, String description, String fileName, String fileType, int sortOrder) {
        this.keyName = keyName;
        this.value = value;
        this.displayName = displayName;
        this.description = description;
        this.fileName = fileName;
        this.fileType = fileType;
        this.sortOrder = sortOrder;
    }
    public static Category fromJSON(JSONObject data){

        return new Category(
                data.optString("key_name"),
                data.optString("value"),
                data.optString("display_name"),
                data.optString("description"),
                data.optString("filename"),
                data.optString("content_type"),
                data.optInt("sort_order")
        );
    }
    public static JSONObject toJSON(Category category){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("key_name",category.keyName);
            jsonObject.put("value",category.value);
            jsonObject.put("display_name",category.displayName);
            jsonObject.put("description",category.description);
            jsonObject.put("filename",category.fileName);
            jsonObject.put("content_type",category.fileType);
            jsonObject.putOpt("sort_order",category.sortOrder);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
