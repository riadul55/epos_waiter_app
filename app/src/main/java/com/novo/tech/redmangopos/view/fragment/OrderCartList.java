package com.novo.tech.redmangopos.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CartListAdapter;
import com.novo.tech.redmangopos.callback.OrderCreateCallBack;
import com.novo.tech.redmangopos.databinding.ViewDynamicItemBinding;
import com.novo.tech.redmangopos.extra.DateTimeHelper;

import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.CustomerProfile;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.ResponseModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.socket.OrderModelToJson;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.storage.DBProductManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.OrderCreatingApiHelper;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.view.activity.CustomerSelect;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.view.activity.TableBookingActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.*;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class OrderCartList extends Fragment {
    TextView type,orderCreateFragmentOrderTableNo;
    TextView totalPayable;
    RecyclerView recyclerViewList;
    CartListAdapter adapter;
    public OrderModel orderModel = new OrderModel();
    public int sortingType = 0;
    CategoryAndProduct categoryAndProduct;
    TextView txt_customerName;
    RelativeLayout btnCustomerSelect;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String orderType = requireArguments().getString("orderType","TABLE");
        CustomerModel customerModel = (CustomerModel) requireArguments().getSerializable("customerModel");
        CustomerAddress shippingAddress = (CustomerAddress) requireArguments().getSerializable("shippingAddress");
        String bookingId = requireArguments().getString("bookingId");
        TableBookingModel table = (TableBookingModel) requireArguments().getSerializable("table");
        OrderModel orderModelId = (OrderModel) requireArguments().getSerializable("OrderModel");
        View v = inflater.inflate(R.layout.fragment_order_cart_list, container, false);
        type = v.findViewById(R.id.orderCreateFragmentOrderType);
        txt_customerName = v.findViewById(R.id.orderCreateFragmentCustomerTitle);
        totalPayable = v.findViewById(R.id.orderCreateFragmentTotalAmount);
        orderCreateFragmentOrderTableNo = v.findViewById(R.id.orderCreateFragmentOrderTableNo);
        recyclerViewList = v.findViewById(R.id.orderCreateCartRecyclerView);
        btnCustomerSelect = v.findViewById(R.id.orderCreateSelectCustomer);
        if(orderModelId != null ){
            orderModel = orderModelId;
        }else{
            //initialize some data to bypass null error
            orderModel.order_id = SharedPrefManager.getLastOrderId(getContext())+1;
            orderModel.items = new ArrayList<>();
            orderModel.order_type = orderType;
            orderModel.fixedDiscount = false;
            orderModel.customer = customerModel;
            orderModel.shippingAddress = shippingAddress;
            orderModel.order_channel = "WAITERAPP";
            if(orderModel.order_status == null)
                orderModel.order_status = "NEW";
            if(orderModel.paymentMethod == null)
                orderModel.paymentMethod = "CASH";
        }
        type.setText(orderModel.order_type);
        Log.e("OrderItems==>", orderModel.items.size() + "");
        loadData();

        btnCustomerSelect.setOnClickListener(v1 -> {
            Intent i = new Intent(getContext(), CustomerSelect.class);
            startActivityForResult(i, 1);
        });

        if (bookingId != null) {
//            if(table != null && orderModel.bookingId == 0 && !orderType.equalsIgnoreCase("DELIVERY")){
//                bookTableNow(table);
//            }
            orderModel.bookingId = Integer.parseInt(bookingId);
        }
//        else {
//            orderModel.bookingId = Integer.parseInt(bookingId);
//        }

        if(table!=null){
            orderCreateFragmentOrderTableNo.setText(String.valueOf(table.table_no));
            orderModel.tableBookingModel = table;
        }
        return v;
    }

    void bookTableNow(TableBookingModel tableBookingModel){
        ProgressDialog progress = new ProgressDialog(getContext());
        progress.setMessage("Loading");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        JSONObject customerJson = new JSONObject();
        try {
            {
                customerJson.put("local_no", "1");
                customerJson.put("floor_no", "1");
                customerJson.put("table_no",tableBookingModel.table_no);
                customerJson.put("num_adults",tableBookingModel.adult);
                customerJson.put("num_children",tableBookingModel.child);
                customerJson.put("booking_start",DateTimeHelper.formatLocalDateTimeForServer(DateTimeHelper.getTime()));
                customerJson.put("creator_type","PROVIDER");
                customerJson.put("creator_uuid",providerID);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        Log.e("TableBooking==>", customerJson.toString());

        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"booking/table?ignore_booking_logic=true")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                progress.dismiss();
                failedToConnect();
                Log.e("BookingError==>", e.getMessage());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response){
                progress.dismiss();
                if(response.code()!=201){
                    failedToConnect();
                    Log.e("BookingError==>", response.code() + "");
                }else{
                    String generatedId = response.header("Location");
                    orderModel.tableBookingModel = tableBookingModel;
                    orderModel.bookingId = Integer.parseInt(generatedId);
                    getActivity().runOnUiThread(()->
                    Toast.makeText(getContext(),"Table "+String.valueOf(tableBookingModel.table_no)+" has been booked",Toast.LENGTH_SHORT).show());
                }
            }
        });


    }


    void failedToConnect(){
        getActivity().runOnUiThread(() -> new androidx.appcompat.app.AlertDialog.Builder(getContext())
                .setMessage("Failed to book table ! please Try again")
                .setPositiveButton("OK", (dialog, which) -> {
                    dialog.dismiss();
                    getActivity().finish();
                }).create().show());


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            if(resultCode == RESULT_OK) {
                orderModel.customer = (CustomerModel) data.getSerializableExtra("customer");
                displayCustomer();
            }
        } else if(requestCode==2){
            Log.d("xxxxxxx", "onActivityResult: "+resultCode);
            if(resultCode == RESULT_OK){
                OrderModel od = (OrderModel) data.getSerializableExtra("orderModel");
                orderModel.discountAmount = od.discountAmount;
                orderModel.discountPercentage = od.discountPercentage;
                orderModel.comments = od.comments;
                orderModel.deliveryCharge = od.deliveryCharge;
                orderModel.tips = od.tips;
                orderModel.adjustmentAmount = od.adjustmentAmount;
                orderModel.adjustmentNote = od.adjustmentNote;
                orderModel.totalPaid = od.totalPaid;
            }

        }
    }

    void displayCustomer(){
        if(orderModel.customer != null){
            if(orderModel.customer.profile!= null){
                txt_customerName.setText((orderModel.customer.profile.first_name+" "+orderModel.customer.profile.last_name).toString().replaceAll("null",""));
            }
        }
    }

    void loadData(){
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewList.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        if(orderModel.items == null)
            orderModel.items = new ArrayList<>();
        List<CartItem> _itms = orderModel.items;
        _itms.removeIf((obj)->obj.type.equals("DELIVERY"));

        _itms.sort((a,b)->{
            if(a.localId>b.localId){
                return 1;
            }else if(a.localId==b.localId){
                return 0;
            }else
                return -1;
        });

        adapter = new CartListAdapter(getContext(), _itms);
        recyclerViewList.setAdapter(adapter);
        updateCartData();
    }
    public void updateCartData(){
        orderModel.subTotal = 0.0;
        if(orderModel.items == null){
            orderModel.items = new ArrayList<>();
        }

        orderModel.items.sort(Comparator.comparingInt(a -> a.localId));
        Collections.reverse(orderModel.items);
        for (CartItem item : orderModel.items){
            if(!item.offered){
                orderModel.subTotal+=(item.subTotal*item.quantity);

                if (item.extra != null) {
                    for (CartItem extraItem: item.extra) {
                        orderModel.subTotal+=extraItem.price;
                    }
                }

            }
            else
                orderModel.subTotal += item.total;
        }
        orderModel.total = orderModel.subTotal;
        totalPayable.setText("£ "+String.format(Locale.getDefault(),"%.2f",orderModel.total));
        adapter.notifyDataSetChanged();
        displayCustomer();

        try {
            ((OrderCreate) requireActivity()).searchBox.setText("");
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    void updateCategoryAndProduct(){
        if(categoryAndProduct==null){
            try{
                categoryAndProduct = (CategoryAndProduct)(getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateCategory);

            }catch (Exception e){

            }
        }
        if(categoryAndProduct != null){
            ((OrderCreate) requireActivity()).searchBox.setText("");
            try {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                // TODO: handle exception
            }
            categoryAndProduct.getCategoryProduct(categoryAndProduct.category);
        }
    }


    public void clearCart(){
        orderModel.items = new ArrayList<>();
        orderModel.total = 0;
        orderModel.subTotal = 0;
        orderModel.discountAmount = 0;
        orderModel.adjustmentAmount = 0;
        orderModel.deliveryCharge = 0;
        orderModel.tips = 0;
        adapter = new CartListAdapter(getContext(), orderModel.items);
        recyclerViewList.setAdapter(adapter);
        updateCartData();
    }
    public void addToCart(CartItem item){
        boolean exist = alreadyExist(item);
        if(!exist){
            item.localId = getMaxId()+1;

            if(offerExist()){
                if(item.offerAble){
                    item.total = 0;
                    item.type = "DYNAMIC";
                    item.offered = true;
                }
            }

            orderModel.items.add(item);
            adapter.active = item;
            updateCategoryAndProduct();
        }
        updateCartData();
    }
    int getMaxId(){
        int max = 0;
        for (CartItem item : orderModel.items){
            if(item.localId > max)
                max = item.localId;
        }
        return max;
    }
    public void setComment(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                    orderModel.items.get(i).comment = item.comment;
            }
        }
        updateCartData();
    }
    public void setPrice(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                orderModel.items.get(i).type = "DYNAMIC";
                orderModel.items.get(i).offered = true;
                orderModel.items.get(i).total = item.total;
                break;
            }
        }
        updateCartData();
    }
    public void inCreaseQuantity(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                    orderModel.items.get(i).quantity++;
            }
        }
        updateCartData();
    }
    public void deCreaseQuantity(CartItem item){
        for (int i = 0; i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                    if(orderModel.items.get(i).quantity > 1)
                        orderModel.items.get(i).quantity--;

            }
        }
        updateCartData();
    }
    public void removeFromCart(CartItem item){
        for (int i=0;i<orderModel.items.size();i++){
            if(orderModel.items.get(i).localId == item.localId){
                deleteItemFromCart(i);
                boolean offer = offerExist();
                if(!offer){
                    for (CartItem cart: orderModel.items) {
                        if(cart.offered){
                            cart.offered = false;
                        }
                    }
                }

                break;
            }
        }
        updateCartData();

    }
    public void updateCartItem(CartItem item){
        boolean exist = alreadyExist(item);
        if(!exist){
            for (int i = 0; i<orderModel.items.size();i++){
                if(orderModel.items.get(i).localId == item.localId) {
                    orderModel.items.set(i, item);
                    adapter.active = null;
                    break;
                }
            }
        }
        updateCartData();
    }

    void deleteItemFromCart(int index){
        FragmentManager fm = getFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.frameLayoutOrderCreateCategory);
        if(currentFragment instanceof ComponentsEdit){
            Toast.makeText(getContext(),"You Cart item in Edit Mode, Please Save or Cancel to delete an item",Toast.LENGTH_LONG).show();
        }else{
            orderModel.items.remove(index);
        }
    }

    private boolean offerExist (){
        boolean result = false;
        for(CartItem item : orderModel.items){
            if(item.category.equals("offer")){
                result = true;
                break;
            }
        }
        return result;
    }


    public void openCheckoutPage(){
        orderModel.items.removeIf((a) -> a.paymentMethod.equals("CARD"));
        if(orderModel.items==null){
            Toast.makeText(getContext(),"Your Cart is Empty",Toast.LENGTH_SHORT).show();
            orderModel.items = new ArrayList<>();
            return;
        }else if(orderModel.items.size()==0){
            Toast.makeText(getContext(),"Your Cart is Empty",Toast.LENGTH_SHORT).show();
            return;
        }else if(orderModel.subTotal==0){
            Toast.makeText(getContext(),"Cart amount is too low",Toast.LENGTH_SHORT).show();
            return;
        }

        if (orderModel.order_type != null && orderModel.order_type.equalsIgnoreCase("DELIVERY")) {
            if (orderModel.customer == null) {
                Toast.makeText(requireContext(), "Please provide customer information", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        orderModel.total = orderModel.subTotal;
        if (orderModel.orderDate == null) {
            orderModel.orderDate = DateTimeHelper.getTime();
            orderModel.orderDateTime = DateTimeHelper.getTime();
        }

        Log.e("OrderId==>", orderModel.db_id + "");

//        int id = SharedPrefManager.createUpdateOrder(getContext(),orderModel);
//        if(id < 0){
//            Toast.makeText(getContext(),"Failed to create order . error : Db Id Conflicting",Toast.LENGTH_SHORT).show();
//            return;
//        }
//        orderModel.db_id = id;
//        requireActivity().finish();

        new AlertDialog.Builder(getContext())
                .setMessage("Are you sure you want submit this order?")
                .setPositiveButton("Yes", (dialog, which) -> {
//                    submitOrderToServer();
//                    if (orderModel.tableBookingModel != null && orderModel.bookingId != 0 && !orderModel.order_type.equalsIgnoreCase("DELIVERY")) {
//                        busyTheTable(orderModel.tableBookingModel);
//                        Log.e("BookingId==>", orderModel.bookingId + "");
//                    } else {
//                    }
                    Log.e("BookingId1==>", orderModel.bookingId + "");
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                        jsonObject.put(SocketHandler.ORDER, OrderModelToJson.fromModel(orderModel, requireContext()));
//                            OrderModelToJson.fromModel(orderModel, requireContext());
                    } catch (Exception e) {
                        Log.e("SubmittedOrder==>", e.toString());
                        e.printStackTrace();
                    }
                    Log.e("SubmittedOrder==>", jsonObject.toString());
                    BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_UPDATE, jsonObject);
                    Intent intent = new Intent();
                    requireActivity().setResult(TableBookingActivity.ORDER_CREATE_RESULT_CODE, intent);
                    requireActivity().finish();
                })
                .setNegativeButton("Back", (dialog, which) -> {
                    dialog.dismiss();
                }).create().show();

//        Intent myIntent = new Intent(getActivity(), Payment.class);
//        myIntent.putExtra("orderData", orderModel.db_id);
//        startActivityForResult(myIntent, 2);
    }

    void busyTheTable(TableBookingModel tableBookingModel) {
        ProgressDialog progress = new ProgressDialog(requireContext());
        progress.setMessage("Loading");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        JSONObject customerJson = new JSONObject();
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + "config/booking/table/busy" + "?floor_no=1" + "&local_no=1" + "&table_no=" + tableBookingModel.table_no)
                .patch(requestBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("ProviderSession", providerSession)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                progress.dismiss();
                failedToConnect();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) {
                progress.dismiss();
                if (response.code() == 200) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                        jsonObject.put(SocketHandler.ORDER, OrderModelToJson.fromModel(orderModel, requireContext()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("SubmittedOrder==>", jsonObject.toString());
                    BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_UPDATE, jsonObject);
                    Intent intent = new Intent();
                    requireActivity().setResult(TableBookingActivity.ORDER_CREATE_RESULT_CODE, intent);
                    requireActivity().finish();
                } else {
                    showToast(response.message() + " | " + response.code());
                }
            }
        });
    }

    void showToast(String message) {
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    void submitOrderToServer(){
        ProgressDialog progress = new ProgressDialog(getContext());
        progress.setMessage("Loading");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        new OrderCreatingApiHelper(getContext(), new OrderCreateCallBack() {
            @Override
            public void onOrderCreate(ResponseModel responseModel) {
                progress.dismiss();

                if(responseModel.success){
                    new AlertDialog.Builder(getContext())
                            .setMessage("Order Has been submitted")
                            .setPositiveButton("OK", (dialog, which) -> {
                                getActivity().finish();
                            })
                            .create().show();
                }else{
                    new AlertDialog.Builder(getContext())
                            .setMessage(responseModel.message+ (responseModel.statusCode != 0 ? " error code = "+String.valueOf(responseModel.statusCode):""))
                            .setPositiveButton("ok", (dialog, which) -> {
                                dialog.dismiss();
                            })
                            .create().show();
                }
            }
        },orderModel).execute();
    }





    public void voidOrderAlert(){
        String[] options = { "Some item are not available ", "Customer request", "Technical issue ", "Recall- Opened", "Recall-Unopened ", "Other",};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Void order cause of");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                voidOrder(options[which]);
            }
        });
        builder.create().show();
    }
    void voidOrder(String message){

    }

    public boolean alreadyExist(CartItem item){
        List<Boolean> anyExist = new ArrayList<>();
        int j = 0;
        for(CartItem cartItem:orderModel.items){
            anyExist.add(false);
            if(cartItem.uuid.equals(item.uuid)){
                if(item.componentSections.size() == 0){
                    anyExist.set(j,true);
                    break;
                }else if(item.componentSections.size()!=cartItem.componentSections.size()){
                    anyExist.set(j,false);
                    break;
                }else{
                    List<Boolean> data = new ArrayList<>();
                    for(int i = 0;i<cartItem.componentSections.size();i++){
                        data.add(false);
                        ComponentSection section1 = cartItem.componentSections.get(i);
                        ComponentSection section2 = item.componentSections.get(i);
                        if(section1.sectionValue.equals(section2.sectionValue)){
                            if(section1.selectedItem!= null && section2.selectedItem != null){
                                if(section1.selectedItem.productUUid.equals(section2.selectedItem.productUUid)){
                                    if (section1.selectedItem.subComponents.size() != 0) {
                                        if (section1.selectedItem.subComponentSelected.productUUid.equals(section2.selectedItem.subComponentSelected.productUUid)) {
                                            data.set(i,true);
                                        }
                                    } else {
                                        data.set(i,true);
                                    }
                                }
                            }
                        }

                    }
                    anyExist.set(j,true);
                    for(boolean b : data){
                        if(!b){
                            anyExist.set(j,false);
                            break;
                        }
                    }
                }
            }
            j++;
        }
        boolean finalExist = false;
        for(boolean b : anyExist){
            if(b){
                finalExist = true;
                break;
            }
        }
        return finalExist;
    }
    public void getDynamicProduct(){
        int[] printOrder = {2};
        LayoutInflater inflater = LayoutInflater.from(getContext());
        ViewDynamicItemBinding binding = ViewDynamicItemBinding.inflate(inflater);
        binding.radioStarter.setChecked(true);
        binding.categoryRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioStarter:
                        printOrder[0] = 2;
                        break;
                    case R.id.radioMainDish:
                        printOrder[0] = 3;
                        break;
                    case R.id.radioSideDish:
                        printOrder[0] = 4;
                        break;
                    case R.id.radioSundries:
                        printOrder[0] = 5;
                        break;

                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Dynamic Product");
        builder.setView(binding.getRoot())
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = binding.productName.getText().toString();
                        String strPrice = binding.productPrice.getText().toString();
                        double price = strPrice.isEmpty()?0.0:Double.parseDouble(strPrice);
                        if (name.isEmpty()) {
                            binding.productName.setError("Required");
                        } else if (strPrice.isEmpty()) {
                            binding.productPrice.setError("Required");
                        } else {
                            CartItem item = new CartItem(-1,name.replaceAll(" ","_"),"",name,new ArrayList<>(),price,0.0,price,price,1, 1,"","DYNAMIC",printOrder[0],true,false,false, "0","",new ArrayList<>(), true, "CASH");
                            addToCart(item);
                            dialog.dismiss();
                        }
//                        DBProductManager manager = new DBProductManager(getContext());
//                        manager.addProduct(item);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

}