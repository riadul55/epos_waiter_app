package com.novo.tech.redmangopos.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.databinding.ViewPriceInputBinding;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;

import java.util.List;
import java.util.Locale;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.OrderListViewHolder> implements View.OnClickListener {

    Context mCtx;
    List<CartItem> cartItems;
    public CartItem active;

    public CartListAdapter(Context mCtx, List<CartItem> cartItems) {
        this.mCtx = mCtx;
        this.cartItems = cartItems;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_cart_item, parent, false);
        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        OrderCartList cartListFragment = (OrderCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);

        int _position = holder.getAdapterPosition();
        CartItem item = cartItems.get(holder.getAdapterPosition());
        double price = item.subTotal * item.quantity;

        if (item.extra != null) {
            for (CartItem extraItem: item.extra) {
                price+=extraItem.price;
            }
        }
        if(item.offered){
            price = item.total;
        }

        holder.setIsRecyclable(false);
        holder.sl.setText("x"+String.valueOf(item.quantity));
        holder.name.setText(cartItems.get(position).shortName);
        holder.price.setText("£ "+String.format(Locale.getDefault(),"%.2f",price));
        holder.quantity.setText(String.valueOf(item.quantity));

        holder.ib_add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(mCtx);
                dialog.setContentView(R.layout.note_layout);
                dialog.setCancelable(true);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

                final RelativeLayout rl_note_parent = dialog.findViewById(R.id.rl_note_parent);
                final EditText ed_alert_note = dialog.findViewById(R.id.ed_alert_note);
                ed_alert_note.setText(item.comment);
                final Button btn_alert_note = dialog.findViewById(R.id.btn_alert_note);
                btn_alert_note.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String note = ed_alert_note.getText().toString().trim();
                        if (!TextUtils.isEmpty(note)){
                            holder.comment.setText(note);
                            CartItem cartItem = cartItems.get(position);
                            cartItem.comment = holder.comment.getText().toString();
                            cartListFragment.setComment(cartItem);
                            ed_alert_note.setText("");
                            dialog.cancel();
                        }
                    }
                });
                dialog.show();
                dialog.getWindow().setAttributes(lp);
            }
        });

        double finalPrice = price;
        holder.applyPrice.setOnClickListener(v -> {
            double originalPrice = item.subTotal;
            double offerPrice = item.offerPrice;
            AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
            LayoutInflater inflater = LayoutInflater.from(mCtx);
            ViewPriceInputBinding binding = ViewPriceInputBinding.inflate(inflater);
            binding.currentPrice.setText(String.format(Locale.getDefault(),"%.2f", finalPrice));
            binding.currentPrice.setSelection(binding.currentPrice.getText().length());
            binding.originalPrice.setText(String.format(Locale.getDefault(),"%.2f", item.subTotal));
            binding.offerPrice.setText(String.format(Locale.getDefault(),"%.2f", item.offerPrice));
            binding.LLOfferPrice.setOnClickListener(v1 -> {
                binding.currentPrice.setText(String.format(Locale.getDefault(),"%.2f", offerPrice));
                binding.currentPrice.setSelection(binding.currentPrice.getText().length());
            });
            binding.LLOriginalPrice.setOnClickListener(v12 -> {
                binding.currentPrice.setText(String.format(Locale.getDefault(),"%.2f", originalPrice));
                binding.currentPrice.setSelection(binding.currentPrice.getText().length());
            });
            builder.setTitle("Item Price")
                    .setView(binding.getRoot())
                    .setPositiveButton("Apply", (dialog, which) -> {
                        try{
                            item.total = Double.parseDouble(binding.currentPrice.getText().toString().isEmpty()?"0":binding.currentPrice.getText().toString());
                            cartListFragment.setPrice(item);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    })
                    .setNegativeButton("Back", (dialog, which) -> dialog.dismiss())
                    .create()
                    .show();
        });

        holder.comment.setText(item.comment);
        String componentName = "";
        if(item.componentSections.size()>0){
            for (ComponentSection section : item.componentSections){

                String _comName = section.selectedItem.shortName;
                if(section.selectedItem.subComponentSelected != null){
                    if (!section.selectedItem.subComponentSelected.shortName.toUpperCase().equals("NONE"))
                        _comName+=" -> "+section.selectedItem.subComponentSelected.shortName;
                }
                componentName+=(section.sectionValue+" : "+_comName+"\n");
            }
        }
        if(item.extra != null && item.extra.size() > 0){
            StringBuilder str = new StringBuilder("Extra :");
            for (CartItem extraItem :item.extra) {
                str.append("  *").append(extraItem.shortName);
            }
            componentName+=str.toString()+"\n";
        }
        holder.componentsName.setText(componentName);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.extentView.getVisibility() == View.GONE){
                    active = item;
                }
                else
                    active = null;
                notifyDataSetChanged();
            }
        });
        holder.increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartListFragment.inCreaseQuantity(cartItems.get(position));

            }
        });
        holder.decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartListFragment.deCreaseQuantity(cartItems.get(position));
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartListFragment.removeFromCart(cartItems.get(position));
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((OrderCreate)mCtx).openComponentEditPage(item);
            }
        });
        /*
        holder.noteSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               CartItem cartItem = cartItems.get(position);
                cartItem.comment = holder.comment.getText().toString();
                cartListFragment.setComment(cartItem);
            }
        });
        */
        if(active != null){
            if(item.localId == active.localId){
                holder.extentView.setVisibility(View.VISIBLE);
            }else{
                holder.extentView.setVisibility(View.GONE);
            }
        }else{
            holder.extentView.setVisibility(View.GONE);
        }

        if(item.componentSections.size()==0){
            holder.edit.setVisibility(View.INVISIBLE);
        }
//        else{
//            holder.applyPrice.setVisibility(View.INVISIBLE);
//        }
        if(item.type.equals("DYNAMIC") || item.category.equals("offer")){
            holder.edit.setVisibility(View.INVISIBLE);
        }

        if(item.comment.isEmpty()){
            holder.comment.setVisibility(View.GONE);
        }

        if (!cartItems.get(_position).isLocalItem) {
            holder.applyPrice.setVisibility(View.INVISIBLE);
            holder.ib_add_note.setVisibility(View.INVISIBLE);
            holder.edit.setVisibility(View.GONE);
            holder.delete.setVisibility(View.GONE);
            if (cartItems.get(_position).quantity > cartItems.get(_position).quantityOrg) {
                holder.decrease.setVisibility(View.VISIBLE);
            } else {
                holder.decrease.setVisibility(View.INVISIBLE);
            }
            if (cartItems.get(_position).paymentMethod.equals("CARD")) {
                holder.actionLayout.setVisibility(View.GONE);
                holder.comment.setVisibility(View.VISIBLE);
                holder.comment.setText("Paid");
            } else {
                holder.actionLayout.setVisibility(View.VISIBLE);
                holder.comment.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    @Override
    public void onClick(View v) {

    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView sl,name,price,quantity,componentsName;
        RelativeLayout extentView, actionLayout;
        ImageButton delete,increase,decrease,applyPrice,ib_add_note,edit;
        TextView comment;
        public OrderListViewHolder(View itemView) {
            super(itemView);
            extentView = itemView.findViewById(R.id.cartListExtentOptions);
            extentView.setVisibility(View.GONE);
            actionLayout = itemView.findViewById(R.id.action_layout);
            sl = itemView.findViewById(R.id.cartListItemSL);
            name = itemView.findViewById(R.id.cartListItemName);
            price = itemView.findViewById(R.id.cartListItemPriceTotal);
            quantity = itemView.findViewById(R.id.cartListExtendQuantity);
            decrease = itemView.findViewById(R.id.cartListExtendDecrease);
            increase = itemView.findViewById(R.id.cartListExtendIncrease);
            delete = itemView.findViewById(R.id.cartListExtendDelete);
            edit = itemView.findViewById(R.id.cartListExtendEdit);
            applyPrice = itemView.findViewById(R.id.iconPriceApply);

            componentsName = itemView.findViewById(R.id.cartListExtendComponentsName);
            comment = itemView.findViewById(R.id.cartListExtendNote);
            ib_add_note = itemView.findViewById(R.id.ib_add_note);
        }
    }
}