package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

public class AddressHelperModel {
    public String postcode,town,buildingNumber,buildingName,subBuildingName,departmentName,locality,organisationName,streetName1,streetName2;


    public AddressHelperModel(String postcode, String town, String buildingNumber, String buildingName, String subBuildingName, String departmentName, String locality, String organisationName, String streetName1, String streetName2) {
        this.postcode = postcode;
        this.town = town;
        this.buildingNumber = buildingNumber;
        this.buildingName = buildingName;
        this.subBuildingName = subBuildingName;
        this.departmentName = departmentName;
        this.locality = locality;
        this.organisationName = organisationName;
        this.streetName1 = streetName1;
        this.streetName2 = streetName2;
    }

    public static AddressHelperModel fromJSON(JSONObject jsonObject){
        return new AddressHelperModel(
            jsonObject.optString("postcode",""),
            jsonObject.optString("town",""),
            jsonObject.optString("buildingNumber",""),
            jsonObject.optString("buildingName",""),
            jsonObject.optString("subBuildingName",""),
            jsonObject.optString("departmentName",""),
            jsonObject.optString("locality",""),
            jsonObject.optString("organisationName",""),
            jsonObject.optString("thoroughfare",""),
            jsonObject.optString("dependentThoroughfare","")
        );
    }

}


//             {
//            "postcode": "LU3 1AB",
//            "town": "LUTON",
//            "locality": "",
//            "doubleLocality": "",
//            "thoroughfare": "Waldeck Road",
//            "dependentThoroughfare": "Nightingale Court",
//            "buildingNumber": "10",
//            "buildingName": "",
//            "subBuildingName": "",
//            "poBox": "",
//            "departmentName": "",
//            "organisationName": "",
//            "udprn": "14214841",
//            "type": "S",
//            "suOrgIndicator": " ",
//            "deliveryPointSuffix": "1N"
//            },