package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.view.activity.TableBookingActivity;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;


public class TableBookingAdapter extends RecyclerView.Adapter<TableBookingAdapter.ViewHolder> {
    Context mCtx;
    ArrayList<TableBookingModel> tableBookingModelArrayList;

    public TableBookingAdapter(Context mCtx, ArrayList<TableBookingModel> tableBookingModelArrayList) {
        this.mCtx = mCtx;
        this.tableBookingModelArrayList = tableBookingModelArrayList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.table_booking_item, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        TableBookingModel tableBookingModel = tableBookingModelArrayList.get(position);



        holder.tv_table_number.setText(String.valueOf(tableBookingModel.table_no));
        holder.tv_chair_number.setText("Chair : "+String.valueOf(tableBookingModel.capacity));
        holder.table_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TableBookingActivity) mCtx).getNoOfGuestInput(tableBookingModel);

            }
        });
        holder.table_card.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        ((TableBookingActivity) mCtx).bookOrFreeNow(tableBookingModel);
                        return true;
                    }
                }
        );

        if(!tableBookingModel.free){
            holder.ll_table.setBackgroundColor(mCtx.getResources().getColor(R.color.DarkGreen));
        }else{
            holder.ll_table.setBackgroundColor(mCtx.getResources().getColor(R.color.DarkRed));        }

    }



    @Override
    public int getItemCount() {
        return tableBookingModelArrayList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_table_number;
        TextView tv_chair_number;
        CardView table_card;
        LinearLayout ll_table;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_table_number = itemView.findViewById(R.id.tv_table_number);
            tv_chair_number = itemView.findViewById(R.id.tv_chair_number);
            table_card = itemView.findViewById(R.id.table_card);
            ll_table = itemView.findViewById(R.id.ll_table);
        }
    }

    public void updateAdapter(ArrayList<TableBookingModel> tableBookingModelArrayList) {
        if (tableBookingModelArrayList != null) {
            this.tableBookingModelArrayList = tableBookingModelArrayList;
        } else {
            this.tableBookingModelArrayList = new ArrayList<>();
        }
        notifyDataSetChanged();

        Log.e("TableBookings==>", "data updated");
    }
}
