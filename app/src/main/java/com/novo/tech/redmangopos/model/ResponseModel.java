package com.novo.tech.redmangopos.model;

public class ResponseModel {
    public int statusCode;
    public boolean success;
    public String message;

    public ResponseModel(int statusCode, boolean success, String message) {
        this.statusCode = statusCode;
        this.success = success;
        this.message = message;
    }
}
