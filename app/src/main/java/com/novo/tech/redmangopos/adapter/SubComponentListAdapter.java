package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.SubComponent;
import com.novo.tech.redmangopos.view.fragment.OrderComponentSubCartList;

import java.util.List;
import java.util.Locale;

public class SubComponentListAdapter extends RecyclerView.Adapter<SubComponentListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<SubComponent> subComponentList;
    OrderComponentSubCartList cartListFragment ;
    ComponentSection section;
    int select_position;
    RefreshCallBack callBack;
    public SubComponentListAdapter(Context mCtx, List<SubComponent> subComponentList, ComponentSection section, RefreshCallBack callBack) {
        this.mCtx = mCtx;
        this.subComponentList = subComponentList;
        this.section = section;
        this.callBack = callBack;
        cartListFragment = (OrderComponentSubCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_components, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        int _position = holder.getAdapterPosition();
        SubComponent component = subComponentList.get(_position);
        String price = String.format(Locale.getDefault(),"%.2f",component.price);
        holder.productTitle.setText(component.shortName);
        holder.productPrice.setText("£ "+price);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
                        onItemClick(_position);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (_position == select_position)
                            holder.catImage.setBackgroundColor(Color.parseColor("#DC143C"));
                        else
                            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
                        select_position = _position;
                        notifyDataSetChanged();
                        break;
                }


                return true;
            }

        });


    }

    void onItemClick(int position){
        section.selectedItem.subComponentSelected = subComponentList.get(position);
        cartListFragment.onComponentChange(section);
        callBack.onChanged();

    }



    @Override
    public int getItemCount() {
        return subComponentList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
        }
    }
}
