package com.novo.tech.redmangopos.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.model.TransactionModel;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DBOrderManager {
    private DatabaseHelper dbHelper;
    private Context context;
    private SQLiteDatabase database;
    RefreshCallBack callBack;
    String[] columns = new String[] { "id","order_id","order_channel", "date", "date", "orderDateTime", "requestedDeliveryDateTime","currentDeliveryDateTime",
            "deliveryType", "order_type", "order_status", "payment_method", "payment_status", "subTotal", "total", "discount","discountCode", "tips",
            "receive", "cartItems", "cloudSubmitted","comments","shift","server_id","customerId","customerInfo","cashId","paymentId", "deliveryCharge",
            "discountPercentage","refund","adjustment","adjustmentNote","requester_type","dueAmount","paidTotal","plasticBagCost","containerBagCost","tableBookingId","discountableTotal",
            "tableBookingInfo"};

    public DBOrderManager(Context c) {
        context = c;
    }
    public DBOrderManager(Context c, RefreshCallBack callBack) {
        this.callBack = callBack;
        context = c;
    }

    public DBOrderManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public ContentValues getOrderContent(OrderModel orderModel){
        if(orderModel.comments == null){
            orderModel.comments="";
        }
        if(orderModel.shift==0){
            orderModel.shift = SharedPrefManager.getCurrentShift(context);
        }

        if(orderModel.orderDateTime==null){
            Log.d("mmmmm", "getOrderContent: "+orderModel.order_id);
        }
        ContentValues contentValue = new ContentValues();
        contentValue.put("order_id", orderModel.order_id);
        contentValue.put("server_id", orderModel.serverId);
        contentValue.put("server_id", orderModel.serverId);
        contentValue.put("order_channel", orderModel.order_channel);
        contentValue.put("date",DateTimeHelper.formatDBDate(orderModel.orderDateTime));
        contentValue.put("orderDateTime", orderModel.orderDateTime);
        contentValue.put("requestedDeliveryDateTime", orderModel.requestedDeliveryTime);
        contentValue.put("currentDeliveryDateTime", orderModel.currentDeliveryTime);
        contentValue.put("deliveryType", orderModel.deliveryType);
        contentValue.put("requester_type", orderModel.requester_type);
        contentValue.put("order_type", orderModel.order_type);
        contentValue.put("order_status", orderModel.order_status);
        contentValue.put("payment_method", orderModel.paymentMethod);
        contentValue.put("payment_status", orderModel.paymentStatus);
        contentValue.put("comments", orderModel.comments);
        contentValue.put("subTotal", orderModel.subTotal);
        contentValue.put("total", orderModel.total);
        contentValue.put("discount", orderModel.discountAmount);
        contentValue.put("discountCode", orderModel.discountCode);
        contentValue.put("plasticBagCost", orderModel.plasticBagCost);
        contentValue.put("adjustment", orderModel.adjustmentAmount);
        contentValue.put("adjustmentNote", orderModel.adjustmentNote);
        contentValue.put("discountPercentage", orderModel.discountPercentage);
        contentValue.put("refund", orderModel.refundAmount);
        contentValue.put("discountPercentage", orderModel.discountPercentage);
        contentValue.put("deliveryCharge", orderModel.deliveryCharge);
        contentValue.put("tips", orderModel.tips);
        contentValue.put("shift", orderModel.shift);
        contentValue.put("dueAmount", orderModel.dueAmount);
        contentValue.put("paidTotal", orderModel.totalPaid);
        contentValue.put("receive", orderModel.receive);
        contentValue.put("paymentId", orderModel.paymentUid==null?"-1":orderModel.paymentUid);
        contentValue.put("cashId", orderModel.cashId==0?-1:orderModel.cashId);
        contentValue.put("customerId", orderModel.customer==null?0:orderModel.customer.dbId);
        contentValue.put("customerInfo", orderModel.customer!=null?GsonParser.getGsonParser().toJson(orderModel.customer):"");
        contentValue.put("tableBookingId", orderModel.bookingId);
        contentValue.put("tableBookingInfo", orderModel.tableBookingModel!=null?GsonParser.getGsonParser().toJson(orderModel.tableBookingModel):"");
        contentValue.put("cloudSubmitted", 0);
        contentValue.put("cartItems", GsonParser.getGsonParser().toJson(orderModel.items));
        return contentValue;
    }

    public int addOrUpdateOrder(OrderModel orderModel) {
        String[] columns = new String[] { "id"};
        int db_id = 0;
        try{
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "id=? ", new String[]{String.valueOf(orderModel.db_id)}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                update(orderModel);
                db_id = orderModel.db_id;
                Log.d("mmmm", "updating order Id  "+orderModel.order_id);
            }else{
                orderModel.order_id = getOrderMax()+1;
                long rowId = database.insert(DatabaseHelper.TABLE_NAME, null, getOrderContent(orderModel));
                Log.d("mmmm", "Inserted at  "+rowId);
                db_id = (int)rowId;
            }

        }}catch (Exception e){
            FirebaseCrashlytics.getInstance().recordException(e);
        }
        return db_id;
    }
    public int getOrderMax() {
        List<String> _date = DateTimeHelper.getTimeRange(context);
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(MAX(order_id),0) as max from "+DatabaseHelper.TABLE_NAME+" WHERE date BETWEEN ? AND ?",new String[]{_date.get(0), _date.get(1)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("max"));
            }
        }
        assert cursor != null;
        cursor.close();
        Log.d("mmmm", "getOrderMax: "+_date+"  "+max);
        return max;
    }

    public void deleteOldOrder(int day) {
        open();
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -day);
        Date date = calendar.getTime();
        String _lastDate  = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
        Cursor cursor = database.rawQuery("DELETE from "+DatabaseHelper.TABLE_NAME+" WHERE date < ?",new String[]{_lastDate});
        assert cursor != null;
        cursor.close();
        close();
    }

    public OrderModel getOrderData(int id) {
        OrderModel order = new OrderModel();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "id=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            if(cursor.moveToFirst()){
                 order = getOrderDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        return order;
    }
    public List<OrderModel> fetchDashboardData() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status!=? AND order_status!=?", new String[]{"CLOSED","CANCELLED"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> fetchDashboardClosedData() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=?", new String[]{"CLOSED"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> fetchDashboardRefundData() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=?", new String[]{"CANCELLED"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> fetchCustomerOrderData(String dbId) {
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "customerId=?", new String[]{dbId}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        return orders;
    }
    public List<OrderModel> fetchAllOrderData() {
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "cloudSubmitted=?", new String[]{"0"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        return orders;
    }
    public List<OrderModel> getAllOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> getActiveOrder() {
        List<OrderModel> orders = new ArrayList<>();

        open();
        List<String> dates = DateTimeHelper.getTimeRange(context);
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status !=? AND order_status!=? AND order_status!=? AND date BETWEEN ? AND ? ORDER BY order_id DESC LIMIT 0,30", new String[]{"CANCELLED","CLOSED","REFUNDED",dates.get(0), dates.get(1)}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();

        Log.e("orders===>", orders.size() + "");

        return orders;
    }
    public void closeAllActiveOrder() {
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put("order_status", "CLOSED");
        int i = database.update(DatabaseHelper.TABLE_NAME,contentValue,"order_status !=? AND order_status!=? AND order_status!=?", new String[]{"CANCELLED","CLOSED","REFUNDED"});
        close();
    }

    public List<OrderModel> getCloseOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=? ORDER BY date DESC LIMIT 0,30", new String[]{"CLOSED"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if(!orderModel.order_status.equals("REFUNDED"))
                        orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }

    public List<OrderModel> getRefundedOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=? OR order_status=? ORDER BY date DESC LIMIT 0,30", new String[]{"CLOSED","REFUNDED"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if(orderModel.order_status.equals("REFUNDED"))
                        orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> getCanceledOrder() {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_status=? ORDER BY date DESC LIMIT 0,30", new String[]{"CANCELLED"}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> getSearchOrder(String orderID) {
        open();
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, "order_id=?", new String[]{orderID}, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    orders.add(orderModel);
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return orders;
    }
    public List<OrderModel> getCustomerOrders(CustomerModel customerModel) {
        List<OrderModel> orders = new ArrayList<>();
        Cursor cursor = database.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    OrderModel orderModel = getOrderDataFromCursor(cursor);
                    if(orderModel.customer != null)
                        if(customerModel.dbId== orderModel.customer.dbId){
                            orders.add(orderModel);
                        }// do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        return orders;
    }

    public int update(OrderModel orderModel) {
        int i = database.update(DatabaseHelper.TABLE_NAME, getOrderContent(orderModel), "id=?", new String[]{String.valueOf(orderModel.db_id)});
        return i;
    }
    public void updateAfterPushed(OrderModel orderModel) {
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put("cloudSubmitted", false);
        contentValue.put("server_id", orderModel.serverId);
        int id  = database.update(DatabaseHelper.TABLE_NAME, contentValue, "id=?", new String[]{String.valueOf(orderModel.db_id)});
        Log.d("mmmm", "updateAfterPushed: updated"+id);
        close();
    }
    public void updateFromServerStatus(OrderModel orderModel) {
        open();
        ContentValues contentValue = new ContentValues();
        contentValue.put("order_status", orderModel.order_status);
        contentValue.put("cloudSubmitted", false);
        contentValue.put("server_id", orderModel.serverId);
        int id  = database.update(DatabaseHelper.TABLE_NAME, contentValue, "id=?", new String[]{String.valueOf(orderModel.db_id)});
        Log.d("mmmm", "updateFromServerStatus: updated "+id);
        close();
    }

    public void delete(OrderModel orderModel) {
        open();
        database.delete(DatabaseHelper.TABLE_NAME,  "id =" + orderModel.db_id, null);
        close();
    }
    private OrderModel getOrderDataFromCursor(Cursor cursor){
        List<CartItem> cartItemList = new ArrayList<>();
        CustomerModel customerModel = null;
        TableBookingModel tableBookingModel = null;
        int db_id = cursor.getInt(cursor.getColumnIndex("id"));
        int order_id = cursor.getInt(cursor.getColumnIndex("order_id"));
        int serverId = cursor.getInt(cursor.getColumnIndex("server_id"));
        String  date = cursor.getString(cursor.getColumnIndex("date"));
        String  orderDateTime = cursor.getString(cursor.getColumnIndex("orderDateTime"));
        String  requestedDeliveryDateTime = cursor.getString(cursor.getColumnIndex("requestedDeliveryDateTime"));
        String  currentDeliveryDateTime = cursor.getString(cursor.getColumnIndex("currentDeliveryDateTime"));
        String  deliveryType = cursor.getString(cursor.getColumnIndex("deliveryType"));
        String  requester_type = cursor.getString(cursor.getColumnIndex("requester_type"));
        String  order_type = cursor.getString(cursor.getColumnIndex("order_type"));
        String  order_status = cursor.getString(cursor.getColumnIndex("order_status"));
        String  order_channel = cursor.getString(cursor.getColumnIndex("order_channel"));
        String  payment_method = cursor.getString(cursor.getColumnIndex("payment_method"));
        String  payment_status = cursor.getString(cursor.getColumnIndex("payment_status"));
        String  comments = cursor.getString(cursor.getColumnIndex("comments"));
        String  discountCode = cursor.getString(cursor.getColumnIndex("discountCode"));
        int  cashId = cursor.getInt(cursor.getColumnIndex("cashId"));
        String  paymentId = cursor.getString(cursor.getColumnIndex("paymentId"));
        String  _cartItems = cursor.getString(cursor.getColumnIndex("cartItems"));
        int  shift = cursor.getInt(cursor.getColumnIndex("shift"));
        double  dueAmount = cursor.getDouble(cursor.getColumnIndex("dueAmount"));
        double  paidTotal = cursor.getDouble(cursor.getColumnIndex("paidTotal"));
        double  subTotal = cursor.getDouble(cursor.getColumnIndex("subTotal"));
        double  total = cursor.getDouble(cursor.getColumnIndex("total"));
        double  discountableTotal = cursor.getDouble(cursor.getColumnIndex("discountableTotal"));
        double  discount = cursor.getDouble(cursor.getColumnIndex("discount"));
        double  adjustment = cursor.getDouble(cursor.getColumnIndex("adjustment"));
        double  plasticBagCost = cursor.getDouble(cursor.getColumnIndex("plasticBagCost"));
        double  containerBagCost = cursor.getDouble(cursor.getColumnIndex("containerBagCost"));
        String  adjustmentNote = cursor.getString(cursor.getColumnIndex("adjustmentNote"));
        int  discountPercentage = cursor.getInt(cursor.getColumnIndex("discountPercentage"));
        double  tips = cursor.getDouble(cursor.getColumnIndex("tips"));
        double  receive = cursor.getDouble(cursor.getColumnIndex("receive"));
        double  deliveryCharge = cursor.getDouble(cursor.getColumnIndex("deliveryCharge"));
        double  refund = cursor.getDouble(cursor.getColumnIndex("refund"));
        boolean  cloudSubmitted = cursor.getInt(cursor.getColumnIndex("cloudSubmitted")) != 0;

        int bookingId = cursor.getInt(cursor.getColumnIndex("tableBookingId"));
        String tableBookingInfo = cursor.getString(cursor.getColumnIndex("tableBookingInfo"));
        int customerId = cursor.getInt(cursor.getColumnIndex("customerId"));
        String _customerInfo = cursor.getString(cursor.getColumnIndex("customerInfo"));


        if(customerId != 0 && customerId != -1){
            DBCustomerManager customerManager = new DBCustomerManager(context);
            customerModel = customerManager.getCustomerData(customerId);
        }else{
            customerModel = GsonParser.getGsonParser().fromJson(_customerInfo,CustomerModel.class);
        }
        try{
            customerModel = GsonParser.getGsonParser().fromJson(_customerInfo,CustomerModel.class);

            if(bookingId !=0){
                tableBookingModel = GsonParser.getGsonParser().fromJson(tableBookingInfo, TableBookingModel.class);
            }
        }catch (Exception e){
            e.printStackTrace();
        }



        try{
            JSONArray _data =new JSONArray(_cartItems.replaceAll("NaN","0.0"));
            for (int i =0;i<_data.length();i++){
                cartItemList.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)),CartItem.class));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(order_type==null){
            order_type = "N/A";
        }

        double cashPayment = 0;
        double cardPayment = 0;
        DBCashManager cashManager = new DBCashManager(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(db_id);

        for (TransactionModel model: allTransactions) {
            if(model.inCash==0){
                cardPayment+=model.amount;
            }else{
                cashPayment+=model.amount;
            }
        }

        OrderModel orderModel = new OrderModel();
        double subTotalAmount = 0;
        double _discountableTotal = 0;
        for (CartItem item : orderModel.items){
            if(item.uuid.equals("plastic-bag")){
                orderModel.plasticBagCost = item.quantity * item.price;
            }else if(item.uuid.equals("container")){
                orderModel.containerBagCost = item.quantity * item.price;
            }else{
                double price = 0;
                if(!item.offered)
                    price=(item.subTotal*item.quantity);
                else
                    price = item.total;

                subTotalAmount+=price;
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.total = subTotalAmount+orderModel.deliveryCharge+orderModel.adjustmentAmount-orderModel.refundAmount-orderModel.discountAmount+orderModel.tips+orderModel.plasticBagCost+orderModel.containerBagCost;

        if(refund>=total){
            orderModel.order_status = "REFUNDED";
        }
        return orderModel;
    }

    public ContentValues getOnlineOrderContent(OrderModel orderModel){
        if(orderModel.comments == null){
            orderModel.comments="";
        }
        if(orderModel.shift<=0){
            orderModel.shift = SharedPrefManager.getCurrentShift(context);
        }

        double subTotalAmount = 0;
        for (CartItem item : orderModel.items){
            double price = 0;
            if(!item.offered)
                price=(item.subTotal*item.quantity);
            else
                price = item.total;

            subTotalAmount+=price;
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.total = subTotalAmount+orderModel.deliveryCharge+orderModel.adjustmentAmount-orderModel.refundAmount-orderModel.discountAmount+orderModel.tips;

        String paymentStatus = "UNPAID";
        if(orderModel.paymentMethod.equals("CARD")){
            paymentStatus = "PAID";
            orderModel.totalPaid = orderModel.total;
            orderModel.receive = orderModel.totalPaid;
            orderModel.cardPaid = orderModel.totalPaid;
            DBCashManager cashManager = new DBCashManager(context);
            cashManager.open();
            cashManager.addPaidTransaction(context,orderModel.total,0,orderModel.db_id,false);
            cashManager.close();
        }

        ContentValues contentValue = new ContentValues();
        contentValue.put("order_id", orderModel.order_id);
        contentValue.put("server_id", orderModel.serverId);
        contentValue.put("order_channel", orderModel.order_channel);
        contentValue.put("date",orderModel.orderDate.length()==8?orderModel.orderDate:DateTimeHelper.formatOnlyDBDate(orderModel.orderDate));
        contentValue.put("orderDateTime", orderModel.orderDateTime);
        contentValue.put("requestedDeliveryDateTime", orderModel.requestedDeliveryTime);
        contentValue.put("currentDeliveryDateTime", orderModel.currentDeliveryTime);
        contentValue.put("deliveryType", orderModel.deliveryType);
        contentValue.put("order_type", orderModel.order_type);
        contentValue.put("order_status", orderModel.order_status);
        contentValue.put("payment_method", orderModel.paymentMethod);
        contentValue.put("dueAmount", orderModel.dueAmount);
        contentValue.put("payment_status", paymentStatus);
        contentValue.put("comments", orderModel.comments);
        contentValue.put("subTotal", orderModel.subTotal);
        contentValue.put("total", orderModel.total);
        contentValue.put("paidTotal", orderModel.totalPaid);
        contentValue.put("discount", orderModel.discountAmount);
        contentValue.put("discountCode", orderModel.discountCode);
        contentValue.put("plasticBagCost", orderModel.plasticBagCost);
        contentValue.put("containerBagCost", orderModel.containerBagCost);
        contentValue.put("adjustment", orderModel.adjustmentAmount);
        contentValue.put("adjustmentNote", orderModel.adjustmentNote);
        contentValue.put("discountPercentage", orderModel.discountPercentage);
        contentValue.put("deliveryCharge", orderModel.deliveryCharge);
        contentValue.put("tips", orderModel.tips);
        contentValue.put("shift", orderModel.shift==-1?SharedPrefManager.getCurrentShift(context):orderModel.shift);
        contentValue.put("paymentId", orderModel.paymentUid==null?"-1":orderModel.paymentUid);
        contentValue.put("cashId", orderModel.cashId==0?-1:orderModel.cashId);
        contentValue.put("receive", orderModel.receive);
        contentValue.put("cloudSubmitted", 1);
        contentValue.put("customerInfo", orderModel.customer!=null?GsonParser.getGsonParser().toJson(orderModel.customer):"");
        contentValue.put("customerId", orderModel.customer!=null?String.valueOf(orderModel.customer.dbId):"");

        contentValue.put("tableBookingId", orderModel.bookingId);
        contentValue.put("tableBookingInfo", orderModel.tableBookingModel!=null?GsonParser.getGsonParser().toJson(orderModel.tableBookingModel):"");

        contentValue.put("cartItems", GsonParser.getGsonParser().toJson(orderModel.items));
        return contentValue;
    }
    public int getOnlineOrderMax(String _date) {
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(MAX(order_id),0) as max from "+DatabaseHelper.TABLE_NAME+" WHERE date = ?",new String[]{_date});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("max"));
            }
        }
        assert cursor != null;
        cursor.close();
        return max;
    }

    public int onlineOrderExist(int serverId) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from "+DatabaseHelper.TABLE_NAME+" WHERE server_id = ?",new String[]{String.valueOf(serverId)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }
}
