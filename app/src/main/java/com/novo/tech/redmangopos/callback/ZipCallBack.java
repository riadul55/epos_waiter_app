package com.novo.tech.redmangopos.callback;


import com.novo.tech.redmangopos.model.AddressHelperModel;

import java.util.ArrayList;

public interface ZipCallBack {
    void onResponseFromServer(ArrayList<String> str);
    void onAddressResponse(ArrayList<AddressHelperModel> data);
}
