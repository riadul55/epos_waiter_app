package com.novo.tech.redmangopos.view.fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.evgenii.jsevaluator.JsEvaluator;
import com.evgenii.jsevaluator.interfaces.JsCallback;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.AddressListAdapter;
import com.novo.tech.redmangopos.adapter.CustomerHelperNameAdapter;
import com.novo.tech.redmangopos.adapter.CustomerHelperPhoneAdapter;
import com.novo.tech.redmangopos.adapter.CustomerHelperZipAdapter;
import com.novo.tech.redmangopos.callback.CustomerSelectCallBack;
import com.novo.tech.redmangopos.callback.ZipCallBack;
import com.novo.tech.redmangopos.model.AddressHelperModel;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.CustomerProfile;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.sync.AddressApi;
import com.novo.tech.redmangopos.sync.ZipCodeHelper;

import java.util.ArrayList;
import java.util.List;

public class CustomerInput extends Fragment implements ZipCallBack {

    public EditText lastName,email,city,houseNumber,town,street;
    public AutoCompleteTextView phone,firstName;
    public Spinner addressSelect;
    public Button saveBtn,selectButton,addressSearch;
    public CustomerModel customerModel;
    public AutoCompleteTextView zip;
    CustomerSearch customerSearch;
    ArrayAdapter houseAdapter;
    CustomerHelperPhoneAdapter customerHelperPhoneAdapter;
    CustomerHelperNameAdapter customerHelperNameAdapter;
    CustomerHelperZipAdapter customerHelperZipAdapter;
    ArrayList<String> zipList = new ArrayList<String>();
    ArrayList<AddressHelperModel> houseList = new ArrayList<AddressHelperModel>();
    JsEvaluator jsEvaluator;
    String preStr="";
    Context context;
    DBCustomerManager customerManager = new DBCustomerManager(getContext());



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_customer_input, container, false);
        context = getActivity();
        firstName = v.findViewById(R.id.customerInputFirstName);
        lastName = v.findViewById(R.id.customerInputLastName);
        phone = v.findViewById(R.id.customerInputPhone);
        email = v.findViewById(R.id.customerInputEmail);
        street = v.findViewById(R.id.customerInputStreetName);
        city = v.findViewById(R.id.customerInputCity);
        zip = v.findViewById(R.id.customerInputZip);
        town = v.findViewById(R.id.customerInputCity);
        houseNumber = v.findViewById(R.id.customerInputHouseNumber);
        saveBtn = v.findViewById(R.id.customerSave);
        selectButton = v.findViewById(R.id.customerSelect);
        addressSearch = v.findViewById(R.id.addressSearch);
        addressSelect = v.findViewById(R.id.addressSelect);
        customerSearch = (CustomerSearch) (getActivity()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerSelect);

        jsEvaluator = new JsEvaluator(getContext());

        firstName.setEnabled(true);
        saveBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                        addCustomer();

                    }
                }
        );
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(customerModel != null)
                    customerSearch.backToOrder(customerModel);
            }
        });
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus && !email.getText().toString().isEmpty()){
                    DBCustomerManager manager = new DBCustomerManager(getContext());
                    int id  = manager.customerExistWithMail(email.getText().toString());

                    Log.d("mmmmmm", "onFocusChange: "+id);

                    if(id > 0){
                        email.setError("Email Already Exist");
                    }
                }
            }
        });
        customerManager = new DBCustomerManager(getContext());
        customerManager.open();
        List<CustomerModel> customerModelList =  customerManager.getAllCustomerData();
        customerModelList.removeIf((a)->{
           if(a.profile==null){
               return true;
           } else if(a.profile.first_name.isEmpty()){
               return true;
           }else if(a.profile.phone.isEmpty()){
               return true;
           }else return false;
        });
        customerManager.close();


        customerHelperPhoneAdapter = new CustomerHelperPhoneAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, customerModelList, new CustomerSelectCallBack() {
            @Override
            public void onCustomerSelect(CustomerModel customerModel) {
                onCustomerSelectFromSuggestion(customerModel);
                phone.setSelection(phone.getText().toString().length());
                phone.dismissDropDown();
            }
        });
        phone.setAdapter(customerHelperPhoneAdapter);
        phone.setThreshold(3);

        customerHelperNameAdapter = new CustomerHelperNameAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, customerModelList, new CustomerSelectCallBack() {
            @Override
            public void onCustomerSelect(CustomerModel customerModel) {
                onCustomerSelectFromSuggestion(customerModel);
                firstName.setSelection(firstName.getText().toString().length());
                firstName.dismissDropDown();
            }
        });
        firstName.setAdapter(customerHelperNameAdapter);
        firstName.setThreshold(3);

        customerHelperZipAdapter = new CustomerHelperZipAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, zipList);
        zip.setAdapter(customerHelperZipAdapter);
        zip.setThreshold(1);

        addressSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zip.dismissDropDown();
                loadHouse();
            }
        });
        zip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==3 && !preStr.equals(s.toString())){
                    preStr = s.toString();
                    loadData(s.toString());
                    //callFormatPostalCode(s.toString());
                }
                if (s.length()>=3){
                  //  callFormatPostalCode(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("mmmmm", "afterTextChanged: called");

            }
        });
        return  v;
    }

    private void loadHouse(){
        Log.d("ppppp", "loadHouse: calling");

        jsEvaluator.callFunction("function myFunction(a) { return a.replace(/(\\S*)\\s*(\\d)/, \"$1 $2\"); }",
                new JsCallback() {
                    @Override
                    public void onResult(String result) {
                        zip.setText(result.toUpperCase());
                        zip.setSelection(zip.getText().length());
                    }

                    @Override
                    public void onError(String errorMessage) {
                        // Process JavaScript error here.
                        // This method is called in the UI thread.
                    }
                }, "myFunction", zip.getText().toString());




        new AddressApi(zip.getText().toString(),this).execute();

    }

    void onCustomerSelectFromSuggestion(CustomerModel model){

        customerModel = model;
        if(model.profile!=null){
            firstName.setText(model.profile.first_name);
            lastName.setText(model.profile.last_name);
            phone.setText(model.profile.phone);
            email.setText(model.email);
        }

        if(model.addresses != null){
            if(model.addresses.size()!=0){
                CustomerAddress address = model.addresses.get(0);
                if(address.properties != null){
                    street.setText(address.properties.street_number+" "+address.properties.street_name);
                    town.setText(address.properties.city);
                    zip.setText(address.properties.zip);
                    houseNumber.setText(address.properties.building);
                }
            }
        }
        selectButton.setVisibility(View.VISIBLE);

    }

    void callFormatPostalCode(String str){
        if (str.length()>3) {
            jsEvaluator.callFunction("function myFunction(a) { return a.replace(/(\\S*)\\s*(\\d)/, \"$1 $2\"); }",
                    new JsCallback() {
                        @Override
                        public void onResult(String result) {

                        if(result.replaceAll(" ","").length() >3){
                           // loadData(result);
                        }else{
                                //loadData(zip.getText().toString());
                               // loadData(zip.getText().toString());
                            }
                        }

                        @Override
                        public void onError(String errorMessage) {
                            // Process JavaScript error here.
                            // This method is called in the UI thread.
                        }
                    }, "myFunction", str);
        }else {
            loadData(zip.getText().toString());
        }
    }
    void loadData(String str){
        new ZipCodeHelper(getContext(),str,this).execute();;
    }

    void addCustomer(){
        DBCustomerManager manager = new DBCustomerManager(getContext());

        DBCustomerManager dbCustomerManager = new DBCustomerManager(getContext());
        CustomerProfile profile = new CustomerProfile(
              phone.getText().toString(),
              lastName.getText().toString(),
              firstName.getText().toString(),
              email.getText().toString()
        );
        CustomerAddressProperties addressProperties = new CustomerAddressProperties(
              zip.getText().toString(),
              "United Kingdom",
              city.getText().toString(),
              "",
              "",
                houseNumber.getText().toString(),
              street.getText().toString()
        );
        CustomerAddress address = new CustomerAddress(
                0,
                0,
                null,
                null,
                false,
                addressProperties
        );
        List<CustomerAddress> addressList = new ArrayList<>();
        addressList.add(0,address);
        CustomerModel customer = new CustomerModel(
                -1,
                null,
                email.getText().toString(),
                email.getText().toString(),
                profile,
                addressList,false
        );

        if(customerModel == null){
            int id = 0;
            if(!phone.getText().toString().isEmpty())
                id = dbCustomerManager.customerExist(phone.getText().toString());
            if(id < 1){
                if(!email.getText().toString().isEmpty()){
                    int idEmail  = manager.customerExistWithMail(email.getText().toString());
                    if(idEmail > 0){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                        alertDialog.setTitle("Email Already Exist. Please use existing customer profile or remove email address from the field for guest checkout")
                                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .create()
                                .show();
                        return;
                    }else{
                        customer.dbId = dbCustomerManager.addNewCustomer(customer,false);
                        backToOrder(customer);
                        clear();                        }
                }else{
                    customer.dbId = dbCustomerManager.addNewCustomer(customer,false);
                    backToOrder(customer);
                    clear();
                }

            }else{
                customerSearch.customerNamePhone.setText(phone.getText().toString());
                customerSearch.getCustomerList(phone.getText().toString());
                Toast.makeText(getContext(),"Phone Number Already Exist",Toast.LENGTH_LONG).show();
            }

        }else{
            customer.dbId = customerModel.dbId;
            if(customerModel.consumer_uuid != null){
                customer.consumer_uuid = customerModel.consumer_uuid;
                if(customerModel.addresses.size()!=0){
                    if(customerModel.addresses.get(0).address_uuid!= null){
                        customer.addresses.get(0).address_uuid = customerModel.addresses.get(0).address_uuid;
                    }
                }
            }
            dbCustomerManager.updateCustomer(customer,false);
            clear();

        }
        customerSearch.setCustomerListView();
    }
    void backToOrder(CustomerModel customerModel){
        customerSearch.backToOrder(customerModel);
//        if(customerModel.profile != null){
//            CustomerProfile profile = customerModel.profile;
//            if((!profile.first_name.isEmpty() || !profile.last_name.isEmpty() ) && !profile.phone.isEmpty()){
//
//            }
//        }
    }
    void clear(){
        customerModel = null;
        firstName.setText("");
        lastName.setText("");
        phone.setText("");
        email.setText("");
        street.setText("");
        city.setText("");
        zip.setText("");
        houseNumber.setText("");
    }

    @Override
    public void onResponseFromServer(ArrayList<String> str) {
        if(getContext() != null){
            if(str.size() != 0){
                zipList = str;
            }
            customerHelperZipAdapter = new CustomerHelperZipAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, zipList);
            //zip.setAdapter(arrayAdapter);
            zip.setAdapter(customerHelperZipAdapter);
        }

        if(getContext() !=null) {
            customerHelperZipAdapter = new CustomerHelperZipAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, android.R.id.text1, zipList);
            //zip.setAdapter(arrayAdapter);
            zip.setAdapter(customerHelperZipAdapter);
        }
        zip.showDropDown();
    }

    @Override
    public void onAddressResponse(ArrayList<AddressHelperModel> data) {
        houseList = data;
        try{
            houseAdapter = new AddressListAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, houseList);
            addressSelect.setAdapter(houseAdapter);
            addressSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    AddressHelperModel model =  houseList.get(position);
                    street.setText(model.streetName1+" "+model.streetName2);
                   // et_street_name.setText(model.streetName1+" "+model.streetName2);
                    city.setText(model.town);
                    //et_city.setText(model.town);
                    houseNumber.setText(model.buildingNumber+" "+model.buildingName);
                    //et_building_number.setText(model.buildingNumber);

                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}