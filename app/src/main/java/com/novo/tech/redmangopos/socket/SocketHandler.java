package com.novo.tech.redmangopos.socket;

import android.util.Log;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;

public class SocketHandler {
    private final String TAG = SocketHandler.class.getSimpleName();
    private final String SOCKET_URL = "http://157.90.23.88:5000";
    public static final String SOCKET_LISTENER_ACTION = "socket.io.epos.action";

    //socket emmit keys
    public static final String EMIT_ORDER_LIST = "ORDER_LIST";
    public static final String EMIT_ORDER_CREATE = "ORDER_CREATE";
    public static final String EMIT_ORDER_UPDATE = "ORDER_UPDATE";
    public static final String EMIT_LISTEN_SUCCESS = "ORDER_SUCCESS";
    public static final String EMIT_ORDER_PRINT = "ORDER_PRINT";

    public static final String EMIT_GET_ITEMS = "GET_ORDER_ITEMS";
    public static final String EMIT_TABLE_BOOKED = "SET_TABLE_BOOKED";

    //socket listener keys
    public static final String LISTEN_ORDER_LIST= "ORDER_LIST_";
    public static final String LISTEN_ORDER_CREATE = "ORDER_CREATE_";
    public static final String LISTEN_ORDER_UPDATE = "ORDER_UPDATE_";
    public static final String LISTEN_SUCCESS = "ORDER_SUCCESS_";

    public static final String LISTEN_MERGED_ITEMS = "MERGED_ORDER_ITEMS_";

    public static final String LISTEN_GET_BOOKINGS = "GET_BOOKING_LIST_";

    //JSON keys
    public static final String PROVIDER_NAME = "provider_name";
    public static final String ORDER = "order";
    public static final String ORDER_LIST = "orderList";

    private Socket socket;

    public Socket init() {
        try {
            socket = IO.socket(SOCKET_URL);
        } catch (URISyntaxException e) {
            Log.e(TAG, e.getMessage());
        }
        return socket;
    }

    public void connect() {
        socket.connect();
    }
}
