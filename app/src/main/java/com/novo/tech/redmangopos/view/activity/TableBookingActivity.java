package com.novo.tech.redmangopos.view.activity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.TableBookingAdapter;
import com.novo.tech.redmangopos.callback.CustomerExportCallBack;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.receivers.NetworkChangeReceiver;
import com.novo.tech.redmangopos.retrofit2.DashboardApi;
import com.novo.tech.redmangopos.retrofit2.ProductApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.socket.JsonToOrderModel;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.CustomerExport;
import com.novo.tech.redmangopos.sync.CustomerImport;
import com.novo.tech.redmangopos.sync.TableBookingApiHelper;
import com.novo.tech.redmangopos.util.AppConstant;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.socket.emitter.Emitter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class TableBookingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final int ORDER_CREATE_RESULT_CODE = 771;

    ArrayList<TableBookingModel> tableBookingModelArrayList = new ArrayList<>();
    LinearLayout drawerButton;
    RecyclerView rv_table_booking_list;
    TableBookingAdapter adapter;
    DrawerLayout drawerLayout;
    NavigationView navigationViewDrawer;
    AppCompatButton orderList, btn_collectionOrder, btnDeliveryOrder;
    TextView noInternet;

    ProgressDialog progress;


    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver mNetworkReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_booking);
        rv_table_booking_list = findViewById(R.id.rv_table_booking_list);
        drawerButton = findViewById(R.id.drawerButton);
        btn_collectionOrder = findViewById(R.id.btn_collectionOrder);
        btnDeliveryOrder = findViewById(R.id.btn_DeliveryOrder);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationViewDrawer = findViewById(R.id.navigation_view_left);
        orderList = findViewById(R.id.btn_orderList);
        noInternet = findViewById(R.id.noInternet);
        navigationViewDrawer.setNavigationItemSelectedListener(this);

        drawerButton.setOnClickListener(v -> drawerLayout.openDrawer(Gravity.LEFT));
        btn_collectionOrder.setOnClickListener(v -> openOrderPage("TAKEOUT"));
        btnDeliveryOrder.setOnClickListener(v -> {
            openOrderPage("DELIVERY");
        });
        orderList.setOnClickListener(v -> openOrderListPage());

        progress = new ProgressDialog(this);
//        progress.setMessage("Loading");
//        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
//        progress.show();


        for (int i=0;i<20;i++) {
            TableBookingModel table = new TableBookingModel(i, 1, i, 100, false, 1, 0, DateTimeHelper.getTime());
            tableBookingModelArrayList.add(table);
        }
        adapter = new TableBookingAdapter(TableBookingActivity.this, tableBookingModelArrayList);
        rv_table_booking_list.setHasFixedSize(true);
        rv_table_booking_list.setLayoutManager(new GridLayoutManager(TableBookingActivity.this, 7));
        rv_table_booking_list.addItemDecoration(new MediaSpaceDecoration(18, 2));
        rv_table_booking_list.setAdapter(adapter);

//        getTableBookings(true);


//        BaseApp.webSocket.off(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business);
//        BaseApp.webSocket.on(SocketHandler.LISTEN_ORDER_LIST + AppConstant.business, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
////                        getTableBookings(false);
//                    }
//                });
//            }
//        });

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put(SocketHandler.ORDER, "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_UPDATE, jsonObject);
    }


    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }
    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case SocketHandler.LISTEN_GET_BOOKINGS:
                        ArrayList<TableBookingModel> bookingModels = (ArrayList<TableBookingModel>) intent.getSerializableExtra("booking_list");
                        Log.e("bookingArray==>", bookingModels.size() + "");
                        adapter.updateAdapter(bookingModels);
                        break;
                    case "INTERNET_STATE_CHANGE":
                        boolean connected = intent.getBooleanExtra("state", false);
                        if (connected) {
                            noInternet.setVisibility(View.GONE);
                        } else {
                            noInternet.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(SocketHandler.LISTEN_GET_BOOKINGS));
        registerReceiver(broadcastReceiver, new IntentFilter("INTERNET_STATE_CHANGE"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!SharedPrefManager.getTakeoutOrder(TableBookingActivity.this)) {
            btn_collectionOrder.setVisibility(View.INVISIBLE);
        } else {
            btn_collectionOrder.setVisibility(View.VISIBLE);
        }
    }

//    private void getTableBookings(boolean isShowProgress) {
//        if (isShowProgress) {
//            progress.show();
//        }
//        new TableBookingApiHelper(data -> {
//            if (progress != null && progress.isShowing()) {
//                progress.dismiss();
//            }
//            if (data.size() > 0) {
//                Log.e("TableBookings==>", "data found");
//                adapter.updateAdapter(data);
//            }
//        }).execute();
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawers();
        if (item.getItemId() == R.id.drawerMenuLogOut) {
            confirmLogOut();

        } else if (item.getItemId() == R.id.drawerMenuRefresh) {
            refreshDataFromServer();
        } else if (item.getItemId() == R.id.drawerMenuSetting) {
            Intent intent = new Intent(TableBookingActivity.this, Setting.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.importCustomerData) {
            getDataFromServer();

        } else if (item.getItemId() == R.id.exportCustomerData) {
            sendDataToServer();
        }
        return false;
    }

    void sendDataToServer() {
        ProgressDialog progressDialog = new ProgressDialog(TableBookingActivity.this);
        progressDialog.setMessage("Exporting data to server");
        progressDialog.show();
        new CustomerExport(TableBookingActivity.this, new CustomerExportCallBack() {
            @Override
            public void onSuccess(List<Boolean> results) {
                progressDialog.dismiss();
                showMessage(results);

            }
        }).execute();
    }

    void getDataFromServer() {
        ProgressDialog progressDialog = new ProgressDialog(TableBookingActivity.this);
        progressDialog.setMessage("Importing data from server");

        if (!TableBookingActivity.this.isFinishing()) {
            progressDialog.show();
        }
        progressDialog.show();


        new CustomerImport(TableBookingActivity.this, results -> {
            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }

            showMessage(results);

        }).execute();
    }

    void showMessage(List<Boolean> results) {
        int done = 0;
        int failed = 0;

        for (Boolean result : results) {
            if (result) {
                done++;
            } else {
                failed++;
            }
        }

        new android.app.AlertDialog.Builder(TableBookingActivity.this)
                .setTitle("Process Done")
                .setMessage("Success : " + String.valueOf(done) + " \nFailed : " + String.valueOf(failed))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    public void bookOrFreeNow(TableBookingModel tableBookingModel) {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want " + (!tableBookingModel.free ? "book" : "free") + " this table ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        freeTheTable(tableBookingModel);
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    void freeTheTable(TableBookingModel tableBookingModel) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put("table", TableBookingModel.toJson(tableBookingModel));
        } catch (Exception e) {
            e.printStackTrace();
        }
        BaseApp.webSocket.emit(SocketHandler.EMIT_TABLE_BOOKED, jsonObject);
//        ProgressDialog progress = new ProgressDialog(this);
//        progress.setMessage("Loading");
//        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
//        progress.show();
//
//        JSONObject customerJson = new JSONObject();
//        OkHttpClient client = new OkHttpClient();
//        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//        RequestBody requestBody = RequestBody.create(JSON, customerJson.toString());
//        Request request = new Request.Builder()
//                .url(BASE_URL + "config/booking/table/" + (!tableBookingModel.free ? "busy" : "free") + "?floor_no=" + tableBookingModel.floor_no + "&local_no=" + tableBookingModel.local_no + "&table_no=" + tableBookingModel.table_no)
//                .patch(requestBody)
//                .addHeader("Content-Type", "application/json")
//                .addHeader("ProviderSession", providerSession)
//                .build();
//
//        client.newCall(request).enqueue(new Callback() {
//
//            @Override
//            public void onFailure(@NotNull Call call, @NotNull IOException e) {
//                progress.dismiss();
//                failedToConnect();
//            }
//
//            @Override
//            public void onResponse(@NotNull Call call, @NotNull Response response) {
//                progress.dismiss();
//                if (response.code() == 200) {
//                    JSONObject jsonObject = new JSONObject();
//                    try {
//                        jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
//                        jsonObject.put(SocketHandler.ORDER, "null");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_UPDATE, jsonObject);
////                    runOnUiThread(() -> {
////                        getTableBookings(true);
////                    });
//
//                } else {
//                    showToast("FAILED");
//                }
//            }
//        });


    }

    void showToast(String message) {
        runOnUiThread(() -> Toast.makeText(TableBookingActivity.this, message, Toast.LENGTH_SHORT).show());
    }

    void failedToConnect() {
        runOnUiThread(() -> new AlertDialog.Builder(TableBookingActivity.this)
                .setMessage("Failed to Connect ! please Try again")
                .setPositiveButton("OK", (dialog, which) -> {
                    dialog.dismiss();
                }).create().show());

    }

    public void getNoOfGuestInput(TableBookingModel tableNo) {
        EditText inputField = new EditText(TableBookingActivity.this);
        inputField.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputField.setText("");
        AlertDialog.Builder builder = new AlertDialog.Builder(TableBookingActivity.this);
        builder.setTitle("No of Guest")
                .setView(inputField)
                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int noOfGuest = 1;
                        String str = inputField.getText().toString();
                        if (!str.isEmpty()) {
                            try {
                                noOfGuest = Integer.parseInt(str);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        tableNo.adult = noOfGuest;
                        openOrderPageWithTable(tableNo);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if (tableNo.free) {
            builder.setNeutralButton("Set Free", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    bookOrFreeNow(tableNo);
                }
            });
        }
        builder.create();
        builder.show();
    }

//    public void getCustomerName() {
//        EditText inputField = new EditText(TableBookingActivity.this);
//        inputField.setInputType(InputType.TYPE_CLASS_TEXT);
//        inputField.setText("");
//        AlertDialog builder = new AlertDialog.Builder(TableBookingActivity.this)
//                .setTitle("Customer Name")
//                .setView(inputField)
//                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        ;
//
//                    }
//                })
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                }).create();
//        builder.show();
//    }


    protected void openOrderPage(String orderType) {
        Intent in = new Intent(TableBookingActivity.this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType", orderType);
        in.putExtras(b);
        startActivity(in);
    }

    private void openOrderPageWithTable(TableBookingModel tableNo) {
        Intent in = new Intent(TableBookingActivity.this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType", "TABLE");
        b.putSerializable("table", tableNo);
        in.putExtras(b);
        startActivityResultLauncher.launch(in);
    }

    ActivityResultLauncher<Intent> startActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == ORDER_CREATE_RESULT_CODE) {
                        Log.e("TableBookings==>", "activity result");
//                        getTableBookings(true);
                    }
                }
            });

    private void openOrderListPage() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put(SocketHandler.ORDER, "null");
        } catch (Exception e) {
            e.printStackTrace();
        }
        BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_UPDATE, jsonObject);
        Intent in = new Intent(TableBookingActivity.this, Order.class);
        startActivity(in);
    }


    private void confirmLogOut() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Logout..!!!")
                .setIcon(R.drawable.ic_logout_red)
                .setMessage("Are you sure,You want to logout ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // logout
                        Intent intent = new Intent(TableBookingActivity.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to logout
                    }
                })
                .show();
    }

    void refreshDataFromServer() {
        loadProperty();
    }

    void loadProperty() {
        ProgressDialog dialog = ProgressDialog.show(TableBookingActivity.this, "", "Loading. Please wait...", true);
        dialog.show();
        Retrofit retrofit = new RetrofitClient(TableBookingActivity.this).getRetrofit(new GsonBuilder().create());
        DashboardApi api = retrofit.create(DashboardApi.class);
        retrofit2.Call call = api.getProperty();
        call.enqueue(new retrofit2.Callback() {
            @Override
            public void onResponse(retrofit2.Call call, retrofit2.Response response) {
                dialog.dismiss();
                int responseCode = response.code();
                List<Property> proList = new ArrayList<Property>();
                if (responseCode == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        jsonObject.keys().forEachRemaining(str -> {
                            JSONArray jsonArray = jsonObject.optJSONArray(str);
                            List<PropertyItem> propertyItems = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                PropertyItem _proItem = GsonParser.getGsonParser().fromJson(jsonArray.optJSONObject(i).toString(), PropertyItem.class);
                                propertyItems.add(_proItem);
                            }
                            Property prop = new Property(str, propertyItems);

                            proList.add(prop);

                        });
                        Log.e("mmmm", "onResponse: si  " + proList.size());
                    } catch (JSONException e) {
                        Log.e("mmmm", "onResponse: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
                SharedPrefManager.setAllProperty(TableBookingActivity.this, proList);
                loadProductsFromServer();
            }

            @Override
            public void onFailure(retrofit2.Call call, Throwable t) {
                dialog.dismiss();
                failedToConnect();
            }
        });
    }

    private void loadProductsFromServer() {
        ProgressDialog dialog = ProgressDialog.show(TableBookingActivity.this, "", "Loading. Please wait...", true);
        dialog.show();
        Retrofit retrofit = new RetrofitClient(TableBookingActivity.this).getRetrofit(new GsonBuilder().create());
        ProductApi api = retrofit.create(ProductApi.class);
        retrofit2.Call call = api.getProducts();
        call.enqueue(new retrofit2.Callback() {
            @Override
            public void onResponse(retrofit2.Call call, retrofit2.Response response) {
                dialog.dismiss();
                int responseCode = response.code();
                List<Product> _productList = new ArrayList<Product>();
                if (responseCode == 200) {
                    try {
                        JSONArray _data = new JSONArray(new Gson().toJson(response.body()));
                        for (int i = 0; i < _data.length(); i++) {
                            _productList.add(Product.fromJSON(_data.getJSONObject(i)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("MMMMMMMM", "onResponse: error " + e.getMessage());
                    }
                }
                SharedPrefManager.setAllProduct(TableBookingActivity.this, _productList);
                successMessage();
            }

            @Override
            public void onFailure(retrofit2.Call call, Throwable t) {
                dialog.dismiss();
                failedToConnect();
            }
        });
    }

    void successMessage() {
        AlertDialog builder = new AlertDialog.Builder(TableBookingActivity.this)
                .setTitle("Success")
                .setMessage("Updating Data from server Success")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }

}