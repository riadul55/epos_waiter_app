package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.CategoryCallBack;
import com.novo.tech.redmangopos.model.CategoryModel;

import org.json.JSONArray;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
/**
 * This class created by alamin
 */
public class CategoryCodeHelper extends AsyncTask<String,Void, ArrayList> {
    ArrayList<CategoryModel> categoryModelArrayListH = new ArrayList<CategoryModel>();
    CategoryCallBack callBack;


    public CategoryCodeHelper(ArrayList<CategoryModel> categoryModelArrayList, CategoryCallBack callBack) {
        this.categoryModelArrayListH = categoryModelArrayList;
        this.callBack = callBack;
    }

    @Override
    protected ArrayList<CategoryModel> doInBackground(String... strings) {
        return getCategory();
    }

    private ArrayList<CategoryModel> getCategory() {

        OkHttpClient client = new OkHttpClient();
        ArrayList<CategoryModel> categoryModelArrayList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/config/product/property/category")
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    CategoryModel obj = CategoryModel.fromJSON(jsonArray.optJSONObject(i));
                    categoryModelArrayList.add(obj);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        this.categoryModelArrayListH = categoryModelArrayList;
        return categoryModelArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList arrayList) {
        callBack.onCategoryResponse(categoryModelArrayListH);
        super.onPostExecute(arrayList);
    }
}
