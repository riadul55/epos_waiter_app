package com.novo.tech.redmangopos.extra;

import android.content.Context;
import android.util.Log;

import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

public class DateTimeHelper {
    public static String getTime(){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.getDefault());
        return format.format(new java.util.Date());
    }
    public static String formatDateTime(String dateTime24H){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.getDefault());

        String dateTime=dateTime24H;
        try {
            dateTime = newFormat.format(Objects.requireNonNull(format.parse(dateTime24H)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String formatServerDateTime(String serverDateTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        newFormat.setTimeZone(TimeZone.getDefault());
        String dateTime=serverDateTime;
        try {
            if (dateTime != null && !dateTime.isEmpty()) {
                dateTime = newFormat.format(Objects.requireNonNull(format.parse(serverDateTime)));
            }
        } catch (ParseException e) {
            Log.e("mmmm", "formatServerDateTime: "+serverDateTime );
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String formatLocalDateTimeForServer(String serverDateTime){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a",Locale.getDefault());
        if(serverDateTime.length()==16){
            format = new SimpleDateFormat("dd/MM/yyyy hh:mm",Locale.getDefault());
        }
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        newFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        format.setTimeZone(TimeZone.getDefault());

        String dateTime=serverDateTime;
        try {
            dateTime = newFormat.format(Objects.requireNonNull(format.parse(serverDateTime)));
        } catch (ParseException e) {
            try{
                format = new SimpleDateFormat("dd/MM/yyyy hh:mm",Locale.getDefault());
                newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                dateTime = newFormat.format(Objects.requireNonNull(format.parse(serverDateTime.substring(0,10))));
            }catch (Exception ex){
                ex.printStackTrace();
            }
            e.printStackTrace();
        }
        return dateTime;
    }


    public static List<String> getTimeRange(Context context) {
        Date shiftOpenTime = SharedPrefManager.getShiftOpenTime(context);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(shiftOpenTime);
        Calendar calendar1 = Calendar.getInstance();
        int currentHour = calendar1.get(Calendar.HOUR_OF_DAY);
        int savedHour = calendar.get(Calendar.HOUR_OF_DAY);
        Date startTime;
        Date endTime;
        if (currentHour < savedHour) {
            calendar.add(Calendar.DATE, -1);
            calendar1.setTime(shiftOpenTime);
            startTime = calendar.getTime();
            endTime = calendar1.getTime();
        } else {
            calendar.add(Calendar.DATE, 1);
            calendar1.setTime(shiftOpenTime);
            startTime = calendar1.getTime();
            endTime = calendar.getTime();
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm",Locale.getDefault());
        Log.e("startTime===>", format.format(startTime));
        Log.e("endTime===>", format.format(endTime));
        List<String> times = new ArrayList<>();
        times.add(format.format(startTime));
        times.add(format.format(endTime));
        return times;
    }

    public static String getDBTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm",Locale.getDefault());
        return format.format(new java.util.Date());
    }
    public static String getDBDateAndTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm",Locale.getDefault());
        return format.format(new java.util.Date());
    }
    public static String formatDBDate(String dateTime12H){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm",Locale.getDefault());
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMddHHmm",Locale.getDefault());
        String dateTime=dateTime12H;
        if(dateTime==null){
            dateTime = getDBTime();
        }else{
            try {
                dateTime = newFormat.format(format.parse(dateTime12H));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return dateTime;
    }
    public static String formatOnlyDBDate(String dateTime12H){
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyyHHmm",Locale.getDefault());
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMddHHmm",Locale.getDefault());
        String dateTime=dateTime12H;
        try {
            dateTime = newFormat.format(Objects.requireNonNull(format.parse(dateTime12H.replaceAll("/","").replaceAll("-",""))));
        } catch (ParseException e) {
            Log.e("mmmmmmmm", "formatDBDate: "+e+"  "+dateTime12H);
            e.printStackTrace();
        }
        return dateTime;
    }




}
