package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.PriceUpdateResponseCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class PriceUpdateHelper extends AsyncTask<String, Void,String> {
    String product_uuid;
    String price_uuid;
    double current_price=0.0;
    PriceUpdateResponseCallBack callBack;
    String apiResponse = "";

    public PriceUpdateHelper(String product_uuid, String price_uuid, double current_price, PriceUpdateResponseCallBack callBack) {
        this.product_uuid = product_uuid;
        this.price_uuid = price_uuid;
        this.current_price = current_price;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... strings) {
        return ProductPriceUpdate(current_price,product_uuid,price_uuid);
    }
    public String ProductPriceUpdate(double currentPrice, String product_uuid, String price_uuid){




        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //String start_date = sdf.format(new Date());
        String start_date = getYesterdayDateString();
        JSONObject priceJson = new JSONObject();
        try {
            priceJson.put("end_date",start_date);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, priceJson.toString());
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uuid+"/price/"+price_uuid)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("providerSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code()==200){
                apiResponse = "success";
                //addNewProductPrice(currentPrice,product_uuid);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiResponse;
    }

    @Override
    protected void onPostExecute(String s) {
        callBack.onPriceUpdateResponse(apiResponse);
        super.onPostExecute(s);
    }
    private String getYesterdayDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(yesterday());
    }

    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public void addNewProductPrice(double newPrice, String product_uuid){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String start_date = sdf.format(new Date());
        JSONObject priceJson = new JSONObject();
        try {
            priceJson.put("price",newPrice);
            priceJson.put("VAT",0);
            priceJson.put("currency","GBP");
            priceJson.put("extra_price",0);
            priceJson.put("start_date",start_date);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, priceJson.toString());
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uuid+"/price")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("providerSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code()==201){

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
