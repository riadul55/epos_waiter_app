package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class ProductAvailabilityUpdateHelper extends AsyncTask<String, String, String> {
    String uid;
    Boolean available;

    public ProductAvailabilityUpdateHelper(String uid, Boolean available) {
        this.uid = uid;
        this.available = available;


    }

    @Override
    protected String doInBackground(String... strings) {
        availabilityUpdate(available, uid);
        return null;
    }

    public String availabilityUpdate(Boolean availability, String product_uuid){
        String AVAILABILITY_API = "https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uuid;
        String availability_data = "0";
        if(availability)
            availability_data="1";

        JSONObject product = new JSONObject();
        JSONObject property = new JSONObject();

        try {
            property.put("available",availability_data);
            product.put("product_type","ITEM");
             product.put("properties",property);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, product.toString());

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(AVAILABILITY_API)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("providerSession", providerSession)
                .build();

        Response response = null;

        try {
            response = client.newCall(request).execute();
            Log.d("response<<<<<<<<< : ",response.body().toString());
            if (response.code() == 202){

               // Toast.makeText(context, "updated code successfully", Toast.LENGTH_SHORT).show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
