package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.PriceUpdateResponseCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class ComponentDeleteHelper extends AsyncTask<String, Void,String> {
    String product_uuid;
    String component_uuid;
    PriceUpdateResponseCallBack callBack;
    String apiResponse = "";

    public ComponentDeleteHelper(String product_uuid, String component_uuid, PriceUpdateResponseCallBack callBack) {
        this.product_uuid = product_uuid;
        this.component_uuid = component_uuid;
        this.callBack = callBack;
    }
    @Override
    protected String doInBackground(String... strings) {
        return ComponentDelete(product_uuid,component_uuid);
    }
    public String ComponentDelete( String product_uuid, String component_uuid){

        JSONObject componentJson = new JSONObject();
        try {
            componentJson.put("componentJson",componentJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, "");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uuid+"/item/"+component_uuid)
                .delete()
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession",providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code()==202){
                apiResponse = "success";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiResponse;
    }
    @Override
    protected void onPostExecute(String s) {
        callBack.onPriceUpdateResponse(apiResponse);
        super.onPostExecute(s);
    }
}
