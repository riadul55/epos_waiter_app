package com.novo.tech.redmangopos.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.CustomerProfile;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class DBCustomerManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBCustomerManager(Context c) {
        context = c;
    }

    public DBCustomerManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }
    public CustomerModel getCustomerData(int id) {
        open();
        CustomerModel customerModel = null;
        Cursor cursor = database.rawQuery("SELECT * from CUSTOMER WHERE id=?", new String[]{String.valueOf(id)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                 customerModel = getCustomerDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customerModel;
    }
    public CustomerModel getCustomerData(String customer_id) {
        open();
        CustomerModel customerModel = null;
        Cursor cursor = database.rawQuery("SELECT * from CUSTOMER WHERE consumer_uuid=?", new String[]{String.valueOf(customer_id)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                customerModel = getCustomerDataFromCursor(cursor);
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customerModel;
    }
    public List<CustomerModel> getAllCustomerData() {
        open();
        List<CustomerModel> customers = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" ORDER BY id DESC LIMIT 5000",null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    CustomerModel customerModel = getCustomerDataFromCursor(cursor);
                    if(customerModel.profile != null){
                        CustomerProfile profile = customerModel.profile;
                        if((!profile.first_name.isEmpty() || !profile.last_name.isEmpty() ) && !profile.phone.isEmpty()){
                            customers.add(customerModel);
                        }
                    }
                    // do what ever you want here
                }while(cursor.moveToNext());
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return customers;
    }
    private CustomerModel getCustomerDataFromCursor(Cursor cursor){
        int id = cursor.getInt(cursor.getColumnIndex("id"));
        String  consumer_uuid = cursor.getString(cursor.getColumnIndex("consumer_uuid"));
        String  userName = cursor.getString(cursor.getColumnIndex("userName"));
        String  firstName = cursor.getString(cursor.getColumnIndex("firstName"));
        String  lastName = cursor.getString(cursor.getColumnIndex("lastName"));
        String  email = cursor.getString(cursor.getColumnIndex("email"));
        String  phone = cursor.getString(cursor.getColumnIndex("phone"));
        boolean cloudSubmitted = cursor.getInt(cursor.getColumnIndex("cloudSubmitted")) != 0;
        CustomerProfile profile = new CustomerProfile(phone,lastName,firstName,email);
        List<CustomerAddress> addresses = new ArrayList<>();
        String _address = cursor.getString(cursor.getColumnIndex("address"));
        if(_address!=null){
            try{
                JSONArray _data =new JSONArray(_address.replaceAll("NaN","0.0"));
                for (int i =0;i<_data.length();i++){
                    addresses.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)),CustomerAddress.class));
                }
            }catch (Exception e){
                e.printStackTrace();

            }
        }
        return new  CustomerModel(id,consumer_uuid,email,userName,profile,addresses,cloudSubmitted);
    }

    public int addNewCustomer(CustomerModel model,boolean cloudSync) {
        int id = 0;
        if(model.profile==null){
            return -1;
        }
        ContentValues data = new ContentValues();
        data.put("firstName",model.profile.first_name);
        data.put("lastName",model.profile.last_name);
        data.put("phone",model.profile.phone);
        data.put("email",model.profile.email);
        data.put("userName",model.username);
        data.put("consumer_uuid",model.consumer_uuid);
        data.put("cloudSubmitted",cloudSync);
        data.put("address",GsonParser.getGsonParser().toJson(model.addresses));
        open();
        id = (int)database.insert(DatabaseHelper.CUSTOMER_TABLE_NAME, null,data);
        close();
        return id;
    }
    public void updateCustomer(CustomerModel model,boolean submitted) {
        ContentValues data = new ContentValues();
        data.put("firstName",model.profile.first_name);
        data.put("lastName",model.profile.last_name);
        data.put("phone",model.profile.phone);
        data.put("email",model.profile.email);
        data.put("userName",model.username);
        data.put("consumer_uuid",model.consumer_uuid);
        data.put("cloudSubmitted",submitted);
        data.put("address",GsonParser.getGsonParser().toJson(model.addresses));
        open();
        database.update(DatabaseHelper.CUSTOMER_TABLE_NAME, data,"id=?",new String[]{String.valueOf(model.dbId)});
        close();
    }
    public int customerExist(String phone) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" WHERE phone = ? OR consumer_uuid=?",new String[]{String.valueOf(phone),String.valueOf(phone)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }
    public int customerExistWithMail(String email) {
        open();
        int max = 0;
        Cursor cursor = database.rawQuery("SELECT id  from "+DatabaseHelper.CUSTOMER_TABLE_NAME+" WHERE email = ? ",new String[]{email});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                max = cursor.getInt(cursor.getColumnIndex("id"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return max;
    }
}
