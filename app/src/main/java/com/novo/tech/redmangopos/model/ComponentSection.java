package com.novo.tech.redmangopos.model;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ComponentSection implements Serializable {
    public String sectionName,sectionValue;
    public List<Component> components;
    public Component selectedItem;

    public ComponentSection(String sectionName, String sectionValue, @Nullable List<Component> components, @Nullable Component selectedItem) {
        this.sectionName = sectionName;
        this.sectionValue = sectionValue;
        this.components = components;
        if(selectedItem != null)
            this.selectedItem = selectedItem;
        else{
            if(components!=null){
                if (components.size()>0){
                    this.selectedItem = components.get(0);
                }
            }
        }
    }

    public static ComponentSection fromJSON(JSONObject data){
        Component selected = null;
        List<Component> componentList =  new ArrayList<>();
        JSONArray jsonArray = data.optJSONArray("components");
        for (int i = 0; i < jsonArray.length(); i++) {
            componentList.add(Component.fromJSON(jsonArray.optJSONObject(i),null));
        }
        if(data.optJSONObject("selected") != null){
            selected = Component.fromJSON(data.optJSONObject("selected"),null);
        }
        return new ComponentSection(
                data.optString("sectionName"),
                data.optString("sectionValue"),
                componentList,
                selected
    );
    }

    static JSONObject toJSON(ComponentSection itemModel){
        JSONObject jsonObject= new JSONObject();
        JSONObject selected= new JSONObject();
        JSONArray components = new JSONArray();
        for (Component component:itemModel.components) {
            components.put(Component.toJSON(component));
        }
        if(itemModel.selectedItem != null){
            selected = Component.toJSON(itemModel.selectedItem);
        }
        try{
            jsonObject.putOpt("sectionName", itemModel.sectionName);
            jsonObject.putOpt("sectionValue", itemModel.sectionValue);
            jsonObject.putOpt("components", components);
            jsonObject.putOpt("selected", selected);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


}