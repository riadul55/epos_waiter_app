package com.novo.tech.redmangopos.view.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.Fragment;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.CustomerImport;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class BusinessSetting extends Fragment {
    Button changeButton,importButton;
    MaterialCheckBox takeoutOrder,takeoutOrderDiscount,plasticBag;
    TextView openTimeTextView, changeOpenTime;

    String shiftOpenTime;
    EditText printerIpAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_bussiness_setting, container, false);
        changeButton = v.findViewById(R.id.shiftTimeChangeButton);
        importButton = v.findViewById(R.id.btnImportCustomerData);
        takeoutOrder = v.findViewById(R.id.checkBoxTakeoutOrder);
        takeoutOrderDiscount = v.findViewById(R.id.takeoutDiscount);
        plasticBag = v.findViewById(R.id.checkBoxForBagMandatory);
        printerIpAddress = v.findViewById(R.id.printerIpAddress);

        changeOpenTime = v.findViewById(R.id.changeOpenTime);
        openTimeTextView = v.findViewById(R.id.openTimeTextView);

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });
        importButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromServer();

            }
        });
        changeOpenTime.setOnClickListener(v12 -> getShiftOpenTime());
        displayData();
        return v;
    }

    void getShiftOpenTime(){
        Calendar mCurrentTime = Calendar.getInstance();
        int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mCurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                shiftOpenTime = selectedHour+":"+selectedMinute;
                try {
                    Date parse = new SimpleDateFormat("HH:mm", Locale.getDefault()).parse(shiftOpenTime);
                    String format = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(parse);
                    openTimeTextView.setText(format);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.show();
    }

    void getDataFromServer(){
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Importing data from server");

        if(getActivity() != null && !getActivity().isFinishing()) {
            progressDialog.show();
        }


        new CustomerImport(getContext(), results -> {
            if (progressDialog.isShowing()){
                progressDialog.cancel();
            }

            showMessage(results);

        }).execute();
    }
    void showMessage(List<Boolean> results){
        int done = 0;
        int failed = 0;

        for (Boolean result : results) {
            if(result){
                done++;
            }else{
                failed++;
            }
        }

        new AlertDialog.Builder(getContext())
                .setTitle("Process Done")
                .setMessage("Success : "+String.valueOf(done)+" \nFailed : "+String.valueOf(failed))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }



    void updateData(){
        if(shiftOpenTime != null){
            SharedPrefManager.setShiftOpenTime(getContext(),shiftOpenTime);
        }

        SharedPrefManager.setPlasticBagMandatory(getContext(),plasticBag.isChecked());
        SharedPrefManager.setTakeoutOrder(getContext(),takeoutOrder.isChecked());
        SharedPrefManager.setTakeoutOrderDiscount(getContext(),takeoutOrderDiscount.isChecked());
        SharedPrefManager.setPrinterIp(getContext(), printerIpAddress.getText().toString());

        successMessage();
    }

    void displayData(){
        Date openTime = SharedPrefManager.getShiftOpenTime(getContext());
        String strOpen = "10.00 AM";
        if(openTime != null){
            strOpen = new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(openTime);
        }
        openTimeTextView.setText(strOpen);

        plasticBag.setChecked(SharedPrefManager.getPlasticBagMandatory(getContext()));
        takeoutOrderDiscount.setChecked(SharedPrefManager.getTakeoutOrderDiscount(getContext()));
        takeoutOrder.setChecked(SharedPrefManager.getTakeoutOrder(getContext()));
        printerIpAddress.setText(SharedPrefManager.getPrinterIp(getContext()));
    }

    void successMessage(){
        new AlertDialog.Builder(getContext())
                .setTitle("Saved Successfully Completed")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create()
                .show();
    }
}