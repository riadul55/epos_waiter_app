package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

public class ShiftInfoOrderModel {

    public int order_id;
    public int local_order_id;
    public int booking_id;
    public String order_channel;
    public String order_type;
    public String order_status;
    public double delivery_charges;
    public double discountable_amount;
    public double none_discountable_amount;
    public double discounted_amount;
    public String payment_type;

    public ShiftInfoOrderModel(int order_id, int local_order_id, int booking_id, String order_channel, String order_type, String order_status, double delivery_charges, double discountable_amount, double none_discountable_amount, double discounted_amount, String payment_type) {
        this.order_id = order_id;
        this.local_order_id = local_order_id;
        this.booking_id = booking_id;
        this.order_channel = order_channel;
        this.order_type = order_type;
        this.order_status = order_status;
        this.delivery_charges = delivery_charges;
        this.discountable_amount = discountable_amount;
        this.none_discountable_amount = none_discountable_amount;
        this.discounted_amount = discounted_amount;
        this.payment_type = payment_type;
    }

    public static ShiftInfoOrderModel fromJSON(JSONObject jsonObject){
        return new ShiftInfoOrderModel(
                jsonObject.optInt("order_id",0),
                jsonObject.optInt("local_order_id",0),
                jsonObject.optInt("booking_id",0),
                jsonObject.optString("order_channel",""),
                jsonObject.optString("order_type",""),
                jsonObject.optString("order_status",""),
                jsonObject.optDouble("delivery_charges",0.0),
                jsonObject.optDouble("discountable_amount",0.0),
                jsonObject.optDouble("none_discountable_amount",0.0),
                jsonObject.optDouble("discounted_amount",0.0),
                jsonObject.optString("payment_type","")
        );
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getLocal_order_id() {
        return local_order_id;
    }

    public void setLocal_order_id(int local_order_id) {
        this.local_order_id = local_order_id;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public String getOrder_channel() {
        return order_channel;
    }

    public void setOrder_channel(String order_channel) {
        this.order_channel = order_channel;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public double getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(double delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public double getDiscountable_amount() {
        return discountable_amount;
    }

    public void setDiscountable_amount(double discountable_amount) {
        this.discountable_amount = discountable_amount;
    }

    public double getNone_discountable_amount() {
        return none_discountable_amount;
    }

    public void setNone_discountable_amount(double none_discountable_amount) {
        this.none_discountable_amount = none_discountable_amount;
    }

    public double getDiscounted_amount() {
        return discounted_amount;
    }

    public void setDiscounted_amount(double discounted_amount) {
        this.discounted_amount = discounted_amount;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }
}
