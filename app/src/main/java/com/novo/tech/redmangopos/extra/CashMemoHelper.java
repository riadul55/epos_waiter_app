package com.novo.tech.redmangopos.extra;


import android.content.Context;
import android.util.Log;

import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TransactionModel;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.storage.DBCashManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.utils.SunmiPrintHelper;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;


public class CashMemoHelper {
    public static void printSendCustomer(OrderModel orderModel, List<CartItem> items) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put("print_type", "invoice");
            jsonObject.put("order_id", orderModel.db_id);
            jsonObject.put("print_items", GsonParser.getGsonParser().toJson(items));
        } catch (Exception e) {
            e.printStackTrace();
        }
        BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_PRINT, jsonObject);
    }

    public static void printSendKitchen(OrderModel orderModel, Context context) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
            jsonObject.put("print_type", "kitchen");
            jsonObject.put("order_id", orderModel.db_id);
            jsonObject.put("printer_ip", SharedPrefManager.getPrinterIp(context));
            jsonObject.put("print_items", GsonParser.getGsonParser().toJson(orderModel.items));
        } catch (Exception e) {
            e.printStackTrace();
        }
        BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_PRINT, jsonObject);
    }

    public static void printCustomerMemo(OrderModel orderModel, Context context){
        double cashPayment = 0;
        double cardPayment = 0;
        DBCashManager cashManager = new DBCashManager(context);
        List<TransactionModel> allTransactions = cashManager.getOrderTransaction(orderModel.db_id);

        for (TransactionModel model: allTransactions) {
            if(model.inCash==0){
                cardPayment+=model.amount;
            }else{
                cashPayment+=model.amount;
            }
        }
        if(orderModel.order_channel == null){
            orderModel.order_channel = "EPOS";
        }
        orderModel.items.sort((a,b)->Integer.compare(a.printOrder,b.printOrder));
        orderModel.items.removeIf((a)->a.uuid.equals("plastic-bag"));

        Log.d("mmmm", "printCustomerMemo: "+allTransactions.size());
        String orderType = orderModel.order_type;
        if(orderModel.order_type.toUpperCase().equals("TAKEOUT")&&orderModel.order_channel.toUpperCase().equals("ONLINE")){
            orderType = "Collection";
        }

        String header = SharedPrefManager.getBusinessName(context);
        header+=SharedPrefManager.getBusinessLocation(context);
        header+="\n...................................\n";
        String str1=orderType +"  Order #"+String.valueOf(orderModel.order_id);
        str1+="\nPrinted "+DateTimeHelper.getTime()+"\n";
        str1+="\nPayment Method :"+ (cardPayment!=0 && cashPayment !=0?"CASH and CARD": orderModel.paymentMethod);
        str1+="\nOrder from :"+orderModel.order_channel.replaceAll("EPOS","Local");
        str1+="\n....................................\n";
        Log.d("mmmmm", "printCustomerMemo: "+str1);
        SunmiPrintHelper.getInstance().printText(header, 40, true, false,1);
        SunmiPrintHelper.getInstance().printText(str1, 40, false, false,1);
        for (CartItem item: orderModel.items) {
            String str3="";
            String str4="";
            String str5="";
            if(item.componentSections.size()>0){
                for (ComponentSection section : item.componentSections){
                    str3+=String.valueOf(item.quantity)+" x "+section.selectedItem.shortName+" : "+item.shortName+"\n";
                }
            }else{
                str3+=String.valueOf(item.quantity)+" x "+item.shortName+"\n";
            }
            str4+=String.valueOf(item.quantity)+" x "+item.price+" = "+ String.format(Locale.getDefault(),"%.2f",item.quantity*item.price)+"\n";
            str5+="\nNote : "+item.comment+"\n";
            Log.d("mmmmm", "printCustomerMemo: "+str3);
            Log.d("mmmmm", "printCustomerMemo: "+str4);
            Log.d("mmmmm", "printCustomerMemo: "+str5);
            Log.d("mmmmm", "printCustomerMemo: sort order "+item.printOrder);

            SunmiPrintHelper.getInstance().printText(str3, 40, false, false,0);
            SunmiPrintHelper.getInstance().printText(str4, 40, false, false,2);
            SunmiPrintHelper.getInstance().printText(str5, 40, false, false,0);
        }

        String summery = "...................................";

        if(orderModel.refundAmount!=0){
            summery+="\nRefunded        "+String.format(Locale.getDefault(),"%.2f",orderModel.refundAmount);
        }
        summery+="\nSubtotal               "+String.format(Locale.getDefault(),"%.2f",orderModel.subTotal);
        if(orderModel.order_type.toUpperCase().equals("DELIVERY")){
            summery+="\nDelivery Charge        "+String.format(Locale.getDefault(),"%.2f",orderModel.deliveryCharge);
        }

        if(orderModel.order_channel.toUpperCase().equals("ONLINE")){
            if(orderModel.discountAmount>0){
                if(orderModel.discountCode==null)
                    orderModel.discountCode = "";

                summery+="\nDiscount ("+orderModel.discountCode+") "+String.format(Locale.getDefault(),"%.2f",orderModel.discountAmount);

            }
            else
                summery+="\nDiscount         0.00";

        }else{
            summery+="\nDiscount ("+orderModel.discountPercentage+"%)        "+String.format(Locale.getDefault(),"%.2f",orderModel.discountAmount);
        }

        if(orderModel.plasticBagCost > 0){
            summery+="\nPlastic Bag             "+String.format(Locale.getDefault(),"%.2f",orderModel.plasticBagCost);
        }

        if(orderModel.adjustmentAmount>0){
            summery+="\nAdjustment        "+String.format(Locale.getDefault(),"%.2f",orderModel.adjustmentAmount);
        }
        summery+="\nTips                   "+String.format(Locale.getDefault(),"%.2f",orderModel.tips);
        summery+="\nTotal                  "+String.format(Locale.getDefault(),"%.2f",orderModel.total);
        summery+="\n...................................";
        if(cardPayment!=0)
            summery+="\nCard Pay               "+String.format(Locale.getDefault(),"%.2f",cardPayment);
        if(cashPayment!=0)
            summery+="\nCash Pay            "+String.format(Locale.getDefault(),"%.2f",cashPayment);
        if((cardPayment + cashPayment) < orderModel.total)
            summery+="\nDue total           "+String.format(Locale.getDefault(),"%.2f",orderModel.total-(cardPayment + cashPayment))+"\n";

        Log.d("pppppppppp", "printCustomerMemo: "+summery);
        String dlAddress=".....................................\n";

        if(orderModel.deliveryType == null){
            dlAddress+=orderModel.order_type+" Time : "+"ASAP";
        }else if(orderModel.deliveryType.equals("ASAP")){
            dlAddress+=orderModel.order_type+" Time : "+"ASAP";
        }else{
            dlAddress+=orderModel.order_type+" Time : "+orderModel.currentDeliveryTime;
        }

        if(orderModel.customer != null){
            CustomerModel customerModel = orderModel.customer;
            if(customerModel.profile != null){
                dlAddress+="\nName : "+customerModel.profile.first_name+" "+customerModel.profile.last_name;
                if(customerModel.addresses!=null){
                    if(customerModel.addresses.size()!=0){
                        if(customerModel.addresses.get(0).properties!=null){
                            CustomerAddressProperties pro = customerModel.addresses.get(0).properties;
                            dlAddress+="\nAddress : "+pro.building+" "+pro.street_number+" "+pro.street_name+"\n"+pro.city+" "+pro.state+" "+pro.zip;
                        }
                    }
                }
                dlAddress+="\nPhone : "+customerModel.profile.phone;
            }
        }
        String comment = "\nComments : "+orderModel.comments;
        SunmiPrintHelper.getInstance().printText(summery, 40, false, false,2);
        SunmiPrintHelper.getInstance().printText(dlAddress, 40, false, false,0);
        SunmiPrintHelper.getInstance().printText(comment, 40, false, false,0);
        SunmiPrintHelper.getInstance().feedPaper();
        SunmiPrintHelper.getInstance().cutpaper();
    }
    public static void printKitchenMemo(OrderModel orderModel,Context context){
//        printCustomerMemo(orderModel,context);
//        String str1=orderModel.order_type;
//        str1+="\n...............................\n";
//        str1+="Order # "+String.valueOf(orderModel.order_id);
//        str1+="\n...............................\n";
//        str1+=DateTimeHelper.getTime()+"\n";
//        str1+="Created "+DateTimeHelper.formatDateTime(orderModel.orderDateTime);
//        str1+="\n===============================\n";
//
//        String str2="";
//        for (CartItem item: orderModel.items) {
//            str2+=String.valueOf(item.quantity)+" x "+item.shortName;
//            if(item.componentSections.size()>0){
//                for (ComponentSection section : item.componentSections){
//                    str2+="\n\t"+(section.sectionValue+" : "+section.selectedItem.shortName);
//                }
//            }
//            str2+="\nNote : "+item.comment;
//            str2+="\n...............................\n";
//        }
//        str2+= "Comments : "+orderModel.comments;
//
//        SunmiPrintHelper.getInstance().printText(str1, 35, false, false,1);
//        SunmiPrintHelper.getInstance().printText(str2, 35, false, false,0);
//        SunmiPrintHelper.getInstance().feedPaper();
//        SunmiPrintHelper.getInstance().cutpaper();
    }

    public static void printShiftReport(Context context){
        DBCashManager cash = new DBCashManager(context);

        String header = DateTimeHelper.getTime()+"\n" +
                "Jk Peri Peri Grill\n" +
                "16 Station Rd,Desborough,\nKettering NN14 2RL\n" +
                "...............................\n";
        String info =    "Total Orders            "+String.valueOf(cash.getTotalOrders())+
                       "\nOnline Orders           "+String.valueOf(cash.getOnlineOrders())+
                       "\nOnline Card Orders      "+String.valueOf(cash.getOnlineCardOrders())+
                       "\nOnline Card Total       "+String.format(Locale.getDefault(),"%.2f",cash.getCardTotal())+
                       "\nTotal Cash              "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashAmount())+
                       "\nTotal Card              "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCardAmount())+
                       "\nManual Discount         "+String.format(Locale.getDefault(),"%.2f",cash.getTotalDiscount())+
                       "\nTotal Tips              "+String.format(Locale.getDefault(),"%.2f",cash.getTotalTips())+
                       "\nTotal Cash In           "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashIn())+
                       "\nTotal Cash Out          "+String.format(Locale.getDefault(),"%.2f",cash.getTotalCashOut())+
                       "\nRefunded Amount         "+String.valueOf(cash.getTotalRefund())+
                       "\nBalance                 "+String.format(Locale.getDefault(),"%.2f",cash.getBalance());
        Log.d("mmmmm", "printShiftReport: "+header);
        Log.d("mmmmm", "printShiftReport: "+info);
        SunmiPrintHelper.getInstance().printText(header, 35, false, false,1);
        SunmiPrintHelper.getInstance().printText(info, 35, false, false,0);
        SunmiPrintHelper.getInstance().feedPaper();
        SunmiPrintHelper.getInstance().cutpaper();


    }

}
