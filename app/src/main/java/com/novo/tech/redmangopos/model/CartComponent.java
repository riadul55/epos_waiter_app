package com.novo.tech.redmangopos.model;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class CartComponent implements Serializable {
    String uuid,shortName,description;
    double price;
    String relationType,relationGroup;

    public CartComponent(String uuid, String shortName, String description, double price, String relationType, String relationGroup) {
        this.uuid = uuid;
        this.shortName = shortName;
        this.description = description;
        this.price = price;
        this.relationType = relationType;
        this.relationGroup = relationGroup;
    }

    public static CartComponent fromJSON(JSONObject data){
        double _price =0;
        if(data.optJSONObject("price")!=null){
            _price = data.optJSONObject("price").optDouble("price");
        }
        return new CartComponent(
                data.optString("product_uuid"),
                data.optString("short_name"),
                data.optString("description"),
                _price,
                data.optString("relation_type"),
                data.optString("relation_group"));
    }
    JSONObject toJSON(CartComponent componentModel){
        JSONObject jsonObject= new JSONObject();
        try{
            jsonObject.putOpt("product_uuid", componentModel.uuid);
            jsonObject.putOpt("short_name", componentModel.shortName);
            jsonObject.putOpt("description", componentModel.description);
            jsonObject.putOpt("relation_group", componentModel.relationGroup);
            jsonObject.putOpt("price", componentModel.price);
            jsonObject.putOpt("relation_type", componentModel.relationType);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

}