package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.view.fragment.CustomerInput;
import com.novo.tech.redmangopos.view.fragment.CustomerSearch;

import java.util.List;

public class CustomerSearchAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    List<CustomerModel> customerModelList;

    public CustomerSearchAdapter(Context applicationContext, List<CustomerModel> customerModelList) {
        this.context = applicationContext;
        this.customerModelList = customerModelList;
        inflater = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return customerModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.item_customer_select, null);
        CustomerModel model = customerModelList.get(position);
        TextView customerName,customerPhone;
        AppCompatImageButton edit,select;
        customerName = view.findViewById(R.id.customerItemName);
        customerPhone = view.findViewById(R.id.customerItemPhone);
        edit = view.findViewById(R.id.edit);
        select = view.findViewById(R.id.select);
        if(model.profile != null){
            customerName.setText((model.profile.first_name+" "+model.profile.last_name).replaceAll("null",""));
            customerPhone.setText((model.profile.phone).replaceAll("null",""));
        }

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerSearch customerSearch = (CustomerSearch)((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerSelect);
                customerSearch.backToOrder(model);
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CustomerInput customerInput = (CustomerInput) ((FragmentActivity)context).getSupportFragmentManager().findFragmentById(R.id.frameLayoutCustomerInput);
               if(customerInput!=null){


                   customerInput.customerModel = model;
                  // customerInput.addNewCustomerFromDialog(model);
                   if(model.profile!=null){
                       customerInput.firstName.setText(model.profile.first_name);
                       customerInput.lastName.setText(model.profile.last_name);
                       customerInput.phone.setText(model.profile.phone);
                       customerInput.email.setText(model.email);
//
//                       customerInput.et_first_name.setText(model.profile.first_name);
//                       customerInput.et_last_name.setText(model.profile.last_name);
//                       customerInput.et_phn_number.setText(model.profile.phone);
//                       customerInput.et_email_address.setText(model.email);
                   }

                   if(model.addresses != null){
                       if(model.addresses.size()!=0){
                           CustomerAddress address = model.addresses.get(0);
                           if(address.properties != null){

                               customerInput.street.setText(address.properties.street_number+" "+address.properties.street_name);
                               customerInput.town.setText(address.properties.city);
                               customerInput.zip.setText(address.properties.zip);
                               customerInput.houseNumber.setText(address.properties.building);
//
//                               customerInput.et_street_name.setText(address.properties.street_number+" "+address.properties.street_name);
//                               customerInput.et_city.setText(address.properties.city);
//                               //customerInput.et_zip_name.setText(address.properties.zip);
//                               customerInput.et_building_number.setText(address.properties.building);
//                               customerInput.et_state.setText(address.properties.state);

                           }
                       }
                   }
               }
            }
        });
        return view;
    }
}
