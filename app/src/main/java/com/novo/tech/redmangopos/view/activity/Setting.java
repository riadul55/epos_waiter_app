package com.novo.tech.redmangopos.view.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.BusinessSetting;

public class Setting extends AppCompatActivity {
    Button setting;
    LinearLayout settingBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        settingBack = findViewById(R.id.settingBAck);
        setting = findViewById(R.id.settingButtonSetting);
        settingBack.setOnClickListener(v -> finish());
        openBusinessFragment();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    void openBusinessFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.settingFragmentView, BusinessSetting.class,null)
                .commit();
    }
}