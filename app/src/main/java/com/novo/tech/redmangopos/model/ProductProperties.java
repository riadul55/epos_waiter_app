package com.novo.tech.redmangopos.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ProductProperties implements Serializable {
    public boolean D,V,G,N,VE,offer;
    public String category,spiced,available_on;
    public int sort_order,print_order;
    public double offerPrice;

    public ProductProperties(boolean d, boolean v, boolean g, boolean n, boolean VE, boolean offer, String category, String spiced, int sort_order, int print_order, String available_on, double offerPrice) {
        D = d;
        V = v;
        G = g;
        N = n;
        this.VE = VE;
        this.category = category;
        this.spiced = spiced;
        this.sort_order = sort_order;
        this.print_order = print_order;
        this.available_on = available_on;
        this.offer = offer;
        this.offerPrice = offerPrice;
    }
    public static ProductProperties fromJSON(JSONObject data){
        return new ProductProperties(
                data.optBoolean("D"),
                data.optBoolean("V"),
                data.optBoolean("G"),
                data.optBoolean("N"),
                data.optBoolean("VE"),
                (data.optInt("offer",0)==1),
                data.optString("categories_epos"),
                data.optString("spiced"),
                data.optInt("sort_order",0),
                data.optInt("print_order",0),
                data.optString("available_on",""),
                data.optDouble("offer_price",0.0)

                );
    }
    public static JSONObject toJSON(ProductProperties properties){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.putOpt("categories_epos",properties.category);
            jsonObject.putOpt("spiced",properties.spiced);
            jsonObject.putOpt("VE",properties.VE);
            jsonObject.putOpt("N",properties.N);
            jsonObject.putOpt("G",properties.G);
            jsonObject.putOpt("V",properties.V);
            jsonObject.putOpt("D",properties.D);
            jsonObject.putOpt("available_on",properties.available_on);
            jsonObject.putOpt("sort_order",properties.sort_order);
            jsonObject.putOpt("offer",properties.offer?1:0);
            jsonObject.putOpt("offer_price",properties.offerPrice);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject;
    }

}