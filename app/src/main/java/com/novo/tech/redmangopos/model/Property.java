package com.novo.tech.redmangopos.model;

import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.List;

public class Property implements Serializable {
    public String key_name;
    public List<PropertyItem> items;

    public Property(String key_name, List<PropertyItem> items) {
        this.key_name = key_name;
        this.items = items;
    }
}
