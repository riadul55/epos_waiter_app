package com.novo.tech.redmangopos.callback;

public interface ProductUpdateResponseCallBack {
    void onProductUpdateResponse(String response);
}
