package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

public class ShiftInfoModel {
    public int id;
    public String timestamp;
    public String provider_uuid;
    public String entry_type;
    public int order_id;
    public double amount;
    public String comment;

    public ShiftInfoModel(int id, String timestamp, String provider_uuid, String entry_type, int order_id, double amount, String comment) {
        this.id = id;
        this.timestamp = timestamp;
        this.provider_uuid = provider_uuid;
        this.entry_type = entry_type;
        this.order_id = order_id;
        this.amount = amount;
        this.comment = comment;
    }
    public static ShiftInfoModel fromJSON(JSONObject jsonObject){
        return new ShiftInfoModel(
                jsonObject.optInt("id",0),
                jsonObject.optString("timestamp",""),
                jsonObject.optString("provider_uuid",""),
                jsonObject.optString("entry_type",""),
                jsonObject.optInt("order_id",0),
                jsonObject.optDouble("amount",0.0),
                jsonObject.optString("comment","")
        );
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getProvider_uuid() {
        return provider_uuid;
    }

    public void setProvider_uuid(String provider_uuid) {
        this.provider_uuid = provider_uuid;
    }

    public String getEntry_type() {
        return entry_type;
    }

    public void setEntry_type(String entry_type) {
        this.entry_type = entry_type;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
