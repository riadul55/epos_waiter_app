package com.novo.tech.redmangopos.retrofit2;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DashboardApi {
    @GET("order")
    Call<Object> getOrders();

    @GET("config/product/property?platform=EPOS")
    Call<Object> getProperty();

    @GET("config/product/property/category")
    Call<Object> getCategory();
}