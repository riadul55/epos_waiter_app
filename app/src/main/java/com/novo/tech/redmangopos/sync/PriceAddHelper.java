package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.PriceAddResponseCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class PriceAddHelper extends AsyncTask<String, Void,String> {
    double newPrice;
    String product_uuid;
    PriceAddResponseCallBack callBack;
    String apiResponse = "";

    public PriceAddHelper(double newPrice, String product_uuid, PriceAddResponseCallBack callBack) {
        this.newPrice = newPrice;
        this.product_uuid = product_uuid;
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... strings) {
        return addNewProductPrice(newPrice,product_uuid);
    }
    public String addNewProductPrice(double newPrice, String product_uuid){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String start_date = sdf.format(new Date());
        JSONObject priceJson = new JSONObject();
        try {
            priceJson.put("price",newPrice);
            priceJson.put("VAT",0);
            priceJson.put("currency","GBP");
            priceJson.put("extra_price",0);
            priceJson.put("start_date",start_date);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, priceJson.toString());
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://labapi.yuma-technology.co.uk:8443/delivery/product/"+product_uuid+"/price")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("providerSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code()==201){
                apiResponse = "success";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return apiResponse;
    }
    @Override
    protected void onPostExecute(String s) {
        callBack.onPriceAddResponse(apiResponse);
        super.onPostExecute(s);
    }
}
