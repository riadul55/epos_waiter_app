package com.novo.tech.redmangopos.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "orders";
    public static final String SHIFT_TABLE_NAME = "shift";
    public static final String CASH_TABLE_NAME = "cash";
    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String PRODUCT_TABLE_NAME = "customProduct";

    // Database Information
    static final String DB_NAME = "NOVO_TECH_DELIVERY_ORDER.DB";

    // database version
    static final int DB_VERSION = 1;

    // Creating table query
    private static final String CREATE_ORDER_TABLE = "CREATE TABLE orders ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "order_channel TEXT, " +
            "requester_type TEXT, " +
            "order_id INTEGER, " +
            "server_id INTEGER, " +
            "date NUMERIC, " +
            "orderDateTime NUMERIC, " +
            "requestedDeliveryDateTime NUMERIC, " +
            "currentDeliveryDateTime NUMERIC, " +
            "deliveryType TEXT, " +
            "order_type TEXT, " +
            "order_status TEXT, " +
            "payment_method TEXT, " +
            "payment_status TEXT, " +
            "comments TEXT, " +
            "subTotal REAL, " +
            "total REAL, " +
            "discountableTotal REAL, " +
            "paidTotal REAL, " +
            "dueAmount REAL, " +
            "discountPercentage INTEGER, " +
            "discount REAL, " +
            "plasticBagCost REAL, " +
            "containerBagCost REAL, " +
            "discountCode TEXT, " +
            "refund REAL, " +
            "adjustment REAL, " +
            "adjustmentNote Text, " +
            "tips REAL, " +
            "deliveryCharge REAL, " +
            "receive REAL, " +
            "cartItems BLOB, " +
            "cloudSubmitted NUMERIC," +
            "shift TEXT," +
            "customerId TEXT," +
            "cashId TEXT," +
            "paymentId TEXT," +
            "tableBookingId INTEGER," +
            "tableBookingInfo TEXT," +
            "customerInfo TEXT," +
            "UNIQUE(date,order_id))";
    private static final String CREATE_SHIFT_TABLE = "CREATE TABLE shift ( id INTEGER PRIMARY KEY AUTOINCREMENT, openTime NUMERIC,closeTime NUMERIC,cloudSubmitted NUMERIC)";
    private static final String CREATE_CASH_TABLE = "CREATE TABLE cash ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "dateTime NUMERIC," +
            "amount REAL," +
            "user TEXT," +
            "type NUMERIC," +
            "shift NUMERIC," +
            "order_id NUMERIC," +
            "cloudSubmitted NUMERIC," +
            "inCash NUMERIC," +
            "localPayment NUMERIC" +
            ")";
    private static final String CREATE_PRODUCT_TABLE = "CREATE TABLE customProduct ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "productId TEXT," +
            "name TEXT," +
            "printOrder NUMERIC," +
            "discountable NUMERIC," +
            "price NUMERIC)";

    private static final String CREATE_CUSTOMER_TABLE = "CREATE TABLE CUSTOMER ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "consumer_uuid TEXT, " +
            "userName TEXT, " +
            "firstName TEXT, " +
            "lastName TEXT, " +
            "phone TEXT, " +
            "email TEXT, " +
            "address TEXT,"+
            "cloudSubmitted TEXT)";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ORDER_TABLE);
        db.execSQL(CREATE_SHIFT_TABLE);
        db.execSQL(CREATE_CASH_TABLE);
        db.execSQL(CREATE_CUSTOMER_TABLE);
        db.execSQL(CREATE_PRODUCT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SHIFT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CASH_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CUSTOMER_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PRODUCT_TABLE_NAME);
        onCreate(db);
    }
}