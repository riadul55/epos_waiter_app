package com.novo.tech.redmangopos.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.model.TransactionModel;

import java.util.ArrayList;
import java.util.List;

public class DBCashManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBCashManager(Context c) {
        context = c;
    }

    public DBCashManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }
    double getTotal(String type){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where type=?",new String[]{type});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    double getTotalWithCash(String type){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where type=? AND inCash=1",new String[]{type});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }


    public double getBalance(){
        double totalCashIn = getTotalWithCash("1");
        double totalCashOut = getTotalWithCash("-1");
        double totalCashOrder = getTotalWithCash("0");
        double totalDiscount = getTotalWithCash("2");
        double totalTips = getTotalWithCash("3");
        double totalRefund = getTotalWithCash("4");
        return totalCashIn+totalCashOut+totalCashOrder-totalRefund;
    }
    public long addTransaction(Context context,double amount,int type,int orderId,boolean inCash){
        ContentValues contentValue = new ContentValues();
        contentValue.put("amount", amount);
        contentValue.put("dateTime",DateTimeHelper.getDBDateAndTime());
        contentValue.put("user", "demo");
        contentValue.put("shift", SharedPrefManager.getCurrentShift(context));
        contentValue.put("type", type);
        contentValue.put("order_id", orderId);
        contentValue.put("cloudSubmitted", 0);
        contentValue.put("inCash", inCash);
        return database.insert(DatabaseHelper.CASH_TABLE_NAME,null,contentValue);
    }

    public void addPaidTransaction(Context context, double amount, int type, int orderId, boolean inCash){
        ContentValues contentValue = new ContentValues();
        contentValue.put("amount", amount);
        contentValue.put("dateTime",DateTimeHelper.getDBDateAndTime());
        contentValue.put("user", "demo");
        contentValue.put("shift", SharedPrefManager.getCurrentShift(context));
        contentValue.put("type", type);
        contentValue.put("order_id", orderId);
        contentValue.put("cloudSubmitted", 1);
        contentValue.put("inCash", inCash);
        database.insert(DatabaseHelper.CASH_TABLE_NAME, null, contentValue);
    }

    public List<TransactionModel> getOrderTransaction(int dbId){
        open();
        List<TransactionModel> transactionModelList = new ArrayList<>();
        Cursor cursor = database.rawQuery("select cash.id,cash.order_id,cash.amount,cash.inCash,cash.type,cash.cloudSubmitted as cashSubmitted from cash where order_id = ?",new String[]{String.valueOf(dbId)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    TransactionModel model =new TransactionModel(
                            cursor.getInt(cursor.getColumnIndex("id")),
                            0,
                            cursor.getDouble(cursor.getColumnIndex("amount")),
                            0,
                            0,
                            cursor.getInt(cursor.getColumnIndex("order_id")),
                            0,
                            "",
                            "",
                            "",
                            "",
                            cursor.getInt(cursor.getColumnIndex("inCash"))
                    );
                    if(model.note==null){
                        model.note = "";
                    }
                    transactionModelList.add(model);
                }while(cursor.moveToNext());
            }
        }
        cursor.close();
        close();
        return  transactionModelList;
    }


    public void deleteOrderTransaction(int dbId){
        open();
        int id = database.delete(DatabaseHelper.CASH_TABLE_NAME,"order_id = ?",new String[]{String.valueOf(dbId)});
        Log.d("mmmm", "deleteOrderTransaction: "+id);
        close();
    }


    public List<TransactionModel> getAllGeneralTransaction(){
        open();
        List<TransactionModel> transactionModelList = new ArrayList<>();
        Cursor cursor = database.rawQuery("select cash.id,cash.order_id,cash.amount,cash.inCash,cash.type,cash.cloudSubmitted as cashSubmitted from cash where type = ? OR type=? AND cloudSubmitted=0",new String[]{String.valueOf(1),String.valueOf(-1)});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    TransactionModel model =new TransactionModel(
                            cursor.getInt(cursor.getColumnIndex("id")),
                            cursor.getInt(cursor.getColumnIndex("type")),
                            cursor.getDouble(cursor.getColumnIndex("amount")),
                            cursor.getInt(cursor.getColumnIndex("cashSubmitted")),
                            0,
                            cursor.getInt(cursor.getColumnIndex("order_id")),
                            0,
                            "",
                            "",
                            "",
                            "",
                            cursor.getInt(cursor.getColumnIndex("inCash"))
                    );
                    if(model.note==null){
                        model.note = "";
                    }
                    transactionModelList.add(model);
                }while(cursor.moveToNext());
            }
        }
        cursor.close();
        close();
        return  transactionModelList;
    }


    public List<TransactionModel> getAllCashTransaction(){
        open();
        List<TransactionModel> transactionModelList = new ArrayList<>();
        Cursor cursor = database.rawQuery("select orders.id as orderId,orders.adjustmentNote as adjustmentNote,orders.total,orders.discount,orders.server_id,orders.order_status,orders.payment_method,orders.payment_status,orders.cloudSubmitted as orderSubmitted,cash.id as cashId,cash.amount,cash.inCash,cash.type,cash.cloudSubmitted as cashSubmitted from cash LEFT OUTER  JOIN  orders on orders.id = cash.order_id AND orders.order_status !=?",new String[]{"NEW"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                do{
                    TransactionModel model =new TransactionModel(
                        cursor.getInt(cursor.getColumnIndex("cashId")),
                        cursor.getInt(cursor.getColumnIndex("type")),
                        cursor.getDouble(cursor.getColumnIndex("amount")),
                        cursor.getInt(cursor.getColumnIndex("cashSubmitted")),
                        cursor.getInt(cursor.getColumnIndex("orderSubmitted")),
                        cursor.getInt(cursor.getColumnIndex("orderId")),
                        cursor.getInt(cursor.getColumnIndex("server_id")),
                        cursor.getString(cursor.getColumnIndex("order_status")),
                        cursor.getString(cursor.getColumnIndex("payment_status")),
                        cursor.getString(cursor.getColumnIndex("payment_method")),
                        cursor.getString(cursor.getColumnIndex("adjustmentNote")),
                        cursor.getInt(cursor.getColumnIndex("inCash"))
                        );
                    if(model.note==null){
                        model.note = "";
                    }
                    transactionModelList.add(model);
                }while(cursor.moveToNext());
            }
        }
        cursor.close();
        close();
        return  transactionModelList;
    }

    public void updateTransaction(int dbID, boolean cloudSubmitted) {
        open();
        ContentValues contentValues = new ContentValues();
        contentValues.put("cloudSubmitted",cloudSubmitted);
        database.update(DatabaseHelper.CASH_TABLE_NAME, contentValues, "id=?", new String[]{String.valueOf(dbID)});
        close();
    }

    public int getTotalOrders(){
        open();
        int value = 0;
        Cursor cursor = database.rawQuery("SELECT count(id) as total from orders where shift = ?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context))});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getInt(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public int getOnlineOrders(){
        open();
        int value = 0;
        Cursor cursor = database.rawQuery("SELECT count(id) as total from orders where shift = ? AND order_channel = ? ",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"ONLINE"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getInt(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public int getOnlineCardOrders(){
        open();
        int value = 0;
        Cursor cursor = database.rawQuery("SELECT count(id) as total from orders where shift = ? AND order_channel = ? AND payment_method=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"ONLINE","CARD"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getInt(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getCardTotal(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT sum(receive) as total from orders where shift = ? AND order_channel = ? AND payment_method=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"ONLINE","CARD"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public int getTotalTransaction(){
        open();
        int value = 0;
        Cursor cursor = database.rawQuery("SELECT count(id) as total from orders where shift = ? AND order_status!=? ",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"CANCELLED"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getInt(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalVoidAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(total),0.00) as total from orders where shift = ? AND order_status=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"CANCELLED"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCashAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND  type=? AND inCash=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"0","1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCardAmount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND  type=? AND inCash=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"0","0"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalDiscount(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND type=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"2"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalTips(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND type=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"3"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalRefund(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND type=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"4"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCashIn(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND type=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }
    public double getTotalCashOut(){
        open();
        double value = 0;
        Cursor cursor = database.rawQuery("SELECT IFNULL(SUM(amount),0.00) as total from cash where shift = ? AND type=?",new String[]{String.valueOf(SharedPrefManager.getCurrentShift(context)),"-1"});
        if (cursor != null) {
            if(cursor.moveToFirst()){
                value = cursor.getDouble(cursor.getColumnIndex("total"));
            }
        }
        assert cursor != null;
        cursor.close();
        close();
        return value;
    }

}
