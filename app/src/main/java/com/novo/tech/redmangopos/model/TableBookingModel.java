package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

import java.io.Serializable;

public class TableBookingModel implements Serializable {
    public int local_no;
    public int floor_no;
    public int table_no;
    public int capacity;
    public int adult,child;
    public String bookingDateTime;
    public Boolean free;

    public TableBookingModel(int local_no, int floor_no, int table_no, int capacity, Boolean free, int adult, int child, String bookingDateTime) {
        this.local_no = local_no;
        this.floor_no = floor_no;
        this.table_no = table_no;
        this.capacity = capacity;
        this.free = free;
        this.adult = adult;
        this.child = child;
        this.bookingDateTime = bookingDateTime;
    }

    public static TableBookingModel fromJSON(JSONObject jsonObject){
        return new TableBookingModel(
                jsonObject.optInt("local_no",0),
                jsonObject.optInt("floor_no",0),
                jsonObject.optInt("table_no",0),
                jsonObject.optInt("capacity",0),
                jsonObject.optBoolean("free",false),
                jsonObject.optInt("num_adults",0),
                jsonObject.optInt("num_children",0),
                jsonObject.optString("booking_start")
        );
    }

    public static JSONObject toJson(TableBookingModel table) {
        JSONObject object = new JSONObject();
        try {
            object.put("local_no", table.local_no);
            object.put("floor_no", table.floor_no);
            object.put("table_no", table.table_no);
            object.put("capacity", table.capacity);
            object.put("free", table.free);
            object.put("num_adults", table.adult);
            object.put("num_children", table.child);
            object.put("booking_start", table.bookingDateTime);
        } catch (Exception e) {
        }
        return object;
    }
}
