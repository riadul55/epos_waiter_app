package com.novo.tech.redmangopos.callback;


import com.novo.tech.redmangopos.model.OrderModel;

import java.util.List;

public interface OrderCallBack {
    void onOrderResponse(List<OrderModel> orderList);
}
