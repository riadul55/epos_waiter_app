package com.novo.tech.redmangopos.socket;

import android.content.Context;

import org.json.JSONObject;

import io.socket.emitter.Emitter;

public class SocketListeners {
    private Context context;

    public SocketListeners(Context context) {
        this.context = context;
    }

    public final Emitter.Listener onOrderCreate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
//            CreateOrderWorker worker = new CreateOrderWorker(context, jsonObject);
//            worker.execute();

        }
    };

    public final Emitter.Listener onOrderUpdate = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
//            UpdateOrderWorker worker = new UpdateOrderWorker(context, jsonObject);
//            worker.execute();
        }
    };

    public final Emitter.Listener onOrderList = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            OrderListWorker worker = new OrderListWorker(context);
//            worker.execute();
        }
    };

    public final Emitter.Listener onOrderSuccess = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
        }
    };
}
