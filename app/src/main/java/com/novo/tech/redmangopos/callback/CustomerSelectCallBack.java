package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.CustomerModel;

public interface CustomerSelectCallBack {
    void onCustomerSelect(CustomerModel customerModel);
}
