package com.novo.tech.redmangopos.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.ProductProperties;
import com.novo.tech.redmangopos.model.Property;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SharedPrefManager {
    private static final String SOME_STRING_VALUE = "ORDER_DATA";
    private static final ProductProperties defaultProperties = new ProductProperties(false,false,false,false,false,false,"","",0,0,"",0);


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SOME_STRING_VALUE, Context.MODE_PRIVATE);
    }
    public static void setSessionToken(Context context,String token){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("session", token);
        editor.apply();
    }
    public static String getSessionToken(Context context){
        String token = getSharedPreferences(context).getString("session",null);
        return  token;
    }
    public static void setCustomerLoaded(Context context){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("loadedCustomer", true);
        editor.apply();
    }
    public static boolean getCustomerLoaded(Context context){
        boolean result = getSharedPreferences(context).getBoolean("loadedCustomer",false);
        return  result;
    }
    public static void setUserId(Context context,String id){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("id", id);
        editor.apply();
    }
    public static String getUserId(Context context){
        String id = getSharedPreferences(context).getString("id",null);
        return  id;
    }

    public static void setBusinessName(Context context,String name){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("BusinessName", name);
        editor.apply();
    }
    public static String getBusinessName(Context context){
        return  getSharedPreferences(context).getString("BusinessName","Red Mango POS");
    }

    public static void setBusinessLocation(Context context,String location){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("BusinessLocation", location);
        editor.apply();
    }
    public static String getBusinessLocation(Context context){
        return  getSharedPreferences(context).getString("BusinessLocation","Luton , United Kingdom");
    }


    public static void setShiftOpenTime(Context context, String closeTIme) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("shiftOpenTime", closeTIme);
        editor.apply();
    }

    public static Date getShiftOpenTime(Context context) {
        String time = getSharedPreferences(context).getString("shiftOpenTime", "10:00");
        String[] splitTime = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTime[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]));
        return calendar.getTime();
    }

    public static void setTakeoutOrder(Context context,boolean status){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("collectionOrder", status);
        editor.apply();
    }
    public static boolean getTakeoutOrder(Context context){
        return  getSharedPreferences(context).getBoolean("collectionOrder",false);
    }

    public static void setPrinterIp(Context context, String ip){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("printerIpAddress", ip);
        editor.apply();
    }
    public static String getPrinterIp(Context context){
        return  getSharedPreferences(context).getString("printerIpAddress","");
    }

    public static void setTakeoutOrderDiscount(Context context,boolean status){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("collectionOrderDiscount", status);
        editor.apply();
    }
    public static boolean getTakeoutOrderDiscount(Context context){
        return  getSharedPreferences(context).getBoolean("collectionOrderDiscount",false);
    }

    public static void setCurrentShift(Context context,int currentShift){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt("currentShift", currentShift);
        editor.apply();
    }
    public static void setPlasticBagMandatory(Context context,boolean status){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean("PlasticBagMandatory", status);
        editor.apply();
    }
    public static boolean getPlasticBagMandatory(Context context){
        return  getSharedPreferences(context).getBoolean("PlasticBagMandatory",false);
    }
    public static int getCurrentShift(Context context){
        int shift = getSharedPreferences(context).getInt("currentShift", -1);
        return  shift;
    }
    public static List<CustomerModel> getAllCustomer(Context context){
        List<CustomerModel> allCustomer = new ArrayList<>();
        Set<String> allData = getSharedPreferences(context).getStringSet("customer", new HashSet<String>());
        for (String data : allData){
            allCustomer.add(GsonParser.getGsonParser().fromJson(data,CustomerModel.class));
        }
        return  allCustomer;
    }
    public static void addNewCustomer(Context context,CustomerModel customerModel){
        Set<String>  allData = getSharedPreferences(context).getStringSet("customer", new HashSet<String>());
        allData.add(GsonParser.getGsonParser().toJson(customerModel));
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putStringSet("customer", new HashSet<String>(allData));
        editor.apply();
    }
    public static List<Property> getAllProperty(Context context){
        List<Property> allProperty = new ArrayList<>();
        Set<String> allData = getSharedPreferences(context).getStringSet("property", new HashSet<String>());
        for (String data : allData){
            try {
                allProperty.add(GsonParser.getGsonParser().fromJson(data,Property.class));
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProperty: "+e+"  String value = "+data );
            }
        }
        return  allProperty;
    }
    public static void setAllProperty(Context context,List<Property> allProperty){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        Set<String>  allData = new HashSet<String>();
        for (Property pro:allProperty) {
            try{
                allData.add(GsonParser.getGsonParser().toJson(pro));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        editor.putStringSet("property", new HashSet<String>(allData));
        editor.apply();

    }


    public static List<Product> getAvailableProduct(Context context){
        List<Product> allProduct = new ArrayList<>();
        Set<String> allData = getSharedPreferences(context).getStringSet("product", new HashSet<String>());
        for (String data : allData){

            try {
                Product pro =  GsonParser.getGsonParser().fromJson(data,Product.class);
                if(pro.properties == null){
                    pro.properties = defaultProperties;
                }
                if(pro.visible && (pro.properties.available_on.equals("both") || pro.properties.available_on.equals("epos"))) {
                    if (pro.componentList.size() > 0) {
                        List<Component> components = new ArrayList<>(pro.componentList);
                        pro.componentList = new ArrayList<>();
                        components.forEach((a) -> {
                            if (a.visible) {
                                if (a.properties != null) {
                                    if (a.properties.available_on.equals("both") || a.properties.available_on.equals("epos")) {
                                        pro.componentList.add(a);
                                    }
                                }
                            }
                        });
                    }
                    allProduct.add(pro);
                }
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProduct: "+e+"  String value = "+data );
            }
        }
        return  allProduct;
    }
    public static Product getProductDetails(Context context,String product_uuid){
        Product product = null;
        Set<String> allData = getSharedPreferences(context).getStringSet("product", new HashSet<String>());
        for (String data : allData){
            try {
                Product pro =  GsonParser.getGsonParser().fromJson(data,Product.class);
                if(pro.productUUid.equals(product_uuid)){
                    product = pro;
                }
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProduct: "+e+"  String value = "+data );
            }
        }
        return  product;
    }
    public static Product getProductDetailsByComponent(Context context,String component_uuid){
        Product product = null;
        Set<String> allData = getSharedPreferences(context).getStringSet("product", new HashSet<String>());
        for (String data : allData){
            try {
                Product pro =  GsonParser.getGsonParser().fromJson(data,Product.class);
                for (Component component:pro.componentList) {
                    if (component.productUUid.equals(component_uuid)) {
                        product = pro;
                        break;
                    }
                }
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProduct: "+e+"  String value = "+data );
            }
        }
        return  product;
    }

    public static Product getDeliveryProduct(Context context){
        Product delivery = null;
        Set<String> allData = getSharedPreferences(context).getStringSet("product", new HashSet<String>());
        for (String data : allData){
            try {
                Product pro =  GsonParser.getGsonParser().fromJson(data,Product.class);
                if(pro.productType.equals("DELIVERY")){
                    Log.d("mmmmm", "getDeliveryProduct: "+data);
                    pro.properties = defaultProperties;
                }
                delivery = pro;
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProduct: "+e+"  String value = "+data );
            }
        }
        return  delivery;
    }

    public static Product getPlasticBagProduct(Context context){
        Product plasticBag = null;
        Set<String> allData = getSharedPreferences(context).getStringSet("product", new HashSet<String>());
        for (String data : allData){
            try {
                Product pro =  GsonParser.getGsonParser().fromJson(data,Product.class);
                if(pro.productUUid.equals("plastic-bag")){
                    pro.properties = defaultProperties;
                    plasticBag = pro;

                }
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProduct: "+e+"  String value = "+data );
            }
        }
        return  plasticBag;
    }
    public static Product getTakeoutDiscountProduct(Context context){
        Product plasticBag = null;
        Set<String> allData = getSharedPreferences(context).getStringSet("product", new HashSet<String>());
        for (String data : allData){
            try {
                Product pro =  GsonParser.getGsonParser().fromJson(data,Product.class);
                if(pro.productUUid.equals("discount_on_amount")){
                    pro.properties = defaultProperties;
                    plasticBag = pro;

                }
            }catch(Exception e){
                Log.e("mmmmmmmmmm", "getAllProduct: "+e+"  String value = "+data );
            }
        }
        return  plasticBag;
    }

    public static void setAllProduct(Context context,List<Product> allProduct){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        Set<String>  allData = new HashSet<String>();
        for (Product pro:allProduct) {
            try{
                allData.add(GsonParser.getGsonParser().toJson(pro));
            }catch(Exception e){
                Log.e("mmmmmm", "setAllProduct: "+pro.shortName +" error = "+e.getMessage());
                e.printStackTrace();
            }
        }

        editor.putStringSet("product", new HashSet<String>(allData));
        editor.apply();

    }
    public static int getLastOrderId(Context context){
        return BaseApp.orderList.size()+1;
    }
    public static int createUpdateOrder(Context context, OrderModel orderModel){
        int dbId = 0;
//        try{
//            DBOrderManager dbOrderManager = new DBOrderManager(context);
//            dbOrderManager.open();
//            dbId = dbOrderManager.addOrUpdateOrder(orderModel);
//            dbOrderManager.close();
//        }catch (Exception e){
//            FirebaseCrashlytics.getInstance().recordException(e);
//        }
        return dbId;
    }

}