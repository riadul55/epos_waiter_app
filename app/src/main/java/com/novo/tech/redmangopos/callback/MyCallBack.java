package com.novo.tech.redmangopos.callback;


import com.novo.tech.redmangopos.model.OrderModel;

public interface MyCallBack {
    void voidCallBack();
    void OrderCallBack(OrderModel order);
}
