package com.novo.tech.redmangopos.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * This class created by alamin
 */
public class ComponentsModel {
    String product_type;
    String product_uuid;
    String short_name;
    String status;
    double average_rate;
    String creator_provider_uuid;
    String language;
    boolean visible;
    String creation_date;
    String last_update;
    ComponentsPropertiesModel componentsPropertiesModels;
    int max_units;
    int default_units;
    String relation_type;
    String relation_group;

    public ComponentsModel() {
    }

    public ComponentsModel(String product_type, String product_uuid, String short_name, String status, double average_rate, String creator_provider_uuid, String language, boolean visible, String creation_date, String last_update, ComponentsPropertiesModel componentsPropertiesModels, int max_units, int default_units, String relation_type, String relation_group) {
        this.product_type = product_type;
        this.product_uuid = product_uuid;
        this.short_name = short_name;
        this.status = status;
        this.average_rate = average_rate;
        this.creator_provider_uuid = creator_provider_uuid;
        this.language = language;
        this.visible = visible;
        this.creation_date = creation_date;
        this.last_update = last_update;
        this.componentsPropertiesModels = componentsPropertiesModels;
        this.max_units = max_units;
        this.default_units = default_units;
        this.relation_type = relation_type;
        this.relation_group = relation_group;
    }

    public static ComponentsModel fromJSON(JSONObject jsonObject) {
       /* ComponentsModel componentsModel = null;
        ArrayList<ComponentsModel> componentsModelArrayList =  new ArrayList<>();
        JSONArray jsonArray = optJSONObject.optJSONArray("components");
        for (int i = 0; i < jsonArray.length(); i++) {
            componentsModelArrayList.add(ComponentsModel.fromJSON(jsonArray.optJSONObject(i)));
        }
        if(optJSONObject.optJSONObject("selected") != null){
            componentsModel = Component.fromJSON(optJSONObject.optJSONObject("selected"));
        }*/

        ComponentsPropertiesModel componentsPropertiesModel = null;
        JSONObject componentsPropertiesObj = jsonObject.optJSONObject("properties");

        if (componentsPropertiesObj != null){
            String item_name = componentsPropertiesObj.optString("item_name","");
            String sort_order = componentsPropertiesObj.optString("sort_order","");
            componentsPropertiesModel = new ComponentsPropertiesModel(item_name,sort_order);
        }

        return new ComponentsModel(
                jsonObject.optString("product_type",""),
                jsonObject.optString("product_uuid",""),
                jsonObject.optString("short_name",""),
                jsonObject.optString("status",""),
                jsonObject.optDouble("average_rate", 0.0),
                jsonObject.optString("creator_provider_uuid",""),
                jsonObject.optString("language",""),
                jsonObject.optBoolean("visible", true),
                jsonObject.optString("creation_date",""),
                jsonObject.optString("last_update",""),
                componentsPropertiesModel,
                jsonObject.optInt("max_units", 0),
                jsonObject.optInt("default_units", 0),
                jsonObject.optString("relation_type", ""),
                jsonObject.optString("relation_group","")
        );
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_uuid() {
        return product_uuid;
    }

    public void setProduct_uuid(String product_uuid) {
        this.product_uuid = product_uuid;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAverage_rate() {
        return average_rate;
    }

    public void setAverage_rate(double average_rate) {
        this.average_rate = average_rate;
    }

    public String getCreator_provider_uuid() {
        return creator_provider_uuid;
    }

    public void setCreator_provider_uuid(String creator_provider_uuid) {
        this.creator_provider_uuid = creator_provider_uuid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public ComponentsPropertiesModel getComponentsPropertiesModels() {
        return componentsPropertiesModels;
    }

    public void setComponentsPropertiesModels(ComponentsPropertiesModel componentsPropertiesModels) {
        this.componentsPropertiesModels = componentsPropertiesModels;
    }

    public int getMax_units() {
        return max_units;
    }

    public void setMax_units(int max_units) {
        this.max_units = max_units;
    }

    public int getDefault_units() {
        return default_units;
    }

    public void setDefault_units(int default_units) {
        this.default_units = default_units;
    }

    public String getRelation_type() {
        return relation_type;
    }

    public void setRelation_type(String relation_type) {
        this.relation_type = relation_type;
    }

    public String getRelation_group() {
        return relation_group;
    }

    public void setRelation_group(String relation_group) {
        this.relation_group = relation_group;
    }
}
