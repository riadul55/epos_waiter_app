package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

import java.io.Serializable;

public class CustomerProfile implements Serializable {
    public String phone,last_name,first_name,email;

    public CustomerProfile(String phone, String last_name, String first_name, String email) {
        this.phone = phone;
        this.last_name = last_name;
        this.first_name = first_name;
        this.email = email;
    }
    public static CustomerProfile fromJSON(JSONObject data){
        return new CustomerProfile(
                data.optString("phone",""),
                data.optString("last_name",""),
                data.optString("first_name",""),
                data.optString("email","")
        );
    }
}
