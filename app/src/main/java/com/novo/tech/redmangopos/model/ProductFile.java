package com.novo.tech.redmangopos.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ProductFile implements Serializable {
    public String fileUid, keyName, itemId;

    public ProductFile(String fileUid, String keyName, String itemId) {
        this.fileUid = fileUid;
        this.keyName = keyName;
        this.itemId = itemId;
    }
    public static ProductFile fromJSON(JSONObject data){
        return new ProductFile(
                data.optString("file_uuid",""),
                data.optString("key_name",""),
                data.optString("product_uuid","")
    );
    }
    public static JSONObject toJSON(ProductFile item){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.putOpt("file_uuid",item.fileUid);
            jsonObject.putOpt("key_name",item.keyName);
            jsonObject.putOpt("product_uuid",item.itemId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject;
    }
}
