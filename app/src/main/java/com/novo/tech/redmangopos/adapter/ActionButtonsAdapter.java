package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.model.OrderModel;

public class ActionButtonsAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    MyCallBack callBack;
    String[] actionList;
    boolean readOnly;

    String[] deliveryActions = {"REVIEW","PROCESSING","READY","SENT","DELIVERING","DELIVERED","CLOSED"};
    String[] takeOutActions = {"REVIEW","PROCESSING","READY","CLOSED"};

    String[] _deliveryActions = {"REVIEW","PROCESSING","READY","DELIVERING"};
    String[] _takeOutActions = {"REVIEW","PROCESSING","READY"};

    OrderModel orderModel;
    int activeIndex = 0;

    public ActionButtonsAdapter(Context applicationContext,OrderModel orderModel,MyCallBack callBack,boolean readOnly) {
        this.context = applicationContext;
        this.orderModel = orderModel;
        this.callBack = callBack;
        this.readOnly = readOnly;

        inflater = (LayoutInflater.from(applicationContext));
        if(orderModel.order_type.toUpperCase().equals("DELIVERY")){
            actionList = _deliveryActions;
        }else{
            actionList = _takeOutActions;
        }
        activeIndex = getActiveIndex(orderModel.order_status.toUpperCase());
        if(orderModel.order_status.equals("CLOSED")){
            activeIndex = -1;
        }

    }
    @Override
    public int getCount() {
        return actionList.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.model_action_button, null);
        AppCompatButton button = view.findViewById(R.id.modelButton);
        button.setText(actionList[position]);
        if(position != activeIndex || orderModel.order_status.equals("CANCELLED") || orderModel.order_status.equals("REFUNDED")){
            button.setEnabled(false);
            button.setBackgroundColor(ContextCompat.getColor(context,R.color.LightGrey));
        }else{
            button.setEnabled(true);
            button.setBackgroundColor(ContextCompat.getColor(context,R.color.DarkOrange));
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((actionList.length-1)==activeIndex && !orderModel.paymentStatus.equals("PAID")){
                    Toast.makeText(context,"Unpaid Order, please pay first",Toast.LENGTH_LONG).show();
                }else{
                    onItemClick(position);
                }
            }
        });
        view.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 100));
        return view;
    }
    int getActiveIndex(String value){
        int index = -1;
        for (int i = 0;i<actionList.length;i++){
            if(actionList[i].toUpperCase().equals(value)){
                index = i;
                break;
            }
        }

        //we are not allowing customer orders directly..
        if(readOnly)
            return -1;
        return index+1;
    }

    void onItemClick(int position){
        orderModel.order_status=actionList[position];
        activeIndex = getActiveIndex(actionList[position]);
        Log.d("mmm", "onItemClick: "+activeIndex);
        notifyDataSetChanged();
        callBack.OrderCallBack(orderModel);
    }
}
