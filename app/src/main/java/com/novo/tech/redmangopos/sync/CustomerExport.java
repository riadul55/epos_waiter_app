package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.CustomerExportCallBack;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.storage.DBCustomerManager;

import java.util.ArrayList;
import java.util.List;

public class CustomerExport extends AsyncTask<String, Integer, List<Boolean>> {
    private Context context;
    CustomerExportCallBack callBack;
    public CustomerExport(Context context,CustomerExportCallBack callBack){
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected List<Boolean> doInBackground(String... strings) {
        List<Boolean> result = checkCustomerDataInLocal();
        return result;
    }

    List<Boolean> checkCustomerDataInLocal(){
        DBCustomerManager manager = new DBCustomerManager(context);
        List<CustomerModel> customerModelList = manager.getAllCustomerData();
        List<Boolean> success = new ArrayList<>();
        for (CustomerModel model:customerModelList) {
            boolean submitted = new CustomerApi(context,model).pushCustomer();
            success.add(submitted);
        }
        return success;
    }


    @Override
    protected void onPostExecute(List<Boolean> success) {
        callBack.onSuccess(success);

        super.onPostExecute(success);
    }
}
