package com.novo.tech.redmangopos.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.view.activity.Order;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.DateTimeHelper;

import java.util.ArrayList;
import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder> {

    Context mCtx;
    List<OrderModel> orderList;
    List<OrderModel> selected = new ArrayList<>();

    public OrderListAdapter(Context mCtx, List<OrderModel> orderList) {
        this.mCtx = mCtx;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_dashborad_item_view, parent, false);

        return new OrderListViewHolder(view);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        OrderModel order = orderList.get(position);
        holder.orderType.setText(order.order_type.toUpperCase());
        if(order.serverId != 0)
            holder.serverId.setText(String.valueOf(order.serverId));

        if(DateTimeHelper.getDBTime().equals(DateTimeHelper.formatDBDate(order.orderDateTime)))
            holder.orderTime.setText("Today " + order.orderDateTime.substring(11) );
        else
            holder.orderTime.setText(order.orderDateTime);

        if(order.deliveryType == null){
            holder.deliveryTime.setText("ASAP");
        }else if(order.deliveryType.equals("ASAP")){
            holder.deliveryTime.setText("ASAP");
        }else if(!order.currentDeliveryTime.equals("")){
            holder.deliveryTime.setText(order.currentDeliveryTime);
        }

        if(order.order_channel==null)
            order.order_channel = "EPOS";
        if(order.paymentStatus == null){
            order.paymentStatus = "UNPAID";
        }

        Log.e("OrderItem===>", order.order_channel);
        Log.e("OrderItem===>", order.order_status);
        if(order.totalPaid > 0){
            holder.editButton.setVisibility(View.INVISIBLE);
        }else if (!order.order_channel.equals("EPOS")) {
            if(order.order_channel.equals("WAITERAPP") || order.order_channel.equals("WAITER")){
                holder.editButton.setVisibility(View.VISIBLE);
            }else
                holder.editButton.setVisibility(View.INVISIBLE);
        }else{
            if(!order.order_status.equals("CLOSED")){
                holder.editButton.setVisibility(View.VISIBLE);
            }
            else{
                holder.editButton.setVisibility(View.INVISIBLE);
            }
        }

        if (order.paymentStatus != null){
            Log.e("paymentStatus", order.paymentStatus);
            if(order.paymentStatus.equals("PAID")){
                holder.paymentStatus.setImageDrawable(ContextCompat.getDrawable(mCtx, R.drawable.payment_complete));
                holder.editButton.setVisibility(View.INVISIBLE);
            }else{
                holder.paymentStatus.setImageDrawable(ContextCompat.getDrawable(mCtx, R.drawable.payment_not_complete));
            }
        }

//        holder.paymentStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(order.paymentStatus.equals("PAID")){
//                    Toast.makeText(mCtx,"Your Order Already Paid",Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                Intent myIntent = new Intent(mCtx, Payment.class);
//                myIntent.putExtra("orderData", order.db_id);
//                mCtx.startActivity(myIntent);
//            }
//        });

        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Order)mCtx).openOrderPage(order);
            }
        });


        holder.detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Order) mCtx).openOnlineOrderDetailsPage(order);
            }
        });

        if(order.tableBookingModel != null) {
            holder.tableNo.setText("Table : " +String.valueOf(order.tableBookingModel.table_no));
        }

        if (order.bookingId != 0) {
            List<OrderModel> ordersByBookingId = getOrdersByBookingId(order.bookingId);
            double total = 0.0;
            for (OrderModel model: ordersByBookingId) {
                total += model.total;
            }
            holder.amount.setText("£ "+String.format("%.2f",total));
        } else {
            holder.amount.setText("£ "+String.format("%.2f",order.total));
        }
        holder.status.setText(order.order_status);
        holder.terminal.setText(order.order_channel);


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((Order) mCtx).openOnlineOrderDetailsPage(order);
//            }
//        });


        switch (order.order_status) {
            case "REVIEW":
                holder.statusImage.setImageResource(R.drawable.ic_review);
                break;
            case "PROCESSING":
                holder.statusImage.setImageResource(R.drawable.ic_processing);
                break;
            case "READY":
                holder.statusImage.setImageResource(R.drawable.ic_ready);
                break;
            case "SENT":
                holder.statusImage.setImageResource(R.drawable.ic_send);
                break;
            case "DELIVERING":
                holder.statusImage.setImageResource(R.drawable.ic_delivering);
                break;
            case "DELIVERED":
                holder.statusImage.setImageResource(R.drawable.ic_delivered);
                break;
            case "CLOSED":
                holder.statusImage.setImageResource(R.drawable.ic_closed);
                break;
            case "CANCELLED":
                holder.statusImage.setImageResource(R.drawable.ic_cancelled);
                break;
            case "NEW":
                holder.statusImage.setImageResource(R.drawable.ic_baseline_star_outline_24);
                break;
        }

    }


    public List<OrderModel> getOrdersByBookingId(int bookingId) {
        List<OrderModel> list = new ArrayList<>();
        for (OrderModel model: BaseApp.orderList) {
            if (model.bookingId == bookingId) {
                list.add(model);
            }
        }
        return list;
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView orderType,orderTime,deliveryTime,status,amount,serverId;
        ImageView statusImage;
        TextView tableNo,terminal;
        CardView parent_card;
        ImageButton detailsButton,editButton,paymentStatus;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            orderType = itemView.findViewById(R.id.orderListOrderType);
            orderTime = itemView.findViewById(R.id.orderListItemOrderTime);
            deliveryTime = itemView.findViewById(R.id.orderListItemDeliveryTime);
            status = itemView.findViewById(R.id.orderListItemStatus);
            statusImage = itemView.findViewById(R.id.orderListItemStatusImage);
            amount = itemView.findViewById(R.id.orderListItemAmount);
            tableNo = itemView.findViewById(R.id.orderListItemTableNo);
            terminal = itemView.findViewById(R.id.orderListItemTerminal);
            serverId = itemView.findViewById(R.id.serverId);
            parent_card = itemView.findViewById(R.id.parent_card);

            detailsButton = (ImageButton)itemView.findViewById(R.id.orderListItemDetailsButton);
            editButton = itemView.findViewById(R.id.orderListItemEditButton);
            paymentStatus =(ImageButton)itemView.findViewById(R.id.orderListItemPaymentButton);
        }
    }
}