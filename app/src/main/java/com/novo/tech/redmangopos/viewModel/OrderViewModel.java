package com.novo.tech.redmangopos.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.novo.tech.redmangopos.model.OrderModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class OrderViewModel  extends ViewModel {
    MutableLiveData<ArrayList<OrderModel>> orderLiveData;
    public OrderViewModel() {
        init();
    }
    public void init(){
        orderLiveData = new MutableLiveData<>();
    }
    public MutableLiveData<ArrayList<OrderModel>> getActiveOrderList() {
        return orderLiveData;
    }
}
