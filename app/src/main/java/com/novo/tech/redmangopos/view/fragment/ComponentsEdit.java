package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.SectionListAdapter;
import com.novo.tech.redmangopos.extra.GsonParser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ComponentsEdit extends Fragment implements View.OnClickListener {
    List<ComponentSection> sections = new ArrayList<>();
    RecyclerView recyclerViewSegment;
    SectionListAdapter sectionListAdapter;
    Button cancel,add;
    Product product;
    String productData;
    CartItem cartItem;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sub_product, container, false);
        cancel = v.findViewById(R.id.orderCreateComponentsSubCancel);
        add = v.findViewById(R.id.orderCreateComponentsSubAdd);
        cancel.setOnClickListener(this);
        add.setOnClickListener(this);
        productData = requireArguments().getString("product");
        cartItem = (CartItem) requireArguments().getSerializable("cartItem");
        product = GsonParser.getGsonParser().fromJson(productData, Product.class);
        List<Component> componentArrayList = product.componentList;
        componentArrayList.sort((a,b)->{
            if(a.properties != null && b.properties!=null){
                return Integer.compare(a.properties.sort_order,b.properties.sort_order);
            }else if(a.properties == null){
                return -1;
            }else
                return 1;
        });
        for (Component component : componentArrayList){
            ComponentSection group = null;
            for(ComponentSection section : sections){
                if(section.sectionValue.equals(component.relationGroup)){
                    group = section;
                }
            }
            if(group != null){
                group.components.add(component);
            }else{
                group = new ComponentSection(component.relationGroup,component.relationGroup,new ArrayList<>(),null);
                group.components.add(component);
                sections.add(group);
            }

        }
        for (ComponentSection section: sections){
            if (cartItem != null){
                cartItem.componentSections.forEach((cartSection)->{
                    if(section.sectionValue.equals(cartSection.sectionValue)){
                        section.components.forEach((e)->{
                            if(e.productUUid.equals(cartSection.selectedItem.productUUid)){
                                section.selectedItem = e;
                            }
                        });
                    }
                });
            }
//            if(section.components.size()>0)
//                section.selectedItem = section.components.get(0);
        }

        loadSegment(v);
        loadSubCartFragment();
        add.setText("Update");
        return v;
    }

    private void loadSubCartFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("product",  productData);
        bundle.putSerializable("sections", (Serializable) sections);
        getFragmentManager().beginTransaction()
                .replace(R.id.orderCreateComponentsSubCart, OrderComponentSubCartList.class, bundle)
                .commit();
    }

    void loadSegment(View v){
        recyclerViewSegment = v.findViewById(R.id.orderCreateComponentsSubSectionRecyclerView);
        recyclerViewSegment.setLayoutManager(new LinearLayoutManager(getContext()));
        sectionListAdapter = new SectionListAdapter(getContext(),new ArrayList<>(sections));
        recyclerViewSegment.setAdapter(sectionListAdapter);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.orderCreateComponentsSubCancel){
            openProductPage();
        }else if(v.getId() == R.id.orderCreateComponentsSubAdd){
            OrderCartList cartListFragment = (OrderCartList)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
            OrderComponentSubCartList subCartFragment = (OrderComponentSubCartList)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);

            double totalPayable = product.price;
            for(ComponentSection section:sections){
                totalPayable+=section.selectedItem.subTotal;
            }
            cartItem.componentSections = new ArrayList<>(subCartFragment.sectionList);
            cartItem.subTotal = totalPayable;
            cartItem.total = totalPayable;
            cartListFragment.updateCartItem(cartItem);
            openProductPage();
        }
    }

    void openProductPage(){
        OrderCreate orderCreate = ((OrderCreate) getActivity());
        Bundle bundle = new Bundle();
        bundle.putString("category", orderCreate.category);
        getFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class,bundle)
                .commit();
    }
}