package com.novo.tech.redmangopos.view.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.utils.BookingOrderUtil;
import com.novo.tech.redmangopos.view.fragment.OnlineCustomerOrderDetailsAction;
import com.novo.tech.redmangopos.view.fragment.OnlineCustomerOrderDetailsCart;
import com.novo.tech.redmangopos.view.fragment.OnlineCustomerOrderDetailsConsumerInfo;
import com.novo.tech.redmangopos.sync.CustomerOrderDetailsService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class OnlineCustomerOrderDetails extends AppCompatActivity {
    public OrderModel orderModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_order_details);
        List<OrderModel> orderList = (List<OrderModel>) getIntent().getSerializableExtra("orders");
        orderModel = (OrderModel) getIntent().getSerializableExtra("orderData");

        if (orderModel != null && orderModel.bookingId != 0 && orderModel.tableBookingModel != null) {
            orderModel.items = BookingOrderUtil.getMergedItems(orderModel, orderList);
        }

        loadFragments();

    }

    void loadFragments(){
        openCartFragment();
        openActionFragment();
        openConsumerFragment();
    }
    void openCartFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCart, OnlineCustomerOrderDetailsCart.class,bundle)
                .commit();
    }
    void openActionFragment(){
        Bundle bundle = new Bundle();
        bundle.putString("orderModel", GsonParser.getGsonParser().toJson(orderModel));
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsCenter, OnlineCustomerOrderDetailsAction.class,bundle)
                .commit();
    }
    void openConsumerFragment(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderModel",orderModel);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderDetailsRight, OnlineCustomerOrderDetailsConsumerInfo.class,bundle)
                .commit();
    }

    public void openOrderEditPage(OrderModel orderModel){
        finish();
        Intent in=new Intent(this,OrderCreate.class);
        Bundle b = new Bundle();
        b.putSerializable("orderModel", orderModel);
        b.putString("order", "add");
        in.putExtras(b);
        startActivity(in);
    }

    public void openOrderPageWithTable(OrderModel orderModel){
        backToHome();
        Intent in=new Intent(this, OrderCreate.class);
        Bundle b = new Bundle();
        b.putString("orderType","LOCAL");
        b.putString("bookingId", orderModel.bookingId + "");
        b.putSerializable("table", orderModel.tableBookingModel);
        b.putSerializable("customerModel", orderModel.customer);
        b.putSerializable("shippingAddress", orderModel.shippingAddress);
        in.putExtras(b);
        startActivity(in);
    }

    void backToHome(){
        finish();
    }
    @Override
    public void onBackPressed() {
        backToHome();
    }
}