package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.novo.tech.redmangopos.callback.CustomerSelectCallBack;
import com.novo.tech.redmangopos.model.CustomerModel;

import java.util.ArrayList;
import java.util.List;


public class CustomerHelperNameAdapter extends ArrayAdapter<CustomerModel> implements Filterable {

    private ArrayList<CustomerModel> fullList;
    private ArrayList<CustomerModel> mOriginalValues;
    private ArrayFilter mFilter;
    private final CustomerSelectCallBack callBack;

    public CustomerHelperNameAdapter(Context context, int resource, int textViewResourceId, List<CustomerModel> objects, CustomerSelectCallBack customerSelectCallBack) {

        super(context, resource, textViewResourceId, objects);
        fullList = (ArrayList<CustomerModel>) objects;
        mOriginalValues = new ArrayList<CustomerModel>(fullList);
        callBack = customerSelectCallBack;

    }

    @Override
    public int getCount() {
        return fullList.size();
    }

    @Override
    public CustomerModel getItem(int position) {
        return fullList.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }
        CustomerModel model = fullList.get(position);
        if (model != null) {
            TextView lblName = (TextView) view.findViewById(android.R.id.text1);
            if (lblName != null){
                if(model.profile!=null){
                    lblName.setText(model.profile.first_name+" "+model.profile.last_name);
                }
            }
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onCustomerSelect(model);
            }
        });
        return view;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }


    private class ArrayFilter extends Filter {
        private Object lock;

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                synchronized (lock) {
                    mOriginalValues = new ArrayList<CustomerModel>(fullList);
                }
            }

            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    ArrayList<CustomerModel> list = new ArrayList<CustomerModel>(mOriginalValues);
                    results.values = list;
                    results.count = list.size();
                }
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                ArrayList<CustomerModel> values = mOriginalValues;
                int count = values.size();

                ArrayList<CustomerModel> newValues = new ArrayList<CustomerModel>(count);

                for (int i = 0; i < count; i++) {
                    CustomerModel item = values.get(i);
                    if(item.profile != null){
                        if (item.profile.first_name.toLowerCase().contains(prefixString)) {
                            newValues.add(item);
                        }
                    }


                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if(results.values!=null){
                fullList = (ArrayList<CustomerModel>) results.values;
            }else{
                fullList = new ArrayList<CustomerModel>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}