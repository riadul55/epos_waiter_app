package com.novo.tech.redmangopos.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.novo.tech.redmangopos.model.CartItem;

public class DBProductManager {
    private DatabaseHelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBProductManager(Context c) {
        context = c;
    }

    public DBProductManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }
    public void close() {
        dbHelper.close();
    }
    public int addProduct(CartItem cartItem) {
        ContentValues data = new ContentValues();
        data.put("productId",cartItem.uuid);
        data.put("name",cartItem.shortName);
        data.put("printOrder",cartItem.printOrder);
        data.put("discountable",true);
        data.put("price",cartItem.price);
        open();
        int id = (int)database.insert(DatabaseHelper.PRODUCT_TABLE_NAME, null,data);
        close();
        return id;
    }
}
