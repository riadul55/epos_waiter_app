package com.novo.tech.redmangopos.sync;

import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.ZipCallBack;
import com.novo.tech.redmangopos.model.AddressHelperModel;

import org.json.JSONArray;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.ADDRESS_URL;
import static com.novo.tech.redmangopos.util.AppConstant.adminSession;

public class AddressApi  extends AsyncTask<String, Void, ArrayList<AddressHelperModel>> {
    ArrayList<AddressHelperModel> houseList = new ArrayList<AddressHelperModel>();
    ZipCallBack callBack;
    String str;
    public AddressApi(String str, ZipCallBack callBack){
        this.str = str;
        this.callBack = callBack;
    }
    @Override
    protected ArrayList<AddressHelperModel> doInBackground(String... strings) {
        return getHouseByZip(str);
    }
    public  ArrayList<AddressHelperModel> getHouseByZip(String str){
        Log.d("mmmm", "getZipSuggestion: "+str);
        OkHttpClient client = new OkHttpClient();
        ArrayList<AddressHelperModel> houseList  = new ArrayList<>();
        Request request = new Request.Builder()
                .url(ADDRESS_URL+"postal_data/"+str)
                .addHeader("Content-Type","application/json")
                .addHeader("AdminSession", adminSession)
                .build();
        Response response = null;
        try{
            response = client.newCall(request).execute();
            Log.d("mmmm", "getZipSuggestion: "+response.code());
            if(response.code()==200){
                String jsonData = response.body().string();
                JSONArray jsonArray = new JSONArray(jsonData);
                for (int i= 0;i<jsonArray.length();i++){
                    AddressHelperModel obj = AddressHelperModel.fromJSON(jsonArray.optJSONObject(i));
                    houseList.add(obj);
                }
            }
        }catch (Exception e){
            Log.e("mmmmm", "getZipSuggestion: "+e );
            e.printStackTrace();
        }
        this.houseList = houseList;
        return houseList;
    }

    @Override
    protected void onPostExecute(ArrayList<AddressHelperModel> arrayList) {
        callBack.onAddressResponse(houseList);
        super.onPostExecute(arrayList);
    }
}

