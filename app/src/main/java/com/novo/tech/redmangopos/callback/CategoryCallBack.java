package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.CategoryModel;

import java.util.ArrayList;
/**
 * This class created by alamin
 */
public interface CategoryCallBack {
    void onCategoryResponse(ArrayList<CategoryModel> data);
}
