package com.novo.tech.redmangopos.util;

import java.util.HashMap;
import java.util.Map;

public class AppConstant {
    public static final  ConfigTypes types = ConfigTypes.LAB;
    public static final  String business = getConfigs().get("business");
    public static final  String providerID = getConfigs().get("providerID");
    public static final  String providerSession = getConfigs().get("providerSession");
    public static final  String adminSession = getConfigs().get("adminSession");
    public static final  String addressCreator = getConfigs().get("addressCreator");
    public static final  String BASE_URL = getConfigs().get("base_url");
    public static final  String ADDRESS_URL = getConfigs().get("address_url");


    static Map<String, String> getConfigs() {
        Map<String, String> map = new HashMap<String, String>();
        switch (types) {
            case LAB:
                map.put("business", "LAB");
                map.put("providerID", "a7033019-3dee-457b-a9da-50df238b3c9p");
                map.put("providerSession", "2f471e02-b567-4dc9-b72c-7e1c3613310b");
                map.put("adminSession", "ff4c7526-d4f8-45f6-be0a-d0221df0200a");
                map.put("addressCreator", "03683bdc-00a1-4d6d-b344-c70d937f7168");
                map.put("base_url", "https://labapi.yuma-technology.co.uk:8443/delivery/");
                map.put("address_url", "https://priapi.yuma-technology.co.uk:8443/delivery/connector/");
                break;
            case BALTISTAN:
                map.put("business", "BALTISTAN");
                map.put("providerID", "d5ab4efc-b8d9-11eb-9e8f-b63f3ad1ecdd");
                map.put("providerSession", "f9a14968-8b31-49ab-a22e-48323f3adc95");
                map.put("adminSession", "1f7b6acd-3319-42a1-9e4e-40418d11fb8f");
                map.put("addressCreator", "1f7b6acd-3319-42a1-9e4e-40418d11fb8f");
                map.put("base_url", "https://priapi.yuma-technology.co.uk:9010/delivery/");
                map.put("address_url", "https://priapi.yuma-technology.co.uk:9010/delivery/connector/");
                break;
            case FLAVA:
                map.put("business", "FLAVA");
                map.put("providerID", "d5ab4efc-b8d9-11eb-9e8f-b63f3ad1ecdd");
                map.put("providerSession", "d2b87535-43e7-4294-8749-40d71bb1c257");
                map.put("adminSession", "7e70b570-d1a2-433b-91a1-2a4072d44329");
                map.put("addressCreator", "7e70b570-d1a2-433b-91a1-2a4072d44329");
                map.put("base_url", "https://priapi.yuma-technology.co.uk:9012/delivery/");
                map.put("address_url", "https://priapi.yuma-technology.co.uk:9012/delivery/connector/");
                break;
            case JKPERI:
                map.put("business", "JKPERI");
                map.put("providerID", "a7033019-3dee-457b-a9da-50df238b3c9f");
                map.put("providerSession", "0b18c375-bc97-4aa3-be84-e52c171548bd");
                map.put("adminSession", "03683bdc-00a1-4d6d-b344-c70d937f7168");
                map.put("addressCreator", "03683bdc-00a1-4d6d-b344-c70d937f7168");
                map.put("base_url", "https://priapi.yuma-technology.co.uk:9008/delivery/");
                map.put("address_url", "https://priapi.yuma-technology.co.uk:9008/delivery/connector/");
                break;
            case KEBAB_GERMAN:
                map.put("business", "KEBAB_E_GERMAN");
                map.put("providerID", "d43b7711-86bd-48eb-91b3-20a588b70960");
                map.put("providerSession", "ca0d5665-ad64-445c-82a2-51f20b63a4f4");
                map.put("adminSession", "311150f0-fa42-4fa5-8591-b3d9cef490e9");
                map.put("addressCreator", "d43b7711-86bd-48eb-91b3-20a588b70960");
                map.put("base_url", "https://priapi.yuma-technology.co.uk:9030/delivery/");
                map.put("address_url", "https://priapi.yuma-technology.co.uk:9030/delivery/connector/");
                break;
            case RUPA_TANDOORI:
                map.put("business", "RUPA_TANDOORI");
                map.put("providerID", "r7033019-3dee-457b-a9da-50df238b3c9f");
                map.put("providerSession", "b0310c27-a92a-4cc8-8f5b-803cb09332fa");
                map.put("adminSession", "r7033019-3dee-457b-a9da-50df238b3c9s");
                map.put("addressCreator", "r7033019-3dee-457b-a9da-50df238b3c9f");
                map.put("base_url", "https://priapi.yuma-technology.co.uk:9028/delivery/");
                map.put("address_url", "https://priapi.yuma-technology.co.uk:9028/delivery/connector/");
                break;
        }
        return map;
    }
}
