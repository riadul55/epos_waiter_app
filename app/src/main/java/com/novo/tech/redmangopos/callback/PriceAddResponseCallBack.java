package com.novo.tech.redmangopos.callback;

public interface PriceAddResponseCallBack {
    void onPriceAddResponse(String response);
}
