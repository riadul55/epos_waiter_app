package com.novo.tech.redmangopos.callback;

import com.novo.tech.redmangopos.model.ShiftInfoOrderModel;

import java.util.ArrayList;

public interface ShiftInfoOrderCallBack {
    void onShiftInfoOrderResponse(ArrayList<ShiftInfoOrderModel> data);
}
