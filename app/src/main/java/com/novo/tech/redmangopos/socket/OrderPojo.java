package com.novo.tech.redmangopos.socket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPojo {
    @SerializedName("db_id")
    @Expose
    private Integer dbId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("server_id")
    @Expose
    private Integer serverId;
    @SerializedName("order_channel")
    @Expose
    private String orderChannel;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("orderDateTime")
    @Expose
    private String orderDateTime;
    @SerializedName("requestedDeliveryDateTime")
    @Expose
    private String requestedDeliveryDateTime;
    @SerializedName("currentDeliveryDateTime")
    @Expose
    private String currentDeliveryDateTime;
    @SerializedName("deliveryType")
    @Expose
    private String deliveryType;
    @SerializedName("order_type")
    @Expose
    private String orderType;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("dueAmount")
    @Expose
    private Double dueAmount;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("subTotal")
    @Expose
    private Double subTotal;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("discountableTotal")
    @Expose
    private Double discountableTotal;
    @SerializedName("paidTotal")
    @Expose
    private Double paidTotal;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("discountCode")
    @Expose
    private String discountCode;
    @SerializedName("plasticBagCost")
    @Expose
    private Double plasticBagCost;
    @SerializedName("containerBagCost")
    @Expose
    private Double containerBagCost;
    @SerializedName("adjustment")
    @Expose
    private Double adjustment;
    @SerializedName("adjustmentNote")
    @Expose
    private String adjustmentNote;
    @SerializedName("discountPercentage")
    @Expose
    private Integer discountPercentage;
    @SerializedName("deliveryCharge")
    @Expose
    private Double deliveryCharge;
    @SerializedName("tips")
    @Expose
    private Double tips;
    @SerializedName("shift")
    @Expose
    private Integer shift;
    @SerializedName("paymentId")
    @Expose
    private String paymentId;
    @SerializedName("cashId")
    @Expose
    private Integer cashId;
    @SerializedName("receive")
    @Expose
    private Double receive;
    @SerializedName("refund")
    @Expose
    private Double refund;
    @SerializedName("cloudSubmitted")
    @Expose
    private Integer cloudSubmitted;
    @SerializedName("fixedDiscount")
    @Expose
    private Integer fixedDiscount;
    @SerializedName("isDiscountOverride")
    @Expose
    private Integer isDiscountOverride;
    @SerializedName("customerInfo")
    @Expose
    private String customerInfo;
    @SerializedName("shippingAddress")
    @Expose
    private String shippingAddress;
    @SerializedName("customerId")
    @Expose
    private String customerId;
    @SerializedName("tableBookingId")
    @Expose
    private Integer tableBookingId;
    @SerializedName("tableBookingInfo")
    @Expose
    private String tableBookingInfo;
    @SerializedName("cartItems")
    @Expose
    private String cartItems;
    @SerializedName("transactions")
    @Expose
    private String transactions;

    public Integer getDbId() {
        return dbId;
    }

    public void setDbId(Integer dbId) {
        this.dbId = dbId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(String orderChannel) {
        this.orderChannel = orderChannel;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public String getRequestedDeliveryDateTime() {
        return requestedDeliveryDateTime;
    }

    public void setRequestedDeliveryDateTime(String requestedDeliveryDateTime) {
        this.requestedDeliveryDateTime = requestedDeliveryDateTime;
    }

    public String getCurrentDeliveryDateTime() {
        return currentDeliveryDateTime;
    }

    public void setCurrentDeliveryDateTime(String currentDeliveryDateTime) {
        this.currentDeliveryDateTime = currentDeliveryDateTime;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(Double dueAmount) {
        this.dueAmount = dueAmount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getDiscountableTotal() {
        return discountableTotal;
    }

    public void setDiscountableTotal(Double discountableTotal) {
        this.discountableTotal = discountableTotal;
    }

    public Double getPaidTotal() {
        return paidTotal;
    }

    public void setPaidTotal(Double paidTotal) {
        this.paidTotal = paidTotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public Double getPlasticBagCost() {
        return plasticBagCost;
    }

    public void setPlasticBagCost(Double plasticBagCost) {
        this.plasticBagCost = plasticBagCost;
    }

    public Double getContainerBagCost() {
        return containerBagCost;
    }

    public void setContainerBagCost(Double containerBagCost) {
        this.containerBagCost = containerBagCost;
    }

    public Double getAdjustment() {
        return adjustment;
    }

    public void setAdjustment(Double adjustment) {
        this.adjustment = adjustment;
    }

    public String getAdjustmentNote() {
        return adjustmentNote;
    }

    public void setAdjustmentNote(String adjustmentNote) {
        this.adjustmentNote = adjustmentNote;
    }

    public Integer getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Integer discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public Double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public Double getTips() {
        return tips;
    }

    public void setTips(Double tips) {
        this.tips = tips;
    }

    public Integer getShift() {
        return shift;
    }

    public void setShift(Integer shift) {
        this.shift = shift;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getCashId() {
        return cashId;
    }

    public void setCashId(Integer cashId) {
        this.cashId = cashId;
    }

    public Double getReceive() {
        return receive;
    }

    public void setReceive(Double receive) {
        this.receive = receive;
    }

    public Double getRefund() {
        return refund;
    }

    public void setRefund(Double refund) {
        this.refund = refund;
    }

    public Integer getCloudSubmitted() {
        return cloudSubmitted;
    }

    public void setCloudSubmitted(Integer cloudSubmitted) {
        this.cloudSubmitted = cloudSubmitted;
    }

    public Integer getFixedDiscount() {
        return fixedDiscount;
    }

    public void setFixedDiscount(Integer fixedDiscount) {
        this.fixedDiscount = fixedDiscount;
    }

    public Integer getIsDiscountOverride() {
        return isDiscountOverride;
    }

    public void setIsDiscountOverride(Integer isDiscountOverride) {
        this.isDiscountOverride = isDiscountOverride;
    }

    public String getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(String customerInfo) {
        this.customerInfo = customerInfo;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getTableBookingId() {
        return tableBookingId;
    }

    public void setTableBookingId(Integer tableBookingId) {
        this.tableBookingId = tableBookingId;
    }

    public String getTableBookingInfo() {
        return tableBookingInfo;
    }

    public void setTableBookingInfo(String tableBookingInfo) {
        this.tableBookingInfo = tableBookingInfo;
    }

    public String getCartItems() {
        return cartItems;
    }

    public void setCartItems(String cartItems) {
        this.cartItems = cartItems;
    }

    public String getTransactions() {
        return transactions;
    }

    public void setTransactions(String transactions) {
        this.transactions = transactions;
    }
}