package com.novo.tech.redmangopos.model;

import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomerAddress implements Serializable {
    public int dbId,customerDbId;
    public String address_uuid,type;
    public CustomerAddressProperties properties;
    public boolean primary;
    public CustomerAddress(int dbId, int customerDbId, String address_uuid, String type, boolean primary, @Nullable CustomerAddressProperties properties) {
        this.dbId = dbId;
        this.customerDbId = customerDbId;
        this.address_uuid = address_uuid;
        this.type = type;
        this.properties = properties;
        this.primary = primary;
    }
    public static CustomerAddress fromJSON(JSONObject data){
        CustomerAddressProperties properties = null;
        if(data.optJSONObject("properties")!=null){
            properties = CustomerAddressProperties.fromJSON(data.optJSONObject("properties"));
        }
        return new CustomerAddress(
                0,0,
                data.optString("address_uuid"),
                data.optString("type"),
                false,
                properties
        );
    }
    public static List<CustomerAddress> fromBackupJSON(JSONArray data){
        Log.e("addresses", data.toString());
        List<CustomerAddress> addressList = new ArrayList<CustomerAddress>();

        for (int i=0;i<data.length();i++) {
            try {
                JSONObject jsonObject = data.getJSONObject(i);

                CustomerAddressProperties properties = new CustomerAddressProperties(
                        jsonObject.optString("zip",""),
                        "United Kingdom",
                        jsonObject.optString("city",""),
                        "",
                        jsonObject.optString("state",""),
                        jsonObject.optString("building",""),
                        jsonObject.optString("street_name","")
                );

                CustomerAddress addressItem = new CustomerAddress(
                        0,0,
                        String.valueOf(jsonObject.optInt("id",0)),
                        "Shipping",
                        jsonObject.optString("type", "SECONDARY").equals("PRIMARY"),
                        properties
                );

                addressList.add(addressItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return addressList;
    }
}
