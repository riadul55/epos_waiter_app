package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.novo.tech.redmangopos.BaseApp;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.extra.GsonParser;

import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.util.AppConstant;
import com.novo.tech.redmangopos.view.fragment.CategoryAndProduct;
import com.novo.tech.redmangopos.view.fragment.Components;
import com.novo.tech.redmangopos.view.fragment.ComponentsEdit;
import com.novo.tech.redmangopos.view.fragment.OrderCartList;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.socket.emitter.Emitter;

public class OrderCreate extends AppCompatActivity implements View.OnClickListener {
    LinearLayout btnBack, btnCheckout, btnClear, btnDynamic, btnSave;
    public String category = "all";
    public Property topCategory;
    public EditText searchBox;
    public TextView textOrderSubmit;
    TableBookingModel table;

    OrderModel orderModel;
    String orderType = null, customerName = "", bookingId;
    CustomerModel customer;
    CustomerAddress shippingAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_create);
        setView();

        if (orderModel != null && orderModel.bookingId != 0 && !orderModel.order_channel.equals("ONLINE")) {
            BaseApp.webSocket.off(SocketHandler.LISTEN_MERGED_ITEMS + AppConstant.business);
            BaseApp.webSocket.on(SocketHandler.LISTEN_MERGED_ITEMS + AppConstant.business, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject jsonObject = (JSONObject) args[0];
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String _cartItems = jsonObject.optString("order_items");
                            List<CartItem> cartItemList = new ArrayList<>();

                            try {
                                JSONArray _data = new JSONArray(_cartItems.replaceAll("NaN", "0.0"));
                                for (int i = 0; i < _data.length(); i++) {
                                    cartItemList.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)), CartItem.class));
                                }
                                orderModel.items = cartItemList;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // listened items will be set here
                            //orderModel.items = ...
                            openCartFragment();
                            openProductFragment();
                        }
                    });
                }
            });
        } else {
            openCartFragment();
            openProductFragment();
        }
    }

    void setView() {
        btnBack = findViewById(R.id.orderCreateBack);
        btnCheckout = findViewById(R.id.orderCreateCheckout);
        textOrderSubmit = findViewById(R.id.textOrderSubmit);
        btnClear = findViewById(R.id.orderCreateClear);
        searchBox = findViewById(R.id.productSearchBox);
        btnDynamic = findViewById(R.id.orderCreateNewItemDynamic);
        btnSave = findViewById(R.id.orderCreateSave);


        btnBack.setOnClickListener(this);
        btnCheckout.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnDynamic.setOnClickListener(this);
        btnSave.setOnClickListener(this);


        Bundle b = getIntent().getExtras();
        bookingId = b.getString("bookingId");
        table = (TableBookingModel) b.getSerializable("table");
        customer = (CustomerModel) b.getSerializable("customerModel");
        shippingAddress = (CustomerAddress) b.getSerializable("shippingAddress");

        orderType = b.getString("orderType");
        orderModel = (OrderModel) b.getSerializable("orderModel");
        if (table == null) {
            if (orderModel != null && orderModel.tableBookingModel != null) {
                table = orderModel.tableBookingModel;
                Log.e("table==>", table.table_no + "");


                if (orderModel.bookingId != 0 && !orderModel.order_channel.equals("ONLINE")) {
                    Log.e("BookingId==>", orderModel.bookingId + "");
                    // emit for merged items
                    // call cart fragment
                    // then call category and product fragment

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                        jsonObject.put("bookingId", orderModel.bookingId + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    BaseApp.webSocket.emit(SocketHandler.EMIT_GET_ITEMS, jsonObject);
                }
            }
        }
    }

    void openCartFragment() {
        if (orderModel != null) {
            textOrderSubmit.setText("Update Order");
        }

        Bundle bundle = new Bundle();
        bundle.putString("orderType", orderType);
        bundle.putSerializable("OrderModel", orderModel);
        bundle.putString("bookingId", bookingId);
        bundle.putSerializable("table", table);
        bundle.putSerializable("customerModel", customer);
        bundle.putSerializable("shippingAddress", shippingAddress);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderCreateLeft, OrderCartList.class, bundle)
                .commit();
    }

    public void openProductFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        bundle.putSerializable("topCategory", topCategory);
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class, bundle)
                .commit();
    }

    public void replaceWithProductFragment() {
        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        bundle.putSerializable("topCategory", topCategory);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class, bundle)
                .commit();
    }

    public void openComponentEditPage(CartItem cartItem) {
        Bundle bundle = new Bundle();
        Product pro = SharedPrefManager.getProductDetails(getApplicationContext(), cartItem.uuid);
        String _productData = GsonParser.getGsonParser().toJson(pro);
        bundle.putString("product", _productData);
        bundle.putSerializable("cartItem", cartItem);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, ComponentsEdit.class, bundle)
                .commit();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        OrderCartList cartListFragment = (OrderCartList) getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
        if (id == R.id.orderCreateBack) {
            backFragment();
        } else if (id == R.id.orderCreateCheckout) {
            cartListFragment.openCheckoutPage();

        } else if (id == R.id.orderCreateVoid) {
            cartListFragment.voidOrderAlert();

        } else if (id == R.id.orderCreateNewItemDynamic) {
            cartListFragment.getDynamicProduct();

        } else if (id == R.id.orderCreateClear) {
            cartListFragment.clearCart();
            replaceWithProductFragment();
        } else if (id == R.id.orderCreateSave) {
            createUpdateOnBackPress(cartListFragment.orderModel);


        }
    }

    public void backFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.frameLayoutOrderCreateCategory);
        if (currentFragment instanceof Components) {
            backToCategory();
        } else {
            OrderCartList cartListFragment = (OrderCartList) getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderCreateLeft);
            OrderModel orderModel = cartListFragment.orderModel;
            if (orderModel.items != null) {
                if (orderModel.items.size() != 0 && orderModel.subTotal != 0) {
                    exitAlert(orderModel);
                } else {
                    finish();
                }
            } else {
                finish();
            }
        }
    }

    public void backToCategory() {

        Bundle bundle = new Bundle();
        bundle.putString("category", category);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, CategoryAndProduct.class, bundle)
                .commit();
    }

    public void setCategory(String catName) {
        category = catName;
    }

    void exitAlert(OrderModel orderModel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderCreate.this);
        builder.setTitle("Do you want to exit order?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }

    public void createUpdateOnBackPress(OrderModel orderModel) {
        orderModel.total = orderModel.subTotal;
        orderModel.total = (orderModel.subTotal - orderModel.discountAmount) + orderModel.deliveryCharge + orderModel.tips;

        if (orderModel.tableBookingModel == null) {
            orderModel.tableBookingModel = table;
        }

        if (orderModel.orderDate == null)
            orderModel.orderDateTime = DateTimeHelper.getTime();
        orderModel.db_id = SharedPrefManager.createUpdateOrder(OrderCreate.this, orderModel);
        finish();
    }


}