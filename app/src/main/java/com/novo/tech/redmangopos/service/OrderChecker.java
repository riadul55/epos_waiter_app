package com.novo.tech.redmangopos.service;

import android.content.Context;
import android.os.AsyncTask;

import com.novo.tech.redmangopos.callback.OrderCallBack;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.sync.OrderApi;

import java.util.List;

public class OrderChecker extends AsyncTask<String, Integer, List<OrderModel>> {
    OrderCallBack callBack;
    Context context;
    List<OrderModel> orders;

    public OrderChecker(Context context, OrderCallBack callBack){
        this.context = context;
        this.callBack = callBack;
    }
    @Override
    protected List<OrderModel> doInBackground(String... strings) {
        return new OrderApi(context).getOrderList("order?status=REVIEW");
    }

//    void getReviewOrdersFromServer(){
//        orders = new OrderApi(context).getOrderList("order?status=REVIEW");
//    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(List<OrderModel> orders) {
        super.onPostExecute(orders);
        callBack.onOrderResponse(orders);
    }
}
