package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

import java.io.Serializable;


public class CashEntryModel implements Serializable {
    int id,orderId;
    String entryType,comment;
    double amount;

    public CashEntryModel(int id, int orderId, String entryType, String comment, double amount) {
        this.id = id;
        this.orderId = orderId;
        this.entryType = entryType;
        this.comment = comment;
        this.amount = amount;
    }

    public static CashEntryModel fromJSON(JSONObject data){
        return new CashEntryModel(
                data.optInt("id",0),
                data.optInt("order_id",0),
                data.optString("entry_type"),
                data.optString("comment"),
                data.optDouble("amount")
        );
    }
}
