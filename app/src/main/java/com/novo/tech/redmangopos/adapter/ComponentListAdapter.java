package com.novo.tech.redmangopos.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.model.Component;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.view.fragment.OrderComponentSubCartList;

import java.util.List;
import java.util.Locale;

public class ComponentListAdapter extends RecyclerView.Adapter<ComponentListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<Component> componentList;
    ComponentSection section;
    OrderComponentSubCartList cartListFragment ;
    Component selected;
    public ComponentListAdapter(Context mCtx, ComponentSection componentSection) {
        this.mCtx = mCtx;
        this.section = componentSection;
        this.componentList = section.components;
        if(section.selectedItem != null){
            this.selected = section.selectedItem;
        }else if(componentList.size()>0){
            this.selected = componentList.get(0);
        }
        cartListFragment = (OrderComponentSubCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_components, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        Component component = componentList.get(position);
        String price = String.format(Locale.getDefault(),"%.2f",component.mainPrice);
        holder.productTitle.setText(component.shortName);
        holder.productPrice.setText("£ "+price);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        holder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFE400")));
                        onItemClick(position);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (component.productUUid.equals(selected.productUUid))
                            holder.catImage.setBackgroundColor(Color.parseColor("#DC143C"));
                        else
                            holder.catImage.setBackground(new ColorDrawable(Color.parseColor("#FFA500")));
                        selected = component;
                        notifyDataSetChanged();
                        break;
                }


                return true;
            }

        });

        if (!component.productUUid.equals(section.selectedItem.productUUid))
            holder.catImage.setBackgroundColor(Color.parseColor("#FFA500"));
        else
            holder.catImage.setBackgroundColor(Color.parseColor("#DC143C"));

    }

    void onItemClick(int position){
//        OrderComponentSubCartList cartListFragment = (OrderComponentSubCartList)((FragmentActivity)mCtx).getSupportFragmentManager().findFragmentById(R.id.orderCreateComponentsSubCart);
//        section.selectedItem = componentList.get(position);
//        cartListFragment.onComponentChange(section);
//        notifyDataSetChanged();

        section.selectedItem = componentList.get(position);

        if(componentList.get(position).subComponents != null && componentList.get(position).subComponents.size()>0){
            AlertDialog alertDialog = new AlertDialog.Builder(mCtx).create();
            LayoutInflater inflater = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = (View) inflater.inflate(R.layout.sub_component_select, null);
            TextView textTitle = view.findViewById(R.id.textTitle);
            textTitle.setText(componentList.get(position).shortName);
            RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new GridLayoutManager(mCtx,4));
            recyclerView.addItemDecoration(new MediaSpaceDecoration(5,5));
            SubComponentListAdapter adapter = new SubComponentListAdapter(mCtx, componentList.get(position).subComponents, section, new RefreshCallBack() {
                @Override
                public void onChanged() {
                    alertDialog.dismiss();
                }
            });
            recyclerView.setAdapter(adapter);
            alertDialog.setView(view);
            alertDialog.show();
        }else{
            cartListFragment.onComponentChange(section);
        }
    }


    @Override
    public int getItemCount() {
        return componentList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {

        ImageView catImage;
        TextView productTitle;
        TextView productPrice;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            catImage = itemView.findViewById(R.id.modelProductImage);
            productTitle = itemView.findViewById(R.id.modelProductTitle);
            productPrice = itemView.findViewById(R.id.modelProductPrice);
        }
    }
}
