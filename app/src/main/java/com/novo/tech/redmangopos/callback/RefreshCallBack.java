package com.novo.tech.redmangopos.callback;

public interface RefreshCallBack{
    void onChanged();
}
