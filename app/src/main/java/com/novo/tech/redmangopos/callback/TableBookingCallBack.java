package com.novo.tech.redmangopos.callback;


import com.novo.tech.redmangopos.model.TableBookingModel;

import java.util.ArrayList;

public interface TableBookingCallBack {
    void onTableBookingInfoResponse(ArrayList<TableBookingModel> data);
}
