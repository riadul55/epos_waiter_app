package com.novo.tech.redmangopos;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.socket.SocketHandler;
import com.novo.tech.redmangopos.util.AppConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class BaseApp extends Application {
    public Context context;
    public static Socket webSocket = null;
    public static List<OrderModel> orderList = new ArrayList<OrderModel>();
    public static List<TableBookingModel> bookingList = new ArrayList<TableBookingModel>();

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();


        // socket connections
        webSocket = new SocketHandler().init();
        webSocket.connect();


        webSocket.off(SocketHandler.LISTEN_SUCCESS + AppConstant.business);
        webSocket.on(SocketHandler.LISTEN_SUCCESS + AppConstant.business, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.e("listening success==>", "for pull order");
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(SocketHandler.PROVIDER_NAME, AppConstant.business);
                    jsonObject.put(SocketHandler.ORDER, "null");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                BaseApp.webSocket.emit(SocketHandler.EMIT_ORDER_UPDATE, jsonObject);
            }
        });
        webSocket.off(SocketHandler.LISTEN_GET_BOOKINGS + AppConstant.business);
        webSocket.on(SocketHandler.LISTEN_GET_BOOKINGS + AppConstant.business, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    JSONObject jsonObject = (JSONObject) args[0];
                    Log.e("bookings==>", jsonObject.toString());
                    ArrayList<TableBookingModel> bookingModels = new ArrayList<>();
                    try {
                        JSONArray jsonArray = (JSONArray) jsonObject.get("booking_list");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            bookingModels.add(TableBookingModel.fromJSON(jsonArray.getJSONObject(i)));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    bookingList = bookingModels;

                    Intent intent = new Intent(SocketHandler.LISTEN_GET_BOOKINGS);
                    intent.putExtra("booking_list", bookingModels);
                    sendBroadcast(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
