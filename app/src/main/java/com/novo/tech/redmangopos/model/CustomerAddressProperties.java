package com.novo.tech.redmangopos.model;

import org.json.JSONObject;

import java.io.Serializable;

public class CustomerAddressProperties implements Serializable {
    public String zip,country,city,street_number,state,building,street_name;

    public CustomerAddressProperties(String zip, String country, String city, String street_number, String state, String building, String street_name) {
        this.zip = zip;
        this.country = country;
        this.city = city;
        this.street_number = street_number;
        this.state = state;
        this.building = building;
        this.street_name = street_name;


    }
    public static CustomerAddressProperties fromJSON(JSONObject data){
        return  new CustomerAddressProperties(
                data.optString("zip",""),
                data.optString("country",""),
                data.optString("city",""),
                data.optString("street_number",""),
                data.optString("state",""),
                data.optString("building",""),
                data.optString("street_name","")
        );
    }
    public static CustomerAddressProperties fromBackupJSON(JSONObject data){
        return  new CustomerAddressProperties(
                data.optString("zip",""),
                data.optString("country",""),
                data.optString("city",""),
                data.optString("street_number",""),
                data.optString("state",""),
                data.optString("building",""),
                data.optString("street_name","")
        );
    }

    public static JSONObject toJSON(CustomerAddressProperties data){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt("zip",data.zip);
            jsonObject.putOpt("country",data.country);
            jsonObject.putOpt("city",data.city);
            jsonObject.putOpt("street_number","");
            jsonObject.putOpt("state",data.state);
            jsonObject.putOpt("building",data.building);
            jsonObject.putOpt("street_name",data.street_name);
        }catch (Exception e){
            e.printStackTrace();
        }
        return  jsonObject;
    }


}
