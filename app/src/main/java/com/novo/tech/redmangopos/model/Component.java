package com.novo.tech.redmangopos.model;

import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Component implements Serializable {
    public String productType,productUUid,shortName,description,relationType,relationGroup;
    public double mainPrice,subTotal;
    public boolean visible;
    public ProductProperties properties;
    public List<SubComponent> subComponents;
    public SubComponent subComponentSelected;

    public Component(String productType, String productUUid, String shortName, String description, double mainPrice, double subTotal, ProductProperties properties, String relationGroup, String relationType, boolean visible, List<SubComponent> subComponents, SubComponent subComponentSelected) {
        this.productType = productType;
        this.productUUid = productUUid;
        this.shortName = shortName;
        this.description = description;
        this.mainPrice = mainPrice;
        this.subTotal = subTotal;
        this.properties = properties;
        this.relationGroup = relationGroup;
        this.relationType = relationType;
        this.visible = visible;
        this.subComponents = subComponents;
        this.subComponentSelected = subComponentSelected;
    }
    public static Component fromJSON(JSONObject data,JSONObject parent){
        double _price = 0,subTotal = 0;
        ProductProperties properties = null;
        List<SubComponent> subComponents = new ArrayList<>();
        SubComponent selected = null;

        if(data.optJSONObject("subTotal")!=null){
            subTotal = data.optDouble("subTotal",0);
        }
        if(data.optJSONObject("price")!=null){
            _price = data.optJSONObject("price").optDouble("price",0);
            if(_price==0){
                _price = data.optJSONObject("price").optDouble("extra_price",0);
            }
        }
        if(data.optJSONObject("properties") !=null){
            properties = ProductProperties.fromJSON(data.optJSONObject("properties"));
        }
        JSONArray jsonArray = data.optJSONArray("components");
        if(jsonArray==null && parent != null){
            jsonArray = parent.optJSONArray("items");
        }
        if(jsonArray != null){
            for(int i = 0; i<jsonArray.length();i++){
                try{
                    SubComponent sub;
                    if(jsonArray.getJSONObject(i).optJSONObject("product") != null){
                        sub = SubComponent.fromJSON(jsonArray.getJSONObject(i).optJSONObject("product"));
                    }else
                        sub = SubComponent.fromJSON(jsonArray.getJSONObject(i));

                    if(sub.visible){
                        subComponents.add(sub);
                    }

                }catch (Exception e){
                    FirebaseCrashlytics.getInstance().recordException(e);
                }
            }

        }
        if(subComponents.size()>0){
            selected = subComponents.get(0);
            subTotal+=_price+ selected.price;
        }
        if(subTotal==0){
            subTotal = _price;
        }
        return new Component(
                data.optString("product_type"),
                data.optString("product_uuid"),
                data.optString("short_name"),
                data.optString("description"),
                _price,
                subTotal,
                properties,
                data.optString("relation_group"),
                data.optString("relation_type"),
                data.optBoolean("visible"),
                subComponents,
                selected
                );
    }
    public static JSONObject toJSON(Component component){
        JSONObject jsonObject= new JSONObject();
        JSONArray subArray = new JSONArray();
        JSONObject properties = null;
        if(component.properties != null){
            properties = ProductProperties.toJSON(component.properties);
        }
        component.subComponents.forEach((a)->{
            JSONObject obj = SubComponent.toJSON(a);
            subArray.put(obj);
        });


        try {
            jsonObject.putOpt("product_uuid",component.productUUid);
            jsonObject.putOpt("product_type",component.productType);
            jsonObject.putOpt("short_name",component.shortName);
            jsonObject.putOpt("description",component.description);
            jsonObject.putOpt("price",component.mainPrice);
            jsonObject.putOpt("subTotal",component.subTotal);
            jsonObject.putOpt("properties",properties);
            jsonObject.putOpt("relation_type",component.relationType);
            jsonObject.putOpt("relation_group",component.relationGroup);
            jsonObject.putOpt("visible",component.visible);
            jsonObject.putOpt("components",subArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject;
    }

}