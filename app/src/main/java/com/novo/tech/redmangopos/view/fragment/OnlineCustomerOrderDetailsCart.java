package com.novo.tech.redmangopos.view.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.adapter.SelectionItemAdapter;
import com.novo.tech.redmangopos.databinding.SplitBillLayoutBinding;
import com.novo.tech.redmangopos.extra.CashMemoHelper;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.storage.DBOrderManager;
import com.novo.tech.redmangopos.view.activity.OnlineCustomerOrderDetails;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.PaymentCartListAdapter;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

public class OnlineCustomerOrderDetailsCart extends Fragment implements View.OnClickListener {
    public TextView type,orderId,manualDiscount,subTotal,total,refund,tips,status,deliveryCharge,adjustmentAmount,adjustmentNote;
    RelativeLayout refundContainer,creditInputContainer;
    ImageView adjustmentNoteContainer;
    public TextView paySeal;
    RecyclerView recyclerViewList;
    PaymentCartListAdapter adapter;
    LinearLayout back, kitchenPrint, btnSplitBill;
    public OrderModel orderModel = new OrderModel();
    public double discountAmount,tipsAmount;
    OnlineCustomerOrderDetailsAction actionFragment;
    TextView plasticBagAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_cart, container, false);
        String data = requireArguments().getString("orderModel");
        orderModel = GsonParser.getGsonParser().fromJson(data,OrderModel.class);
        type = v.findViewById(R.id.orderDetailsCartOrderType);
        orderId = v.findViewById(R.id.orderDetailsCartOrderNumber);
        manualDiscount = v.findViewById(R.id.orderDetailsCartManualDiscount);
        subTotal = v.findViewById(R.id.orderDetailsCartSubTotal);
        tips = v.findViewById(R.id.orderDetailsCartTips);
        total = v.findViewById(R.id.orderDetailsCartTotal);
        back = v.findViewById(R.id.orderDetailsCartBack);
        status = v.findViewById(R.id.orderDetailsCartOrderStatus);
        refund = v.findViewById(R.id.orderDetailsCartRefundAmount);
        creditInputContainer = v.findViewById(R.id.creditInput);
        refundContainer = (RelativeLayout) v.findViewById(R.id.refundContainer);
        paySeal = v.findViewById(R.id.paidText);
        adjustmentAmount = v.findViewById(R.id.orderDetailsCartAdjustment);
        adjustmentNote = v.findViewById(R.id.orderDetailsCartAdjustmentNote);
        adjustmentNoteContainer = v.findViewById(R.id.iv_orderDetailsCartAdjustmentNote);
        deliveryCharge = v.findViewById(R.id.orderDetailsCartDeliveryCharge);
        recyclerViewList = v.findViewById(R.id.orderDetailsCartRecyclerView);
        orderId.setText("Order : # "+String.valueOf(orderModel.serverId));
        type.setText(orderModel.order_type);
        back.setOnClickListener(this);
        adjustmentNoteContainer.setOnClickListener(this);

        plasticBagAmount = v.findViewById(R.id.orderDetailsCartPlasticBagAmount);


        kitchenPrint = v.findViewById(R.id.orderDetailsCartKitchenPrint);
        kitchenPrint.setOnClickListener(this);
        btnSplitBill = v.findViewById(R.id.orderDetailsConsumerSplitPrint);
        btnSplitBill.setOnClickListener(this);

        //for read only purpose
        adjustmentNoteContainer.setVisibility(View.GONE);


        status.setText(orderModel.order_status);
        deliveryCharge.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.deliveryCharge));
        actionFragment = (OnlineCustomerOrderDetailsAction) ((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCenter);
       // paySeal.setText(orderModel.paymentStatus);
        loadData();
        return v;
    }

    void loadData(){
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewList.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        adapter = new PaymentCartListAdapter(getContext(), orderModel.items);
        recyclerViewList.setAdapter(adapter);

        calculateTotal();

    }
    @SuppressLint("SetTextI18n")
    public void calculateTotal(){
        discountAmount=orderModel.discountAmount;
        orderModel.subTotal = orderModel.total;
        tipsAmount=orderModel.tips;

        double subTotalAmount = 0;
        for (CartItem item : orderModel.items){
            if(item.uuid.equals("plastic-bag")){
                orderModel.plasticBagCost = item.quantity*item.price;
            }else
                subTotalAmount+=(item.subTotal*item.quantity);
        }
        orderModel.subTotal = subTotalAmount;
        orderModel.total = subTotalAmount+orderModel.deliveryCharge+orderModel.adjustmentAmount-orderModel.refundAmount-orderModel.discountAmount+orderModel.plasticBagCost;



        total.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.total));
        manualDiscount.setText("("+orderModel.discountPercentage+"%)  -£ "+String.format("%.2f", discountAmount));
        subTotal.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.subTotal));
        tips.setText("£ "+String.format(Locale.getDefault(),"%.2f", tipsAmount));
        refund.setText("-£ "+String.format(Locale.getDefault(),"%.2f", orderModel.refundAmount));
        adjustmentAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f", orderModel.adjustmentAmount));
        plasticBagAmount.setText("£ "+String.format(Locale.getDefault(), "%.2f", orderModel.plasticBagCost));
        adjustmentNote.setText(" "+orderModel.adjustmentNote);

        if(orderModel.refundAmount !=0){
            refundContainer.setVisibility(View.VISIBLE);
        }else{
            refundContainer.setVisibility(View.GONE);
        }
        if(orderModel.order_status.equals("REFUNDED")){
            paySeal.setText("REFUNDED");
        }if(orderModel.order_status.equals("CANCELLED")){
            paySeal.setText("CANCELLED");
        }else{
            paySeal.setText(orderModel.paymentStatus);
        }



    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.orderDetailsCartBack){
            ((OnlineCustomerOrderDetails)getContext()).onBackPressed();
        }else if(v.getId() == R.id.iv_orderDetailsCartAdjustmentNote){
            if(orderModel.order_status.equals("CLOSED") || orderModel.order_status.equals("REFUNDED") || orderModel.order_status.equals("CANCELLED")){
                Toast.makeText(getContext(), "You can't add adjustment at this stage of the order", Toast.LENGTH_SHORT).show();
            }else{
                creditNoteAlert();
            }
        }else if (v.getId() == kitchenPrint.getId()) {
            try {
                CashMemoHelper.printSendKitchen(orderModel, getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Toast.makeText(getContext(), "Printing...", Toast.LENGTH_LONG).show();
        } else if (v.getId() == btnSplitBill.getId()) {
//            orderModel.items.removeIf((a) -> a.uuid.equals("plastic-bag"));
//            orderModel.items.removeIf((a) -> a.uuid.equals("container"));
//            Log.e("items===>", orderModel.items.size() + "");
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(requireContext());
            LayoutInflater inflater = LayoutInflater.from(requireContext());
            SplitBillLayoutBinding binding = SplitBillLayoutBinding.inflate(inflater);
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false));
            SelectionItemAdapter adapter = new SelectionItemAdapter(getContext(), orderModel.items, new SelectionItemAdapter.OnSectionListener() {
                @Override
                public void onItemSelect(List<CartItem> items) {
                    double total = 0.0;
                    for (int i=0;i<items.size();i++) {
                        double price = 0;

                        if(!items.get(i).offered){
                            price=(items.get(i).subTotal*items.get(i).quantity);

                            if (items.get(i).extra != null) {
                                for (CartItem extraItem: items.get(i).extra) {
                                    price+=extraItem.price;
                                }
                            }

                        }
                        else {
                            price = items.get(i).total;
                        }

                        total += price;
                    }
                    DecimalFormat df = new DecimalFormat("###.##");
                    binding.textTotal.setText("Total Price: £" + df.format(total));
                }
            });
            binding.recyclerView.setAdapter(adapter);
            binding.selectionPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<CartItem> selectedItems = adapter.getSelectedItems();
                    if (selectedItems != null && selectedItems.size() > 0) {
                        try {
                            CashMemoHelper.printSendCustomer(orderModel, selectedItems);
                            Toast.makeText(getContext(), "Printing...", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "Please select items!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            builder.setTitle("Print Bill");
            builder.setView(binding.getRoot());
            android.app.AlertDialog dialog = builder.create();
            dialog.getWindow().setLayout(1000, 700);
            dialog.show();
        }
    }

    void creditNoteAlert(){
        EditText inputField = new EditText(getContext());
        inputField.setInputType(InputType.TYPE_CLASS_TEXT);
        inputField.setText(orderModel.adjustmentNote);
        AlertDialog builder = new AlertDialog.Builder(getContext())
                .setTitle("Credit Note")
                .setView(inputField)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        creditNote(inputField.getText().toString());
                    }
                })
                .setNegativeButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        builder.show();
    }
    void creditNote(String str){
        orderModel.adjustmentNote = str;
        SharedPrefManager.createUpdateOrder(getContext(),orderModel);
        calculateTotal();
    }
}