package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.util.Log;

import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;
import com.novo.tech.redmangopos.model.CustomerAddressProperties;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.ResponseModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.storage.DBCustomerManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class OrderApi {
    Context context;
    public OrderApi(Context context){
        this.context = context;
    }
    public OrderModel getOrderData(int orderId){
        OkHttpClient client = new OkHttpClient();
        OrderModel order  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+orderId)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", SharedPrefManager.getSessionToken(context))
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code() == 200){

                try {
                    String jsonData = response.body().string();

                    JSONObject jsonObject = new JSONObject(jsonData);

                    order = OrderModel.fromJSON(jsonObject);

                    if(order.bookingId != 0){
                        order.tableBookingModel = getBookingData(order.bookingId);
                    }


                    if(order.customer!=null){
                        if(order.customer.profile.phone != null)
                            if(!order.customer.profile.phone.equals("null") && !order.customer.profile.phone.isEmpty())
                                if(!order.requester_type.equals("ONE_TIME")){
                                    DBCustomerManager customerManager = new DBCustomerManager(context);
                                    int id = customerManager.customerExist(order.customer.consumer_uuid);
                                    if(id == 0){
                                        try {
                                            customerManager.addNewCustomer(order.customer, true);
                                        }catch (Exception e){
                                            Log.e("mmmmm", "getOrderData: "+ order.serverId +"e");
                                        }
                                    }else{
//                                       CustomerModel _customerModel =  CustomerApi.getCustomerData(order.customer.consumer_uuid,context);
//                                       customerManager.updateCustomer(_customerModel,true);
                                    }
                                }
                    }

                } catch (Exception e) {
                    Log.d("mmmmm", "getOrderData: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.e("mmmmm", "getOrderData: RE"+response.code());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return order;
    }
    public List<OrderModel> getOrderList(String endPoint){
        OkHttpClient client = new OkHttpClient();
        List<OrderModel> orders = new ArrayList<>();
        String token = SharedPrefManager.getSessionToken(context);
        Request request = new Request.Builder()
                .url(BASE_URL+endPoint)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession",token==null?"":token)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.d("mmmm", "getOrderList: "+response.code());
            if(response.code() == 200){
                try {
                    String jsonData = response.body().string();
                    Log.e("OrderDetails", jsonData);
                    JSONArray jsonArray = new JSONArray(jsonData);
                    for (int i = 0;i<jsonArray.length();i++){
                        OrderModel orderModel = OrderModel.fromJSON(jsonArray.optJSONObject(i));
                        orderModel = getOrderData(orderModel.serverId);

                        if(orderModel.bookingId != 0){
                            orderModel.tableBookingModel = getBookingData(orderModel.bookingId);
                        }

                        double subTotalAmount = 0;
                        for (CartItem item : orderModel.items){
                            if(item.uuid.equals("plastic-bag")){
                                orderModel.plasticBagCost = item.quantity*item.price;
                            }else
                                subTotalAmount+=(item.subTotal*item.quantity);
                        }

                        orderModel.subTotal = subTotalAmount;
                        orderModel.total = subTotalAmount+orderModel.deliveryCharge+orderModel.adjustmentAmount-orderModel.refundAmount-orderModel.discountAmount+orderModel.plasticBagCost;


                        orders.add(orderModel);
                    }
                } catch (Exception e) {
                    Log.d("mmmmm", "failedToUpdate: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.d("mmmmm", "failedToUpdate: RE "+response.code());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return orders;
    }

    ResponseModel createOrder(OrderModel order){
        JSONObject jsonObject = new JSONObject();
        JSONObject delivery = new JSONObject();
        JSONObject shipping = new JSONObject();
        JSONObject shippingProperties = new JSONObject();
        JSONObject orderProperties = new JSONObject();
        JSONObject profile = new JSONObject();
        JSONArray items = new JSONArray();
        ResponseModel responseModel = new ResponseModel(0,false,"Failed to connect ! Please check your internet connection and try again");

        try{

            for (CartItem cartItem: order.items) {
                if(cartItem.type.equals("DYNAMIC")){
                    items.put(dynamicItemToJSONOBJECT(cartItem));
                    orderProperties.put("print_"+cartItem.uuid,cartItem.printOrder);
                }else
                    items.put(itemToJSONOBJECT(cartItem));
            }
            if( order.order_type.equals("TAKEOUT")){
                if(SharedPrefManager.getPlasticBagMandatory(context)){
                    Product pro = SharedPrefManager.getPlasticBagProduct(context);
                    if(pro != null){
//                        CartItem cartItem = new CartItem(
//                                -1,pro.productUUid,pro.description,pro.shortName,new ArrayList<>(),0,0, 0,0, 1,"",pro.productType,0,false,false,""
//                        );
//                        items.put(itemToJSONOBJECT(cartItem));
                    }
                }

                if(SharedPrefManager.getTakeoutOrderDiscount(context)){
                    Product pro = SharedPrefManager.getTakeoutDiscountProduct(context);
                    if(pro != null){
//                        CartItem cartItem = new CartItem(
//                                -1,pro.productUUid,pro.description,pro.shortName,new ArrayList<>(),0,0, 0,0, 1,"",pro.productType,0,false,false,""
//                        );
//                        items.put(itemToJSONOBJECT(cartItem));
                    }
                }
            }

            profile.put("first_name","");
            profile.put("last_name","");
            profile.put("phone","");
            delivery.put("email","");

            shippingProperties.put("zip","");
            shippingProperties.put("country","");
            shippingProperties.put("city","");
            shippingProperties.put("street_number","");
            shippingProperties.put("state","");
            shippingProperties.put("building","");
            shippingProperties.put("street_name","");




            if(order.customer != null){
                if(order.customer.profile != null){
                    profile.put("first_name",order.customer.profile.first_name);
                    profile.put("last_name",order.customer.profile.last_name);
                    profile.put("phone",order.customer.profile.phone);
                    profile.put("email",order.customer.profile.email);
                }
                if(order.customer.addresses.size()>0){
                    CustomerAddressProperties properties = order.customer.addresses.get(0).properties;
                    shippingProperties.put("zip",properties.zip);
                    shippingProperties.put("country","");
                    shippingProperties.put("city",properties.city);
                    shippingProperties.put("street_number",properties.street_number);
                    shippingProperties.put("state",properties.state);
                    shippingProperties.put("building",properties.building);
                    shippingProperties.put("street_name",properties.street_name);
                }
            }

            delivery.put("profile",profile);


            shipping.put("type","INVOICE");
            shipping.put("creator_type","PROVIDER");
            shipping.put("properties",shippingProperties);


            jsonObject.put("order_type",order.order_type.toUpperCase().replaceAll("TABLE","LOCAL"));
            jsonObject.put("requested_delivery_timestamp",new SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new java.util.Date()));
            jsonObject.put("order_provider_uuid", SharedPrefManager.getUserId(context));
            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
            jsonObject.put("order_channel","WAITERAPP");
            orderProperties.put("comment",order.comments);
            jsonObject.put("requester",delivery);
            jsonObject.put("shipping_address",shipping);
            jsonObject.put("order_properties",orderProperties);
            jsonObject.put("order_items",items);

            if(order.bookingId != 0){
                jsonObject.put("booking_id",order.bookingId);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

        Log.d("order", "createOrder: "+jsonObject);

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order")
                .post(requestBody)
                .addHeader("Content-Type","application/json")
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null){
            responseModel.statusCode = response.code();
            if(response.code() == 201){
                String generatedId = response.header("Location").replaceAll("/delivery/order/","");
                order.serverId = Integer.parseInt(generatedId);
                responseModel = updateOrderStatus(order);

            }else if(response.code() == 500){
                responseModel.message = " Failed to submit order! error Message = Internal Server Error";

                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+= "error message = "+obj.optString("error_description","N/A");

                }catch (Exception e){
                    responseModel.message+= "error Message = Internal Server Error";
                }

            }else if(response.code() == 400){
                responseModel.message = " Failed to submit order!";
                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+= "error message = "+obj.optString("error_description","N/A");

                }catch (Exception e){
                    e.printStackTrace();
                }
            }else if(response.code() == 403){
                responseModel.message = " Failed to submit order!";
                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+="error message = "+ obj.optString("error_description","Forbidden");

                }catch (Exception e){
                    e.printStackTrace();
                }

            }else{
                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+="error message = "+ obj.optString("error_description","Forbidden");

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return responseModel;
    }

    ResponseModel updateOrder(OrderModel order) {
        JSONObject jsonObject = new JSONObject();
        JSONArray items = new JSONArray();
        ResponseModel responseModel = new ResponseModel(0,false,"Failed to update order! ");

        try{
//            jsonObject.put("order_id",order.serverId);
//            jsonObject.put("order_status", "REVIEW");
//            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
//            jsonObject.put("driver_provider_uuid", providerID);


            for (CartItem cartItem: order.items) {
                if(cartItem.type.equals("DYNAMIC")){
                    items.put(dynamicItemToJSONOBJECT(cartItem));
                }else
                    items.put(itemToJSONOBJECT(cartItem));
            }

            jsonObject.put("order_items",items);
        }catch (Exception e){
            e.printStackTrace();
        }
        Log.d("mmmmm", "updateOrder: "+jsonObject.toString());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+order.serverId+"/order_item")
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null) {
            responseModel.statusCode = response.code();
            if(response.code() == 202){
                responseModel.message = "Order Successfully Submitted";
                responseModel.success = true;
            }else {
                responseModel.message += "Error===>" + response.code();
            }
        } else {
            responseModel.message+=" failed to connect with server . please check your internet connection!";
        }
        return responseModel;
    }

    ResponseModel updateOrderStatus(OrderModel order){
        JSONObject jsonObject = new JSONObject();
        ResponseModel responseModel = new ResponseModel(0,false,"Failed to change order status ! ");

        try{
            jsonObject.put("order_id",order.serverId);
            jsonObject.put("order_status", "REVIEW");
            jsonObject.put("payment_type",order.paymentMethod.toUpperCase());
            jsonObject.put("driver_provider_uuid", providerID);


        }catch (Exception e){
            e.printStackTrace();
        }
        Log.d("mmmmm", "updateOrder: "+jsonObject.toString());
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(BASE_URL+"order/"+order.serverId)
                .patch(requestBody)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", providerSession)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(response != null){
            responseModel.statusCode = response.code();

            if(response.code() == 202){
                responseModel.message = "Order Successfully Submitted";
                responseModel.success = true;
            }else if(response.code() == 500){
                responseModel.message += "error Message = Internal Server Error";
            }else if(response.code() == 400){
                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+="error message = "+ obj.optString("error_description","");

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
            else if(response.code() == 403){
                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+="error message = "+ obj.optString("error_description","Forbidden");

                }catch (Exception e){
                    e.printStackTrace();
                }

            }else{
                try{
                    String jsonData = response.body().string();
                    JSONObject obj = new JSONObject(jsonData);

                    responseModel.message+="error message = "+ obj.optString("error_description","Forbidden");

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }else{
            responseModel.message+=" failed to connect with server . please check your internet connection!";
        }

        return responseModel;

    }


    JSONObject itemToJSONOBJECT(CartItem cartItem){
        JSONObject product = new JSONObject();
        try{
            JSONArray items = new JSONArray();
            for (ComponentSection section: cartItem.componentSections) {
                JSONObject obj = new JSONObject();
                obj.putOpt("product_uuid",section.selectedItem.productUUid);
                obj.putOpt("product_type","COMPONENT");
                obj.putOpt("units",1);
                items.put(obj);
            }
            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_type",cartItem.type.isEmpty()?"ITEM":cartItem.type);
            product.putOpt("units",cartItem.quantity);
            product.putOpt("comment",cartItem.comment);
            product.putOpt("items",items);
        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }
    JSONObject dynamicItemToJSONOBJECT(CartItem cartItem){
        double price = 0;
        if(cartItem.offered){
            price = cartItem.total;
        }else{
            price = cartItem.price * cartItem.quantity;
        }

        JSONObject product = new JSONObject();
        try{
            JSONArray items = new JSONArray();
            for (ComponentSection section: cartItem.componentSections) {
                JSONObject obj = new JSONObject();
                obj.putOpt("product_uuid",section.selectedItem.productUUid);
                obj.putOpt("product_type","COMPONENT");
                obj.putOpt("units",1);
                items.put(obj);
            }

            product.putOpt("product_uuid",cartItem.uuid);
            product.putOpt("product_short_name",cartItem.shortName);
            product.putOpt("product_type","DYNAMIC");
            product.putOpt("units",cartItem.quantity);
            product.putOpt("net_amount",price);
            product.putOpt("currency","GBP");
            product.putOpt("discountable",true);
            product.putOpt("comment",cartItem.comment);
            product.putOpt("items",items);



        }catch (Exception e){
            e.printStackTrace();
        }
        return product;
    }
    public TableBookingModel getBookingData(int bookingId){
        OkHttpClient client = new OkHttpClient();
        TableBookingModel tableBookingModel  = null;
        Request request = new Request.Builder()
                .url(BASE_URL+"booking/table/"+bookingId)
                .addHeader("Content-Type","application/json")
                .addHeader("ProviderSession", SharedPrefManager.getSessionToken(context))
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if(response.code() == 200){
                try {
                    String jsonData = response.body().string();
                    JSONObject jsonObject = new JSONObject(jsonData);
                    tableBookingModel = tableBookingModel.fromJSON(jsonObject);

                } catch (Exception e) {
                    Log.d("mmmmm", "getOrderData: "+e.getMessage());
                    e.printStackTrace();
                }
            }else{
                Log.e("mmmmm", "getOrderData: RE"+response.code());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tableBookingModel;
    }
}
