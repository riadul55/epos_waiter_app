package com.novo.tech.redmangopos.retrofit2;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ProductApi {
    @GET("product?show_hierarchy=true")
    Call<Object> getProducts();
}
