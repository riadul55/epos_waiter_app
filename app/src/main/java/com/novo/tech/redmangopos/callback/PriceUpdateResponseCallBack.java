package com.novo.tech.redmangopos.callback;

public interface PriceUpdateResponseCallBack {
    void onPriceUpdateResponse(String response);
}
