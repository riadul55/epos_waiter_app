package com.novo.tech.redmangopos.retrofit2;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.Query;

public interface TableApi {
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @PATCH("config/booking/table/free")
    Call<ResponseModel> freeNow(
            @Query("local_no") int localNo,
            @Query("floor_no") int floorNo,
            @Query("table_no") int tableNo,
            @Body RequestBody body);
    @PATCH("config/booking/table/busy")
    Call<ResponseModel> busyNow(
            @Query("local_no") int localNo,
            @Query("floor_no") int floorNo,
            @Query("table_no") int tableNo,
            @Body RequestBody body
    );

}
