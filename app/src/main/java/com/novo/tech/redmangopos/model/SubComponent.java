package com.novo.tech.redmangopos.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class SubComponent implements Serializable {
    public String productType,productUUid,shortName,description,relationType,relationGroup;
    public double price;
    public boolean visible;
    public ProductProperties properties;

    public SubComponent(String productType, String productUUid, String shortName, String description, double price, ProductProperties properties, String relationGroup, String relationType, boolean visible) {
        this.productType = productType;
        this.productUUid = productUUid;
        this.shortName = shortName;
        this.description = description;
        this.price = price;
        this.properties = properties;
        this.relationGroup = relationGroup;
        this.relationType = relationType;
        this.visible = visible;
    }
    public static SubComponent fromJSON(JSONObject data){
        double _price = 0;
        ProductProperties properties = null;

        if(data.optJSONObject("price")!=null){
            _price = data.optJSONObject("price").optDouble("price",0);
            if(_price==0){
                _price = data.optJSONObject("price").optDouble("extra_price",0);
            }

        }
        if(data.optJSONObject("properties") !=null){
            properties = ProductProperties.fromJSON(data.optJSONObject("properties"));
        }

        return new SubComponent(
                data.optString("product_type"),
                data.optString("product_uuid"),
                data.optString("short_name"),
                data.optString("description"),
                _price,
                properties,
                data.optString("relation_group"),
                data.optString("relation_type"),
                data.optBoolean("visible")
        );
    }
    public static JSONObject toJSON(SubComponent component){
        JSONObject jsonObject= new JSONObject();
        JSONObject properties = null;
        if(component.properties != null){
            properties = ProductProperties.toJSON(component.properties);
        }


        try {
            jsonObject.putOpt("product_uuid",component.productUUid);
            jsonObject.putOpt("product_type",component.productType);
            jsonObject.putOpt("short_name",component.shortName);
            jsonObject.putOpt("description",component.description);
            jsonObject.putOpt("price",component.price);
            jsonObject.putOpt("properties",properties);
            jsonObject.putOpt("relation_type",component.relationType);
            jsonObject.putOpt("relation_group",component.relationGroup);
            jsonObject.putOpt("visible",component.visible);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject;
    }
}
