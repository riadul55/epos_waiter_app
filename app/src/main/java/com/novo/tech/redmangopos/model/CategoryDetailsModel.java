package com.novo.tech.redmangopos.model;

import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * This class created by alamin
 */
public class CategoryDetailsModel extends ArrayList<Parcelable> {
    String product_uuid;
    String short_name;
    String description;
    String status;
    PriceModel price;
    double average_rate;
    String creator_provider_uuid;
    String language;
    boolean discountable;
    boolean visible;
    String creation_date;
    String last_update;
    public List<ProductFile> files;
    PropertiesModel properties;
    ArrayList<ComponentsModel> components;

    public CategoryDetailsModel() {
    }

    public CategoryDetailsModel(String product_uuid, String short_name, String description, String status, PriceModel price, double average_rate, String creator_provider_uuid, String language, boolean discountable, boolean visible, String creation_date, String last_update, List<ProductFile> files, PropertiesModel properties, ArrayList<ComponentsModel> components) {
        this.product_uuid = product_uuid;
        this.short_name = short_name;
        this.description = description;
        this.status = status;
        this.price = price;
        this.average_rate = average_rate;
        this.creator_provider_uuid = creator_provider_uuid;
        this.language = language;
        this.discountable = discountable;
        this.visible = visible;
        this.creation_date = creation_date;
        this.last_update = last_update;
        this.files = files;
        this.properties = properties;
        this.components = components;
    }

    public static CategoryDetailsModel fromJSON(JSONObject jsonObject) {

        if (jsonObject != null) {

            PriceModel priceModel = null;
            if ((jsonObject.optJSONObject("price")) != null) {
                JSONObject priceObj = jsonObject.optJSONObject("price");
                if (priceObj != null) {
                    String price_uid = priceObj.optString("price_uuid", "");
                    double price = priceObj.optDouble("price", 0.0);
                    String currency = priceObj.optString("currency", "");
                    String start_date = priceObj.optString("start_date", "");
                    priceModel = new PriceModel(price_uid, price, currency, start_date);
                }
            }
            List<ProductFile> files = new ArrayList<>();

            if (jsonObject.optJSONArray("files") != null) {
                JSONArray jsonArray = jsonObject.optJSONArray("files");
                for (int i = 0; i < jsonArray.length(); i++) {
                    files.add(ProductFile.fromJSON(jsonArray.optJSONObject(i)));
                }
            }

            PropertiesModel propertiesModel = null;
            JSONObject propertiesObj = jsonObject.optJSONObject("properties");
            if (propertiesObj != null) {
                String D = propertiesObj.optString("D", "");
                String V = propertiesObj.optString("V", "");
                String categories_key = propertiesObj.optString("categories_key", "");
                String G = propertiesObj.optString("G", "");
                String available = propertiesObj.optString("available", "");
                String categories = propertiesObj.optString("categories", "");
                String sort_order = propertiesObj.optString("sort_order", "");
                String N = propertiesObj.optString("sort_order", "");
                String spiced = propertiesObj.optString("spiced", "");
                String VE = propertiesObj.optString("VE", "");
                propertiesModel = new PropertiesModel(D, V, categories_key, G, available, categories, sort_order, N, spiced, VE);
            }

            ArrayList<ComponentsModel> componentsModelArrayList = new ArrayList<>();
            JSONArray componentsArr = jsonObject.optJSONArray("components");

            if (componentsArr != null) {
                for (int i = 0; i < componentsArr.length(); i++) {
                    componentsModelArrayList.add(ComponentsModel.fromJSON(componentsArr.optJSONObject(i)));
                }
            }

            return new CategoryDetailsModel(
                    jsonObject.optString("product_uuid", ""),
                    jsonObject.optString("short_name", ""),
                    jsonObject.optString("description", ""),
                    jsonObject.optString("status", ""),
                    priceModel,
                    jsonObject.optDouble("average_rate", 0.0),
                    jsonObject.optString("creator_provider_uuid", ""),
                    jsonObject.optString("language", ""),
                    jsonObject.optBoolean("discountable", true),
                    jsonObject.optBoolean("visible", true),
                    jsonObject.optString("creation_date", ""),
                    jsonObject.optString("last_update", ""),
                    files,
                    propertiesModel,
                    componentsModelArrayList
            );

        }else {
            return null;
        }

    }

    public String getProduct_uuid() {
        return product_uuid;
    }

    public void setProduct_uuid(String product_uuid) {
        this.product_uuid = product_uuid;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PriceModel getPrice() {
        return price;
    }

    public void setPrice(PriceModel price) {
        this.price = price;
    }

    public double getAverage_rate() {
        return average_rate;
    }

    public void setAverage_rate(double average_rate) {
        this.average_rate = average_rate;
    }

    public String getCreator_provider_uuid() {
        return creator_provider_uuid;
    }

    public void setCreator_provider_uuid(String creator_provider_uuid) {
        this.creator_provider_uuid = creator_provider_uuid;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isDiscountable() {
        return discountable;
    }

    public void setDiscountable(boolean discountable) {
        this.discountable = discountable;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public PropertiesModel getProperties() {
        return properties;
    }

    public void setProperties(PropertiesModel properties) {
        this.properties = properties;
    }

    public ArrayList<ComponentsModel> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<ComponentsModel> components) {
        this.components = components;
    }

  /*   private static ArrayList<PriceModel> PriceData(JSONArray jArray) {
        ArrayList<PriceModel> modelArrayList = new ArrayList<>();
        if (jArray != null) {
            for (int i=0;i<jArray.length();i++){
                try {
                    Gson g = new Gson();
                    PriceModel priceModel = g.fromJson(jArray.getString(i), PriceModel.class);
                    modelArrayList.add(priceModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return modelArrayList;
    }
    private static ArrayList<PropertiesModel> PropertiesData(JSONArray jArray) {
        ArrayList<PropertiesModel> modelArrayList = new ArrayList<>();
        if (jArray != null) {
            for (int i=0;i<jArray.length();i++){
                try {
                    Gson g = new Gson();
                    PropertiesModel propertiesModel = g.fromJson(jArray.getString(i), PropertiesModel.class);
                    modelArrayList.add(propertiesModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return modelArrayList;
    }
    private static ArrayList<ComponentsModel> ComponentData(JSONArray jArray) {
        ArrayList<ComponentsModel> modelArrayList = new ArrayList<>();
        if (jArray != null) {
            for (int i=0;i<jArray.length();i++){
                try {
                    Gson g = new Gson();
                    ComponentsModel propertiesModel = g.fromJson(jArray.getString(i), ComponentsModel.class);
                    modelArrayList.add(propertiesModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return modelArrayList;
    }
*/


}
