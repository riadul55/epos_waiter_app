package com.novo.tech.redmangopos.socket;

import android.util.Log;

import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.CustomerAddress;
import com.novo.tech.redmangopos.model.CustomerModel;
import com.novo.tech.redmangopos.model.OrderModel;
import com.novo.tech.redmangopos.model.TableBookingModel;
import com.novo.tech.redmangopos.model.TransactionModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonToOrderModel {
    public static OrderModel toModel(JSONObject object) {
        Log.e("Object===>", object.toString());
        List<CartItem> cartItemList = new ArrayList<>();
        CustomerModel customerModel = null;
        CustomerAddress shippingAddress = null;
        TableBookingModel tableBookingModel = null;
        OrderPojo order = GsonParser.getGsonParser().fromJson(object.toString(), OrderPojo.class);
        int db_id = order.getDbId();
        int order_id = order.getOrderId();
        int serverId = order.getServerId();
        String date = order.getDate();
        String orderDateTime = order.getOrderDateTime();
        String requestedDeliveryDateTime = order.getRequestedDeliveryDateTime();
        String currentDeliveryDateTime = order.getCurrentDeliveryDateTime();
        String deliveryType = order.getDeliveryType();
        String requester_type = "";
        String order_type = order.getOrderType();
        String order_status = order.getOrderStatus();
        String order_channel = order.getOrderChannel();
        String payment_method = order.getPaymentMethod();
        String payment_status = order.getPaymentStatus();
        String comments = order.getComments();
        String discountCode = order.getDiscountCode();
        int cashId = order.getCashId();
        String paymentId = order.getPaymentId();
        String _cartItems = order.getCartItems();
        int shift = order.getShift();
        double dueAmount = order.getDueAmount();
        double subTotal = order.getSubTotal();
        double total = order.getTotal();
        double discountableTotal = order.getDiscountableTotal();
        double discount = order.getDiscount();
        double adjustment = order.getAdjustment();
        double plasticBagCost = order.getPlasticBagCost();
        double containerBagCost = order.getContainerBagCost();
        String adjustmentNote = order.getAdjustmentNote();
        int discountPercentage = order.getDiscountPercentage();
        double tips = order.getTips();
        double receive = order.getReceive();
        double deliveryCharge = order.getDeliveryCharge();
        double refund = order.getRefund();
        boolean cloudSubmitted = order.getCloudSubmitted() != 0;
        boolean fixedDiscount = order.getFixedDiscount() != 0;
        boolean isDiscountOverride = order.getIsDiscountOverride() != 0;

        int bookingId = order.getTableBookingId();
        String tableBookingInfo = order.getTableBookingInfo();
        String _customerInfo = order.getCustomerInfo();
        String _shipping = order.getShippingAddress();

        try {
            customerModel = GsonParser.getGsonParser().fromJson(_customerInfo, CustomerModel.class);
            shippingAddress = GsonParser.getGsonParser().fromJson(_shipping, CustomerAddress.class);

            if (tableBookingInfo != null) {
                tableBookingModel = GsonParser.getGsonParser().fromJson(tableBookingInfo, TableBookingModel.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONArray _data = new JSONArray(_cartItems.replaceAll("NaN", "0.0"));
            for (int i = 0; i < _data.length(); i++) {
                cartItemList.add(GsonParser.getGsonParser().fromJson(String.valueOf(_data.optJSONObject(i)), CartItem.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (order_type == null) {
            order_type = "N/A";
        }

        double cashPayment = 0;
        double cardPayment = 0;
//        for (TransactionModel model : transactionList) {
//            if (model.inCash == 0) {
//                if (model.type == 4) {
//                    cardPayment -= model.amount;
//                } else if (model.type == 2) {
//                } else {
//                    cardPayment += model.amount;
//                }
//            } else {
//                if (model.type == 4) {
//                    cashPayment -= model.amount;
//                } else if (model.type == 2) {
//                } else {
//                    cashPayment += model.amount;
//                }
//            }
//        }

        OrderModel orderModel = new OrderModel(order_id, order_channel, order_type, order_status, payment_method, subTotal, total, discountableTotal, cardPayment + cashPayment, dueAmount, discount, discountCode, tips, cartItemList, orderDateTime, currentDeliveryDateTime, requestedDeliveryDateTime, deliveryType, payment_status,
                receive, db_id, date, comments, shift, cloudSubmitted, fixedDiscount,isDiscountOverride, serverId, customerModel, cashId, paymentId, deliveryCharge, discountPercentage, refund, adjustment, adjustmentNote,
                requester_type, new ArrayList<>(), plasticBagCost, containerBagCost, cashPayment, cardPayment, bookingId, tableBookingModel, shippingAddress);
        double subTotalAmount = 0;
        double _discountableTotal = 0;
        for (CartItem item : orderModel.items) {
            if (item.uuid.equals("plastic-bag")) {
                orderModel.plasticBagCost = item.quantity * item.price;
            } else if (item.uuid.equals("container")) {
                orderModel.containerBagCost = item.quantity * item.price;
            } else {
                double price = 0;
                if (!item.offered) {
                    price = (item.subTotal * item.quantity);

                    if (item.extra != null) {
                        for (CartItem extraItem : item.extra) {
                            price += extraItem.price;
                        }
                    }
                } else
                    price = item.total;

                subTotalAmount += price;

                if (item.discountable) {
                    _discountableTotal += (item.subTotal * item.quantity);
                }
            }
        }

        orderModel.subTotal = subTotalAmount;
        orderModel.discountableTotal = _discountableTotal;
        orderModel.total = subTotalAmount + orderModel.deliveryCharge + orderModel.adjustmentAmount - orderModel.refundAmount - orderModel.discountAmount + orderModel.tips + orderModel.plasticBagCost + orderModel.containerBagCost;

        if (refund >= total && total != 0) {
            orderModel.order_status = "REFUNDED";
        }
        return orderModel;
    }
}
