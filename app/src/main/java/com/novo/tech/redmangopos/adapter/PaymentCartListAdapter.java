package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.model.CartItem;
import com.novo.tech.redmangopos.model.ComponentSection;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

public class PaymentCartListAdapter extends RecyclerView.Adapter<PaymentCartListAdapter.OrderListViewHolder> implements View.OnClickListener {

    Context mCtx;
    List<CartItem> cartItems;
    double total = 0;

    public PaymentCartListAdapter(Context mCtx, List<CartItem> cartItems) {
        this.mCtx = mCtx;
        this.cartItems = cartItems;
    }

    @NonNull
    @Override
    public OrderListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_payment_cart_item, parent, false);
        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListViewHolder holder, int position) {
        CartItem item = cartItems.get(position);
        double price = item.subTotal * item.quantity;
        if(item.offered){
            price = item.total;
        }
        holder.setIsRecyclable(false);
        holder.sl.setText("x"+String.valueOf(item.quantity));
        holder.name.setText(cartItems.get(position).shortName);
        holder.price.setText("£ "+String.format(Locale.getDefault(),"%.2f",price));
        String componentName = "";
        if(item.componentSections.size()>0){
            for (ComponentSection section : item.componentSections){
                componentName+=(section.sectionValue+" : "+section.selectedItem.shortName+"\n");
            }
        }
        if(!item.comment.isEmpty()){
            if(item.componentSections.size()>0){
                componentName = componentName.substring(0,componentName.length()-2);
            }
            holder.comments.setText("Note : "+item.comment);
        }else{
            holder.comments.setVisibility(View.GONE);
        }

        if(!componentName.isEmpty()){
            holder.componentsName.setText(componentName);
        }else{
            holder.componentsName.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    @Override
    public void onClick(View v) {

    }

    static class OrderListViewHolder extends RecyclerView.ViewHolder {
        TextView sl,name,price,componentsName,comments;
        public OrderListViewHolder(View itemView) {
            super(itemView);
            sl = itemView.findViewById(R.id.paymentCartItemSL);
            name = itemView.findViewById(R.id.paymentCartItemName);
            price = itemView.findViewById(R.id.paymentCartItemPriceTotal);
            comments = itemView.findViewById(R.id.paymentCartExtendComponentsComment);
            componentsName = itemView.findViewById(R.id.paymentCartExtendComponentsName);
        }
    }
}