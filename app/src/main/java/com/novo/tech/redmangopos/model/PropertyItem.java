package com.novo.tech.redmangopos.model;

public class PropertyItem {
    public String key_name,value,display_name,description,content_type,filename,creation_date,last_update;
    public int sort_order;

    public PropertyItem(String key_name, String value, String display_name, String description, String content_type, String filename, String creation_date, String last_update, int sort_order) {
        this.key_name = key_name;
        this.value = value;
        this.display_name = display_name;
        this.description = description;
        this.content_type = content_type;
        this.filename = filename;
        this.creation_date = creation_date;
        this.last_update = last_update;
        this.sort_order = sort_order;
    }

}
