package com.novo.tech.redmangopos.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.model.OrderModel;

import java.util.List;

public class SyncFromServer extends AsyncTask<String, Integer, String> {
    RefreshCallBack callBack;
    private Context context;

    public SyncFromServer(Context context, RefreshCallBack callBack){
        this.context = context;
        this.callBack = callBack;
    }
    @Override
    protected String doInBackground(String... strings) {
        getReviewOrdersFromServer();
        return null;
    }

    void getReviewOrdersFromServer(){
        List<OrderModel> orders = new OrderApi(context).getOrderList("order?status=REVIEW&&channel=ONLINE");
        Log.d("mmmm", "getReviewOrdersFromServer: ");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callBack.onChanged();
    }
}
