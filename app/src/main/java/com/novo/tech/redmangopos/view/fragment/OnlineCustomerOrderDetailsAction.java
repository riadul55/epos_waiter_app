package com.novo.tech.redmangopos.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.ActionButtonsAdapter;
import com.novo.tech.redmangopos.callback.MyCallBack;
import com.novo.tech.redmangopos.model.OrderModel;

import java.util.Locale;

public class OnlineCustomerOrderDetailsAction extends Fragment implements MyCallBack {
    OrderModel orderModel;
    TextView totalAmount,orderDate,paymentMessage;
    public OnlineCustomerOrderDetailsCart cartListFragment;
    public OnlineCustomerOrderDetailsConsumerInfo consumerInfoFragment;
    ActionButtonsAdapter adapter;
    GridView gridView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_online_order_details_action, container, false);
        cartListFragment = (OnlineCustomerOrderDetailsCart)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsCart);
        consumerInfoFragment = (OnlineCustomerOrderDetailsConsumerInfo)((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentById(R.id.frameLayoutOrderDetailsRight);
        totalAmount = v.findViewById(R.id.orderDetailsActionsTotalAmount);
        orderDate = v.findViewById(R.id.orderDetailsActionsTotalOrderDate);
        gridView = v.findViewById(R.id.actionGridView);
        paymentMessage = v.findViewById(R.id.paymentMessage);
        orderModel = cartListFragment.orderModel;
        totalAmount.setText("£ "+String.format(Locale.getDefault(),"%.2f",(orderModel.total)));
        orderDate.setText(orderModel.orderDateTime);

        setActionView();

        return v;
    }
    void setActionView(){
        adapter = new ActionButtonsAdapter(getContext(),orderModel,this,true);
        gridView.setAdapter(adapter);
    }
    @Override
    public void voidCallBack() {

    }



    @Override
    public void OrderCallBack(OrderModel order) {


    }
}