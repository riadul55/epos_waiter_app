package com.novo.tech.redmangopos.view.activity;

import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.view.fragment.CustomerInput;
import com.novo.tech.redmangopos.view.fragment.CustomerSearch;

public class CustomerSelect extends AppCompatActivity {
    LinearLayout back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_select);
        back = (LinearLayout) findViewById(R.id.customerSelectBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
        openCustomerInputFragment();
        openCustomerSearchFragment();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    void openCustomerInputFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutCustomerInput, CustomerInput.class,null)
                .commit();
    }
    void openCustomerSearchFragment(){
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.frameLayoutCustomerSelect, CustomerSearch.class,null)
                .commit();
    }
}