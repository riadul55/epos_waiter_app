package com.novo.tech.redmangopos.callback;

import java.util.List;

public interface CustomerExportCallBack {
    void onSuccess(List<Boolean> results);
}
