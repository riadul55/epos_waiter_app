package com.novo.tech.redmangopos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.model.ComponentSection;

import java.util.List;

public class SectionListAdapter extends RecyclerView.Adapter<SectionListAdapter.ProductListViewHolder> {

    Context mCtx;
    List<ComponentSection> sectionList;

    public SectionListAdapter(Context mCtx, List<ComponentSection> sectionList) {
        this.mCtx = mCtx;
        this.sectionList = sectionList;
    }

    @NonNull
    @Override
    public ProductListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.model_section_view, parent, false);
        return new ProductListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductListViewHolder holder, int position) {
        //do something
        holder.sectionTitle.setText(sectionList.get(position).sectionName);
        ComponentListAdapter adapter = new ComponentListAdapter(mCtx,sectionList.get(position));
        holder.recyclerView.setLayoutManager(new GridLayoutManager(mCtx,4));
        holder.recyclerView.addItemDecoration(new MediaSpaceDecoration(10,5));
        holder.recyclerView.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return sectionList.size();
    }


    static class ProductListViewHolder extends RecyclerView.ViewHolder {
        TextView sectionTitle;
        RecyclerView recyclerView;

        public ProductListViewHolder(View itemView) {
            super(itemView);
            sectionTitle = itemView.findViewById(R.id.sectionViewTitle);
            recyclerView = itemView.findViewById(R.id.sectionViewComponentRecyclerView);
        }
    }
}
