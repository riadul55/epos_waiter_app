package com.novo.tech.redmangopos.view.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.LoginKeyboardAdapter;
import com.novo.tech.redmangopos.extra.DateTimeHelper;
import com.novo.tech.redmangopos.callback.RefreshCallBack;
import com.novo.tech.redmangopos.storage.DBShiftManager;
import com.novo.tech.redmangopos.storage.SharedPrefManager;
import com.novo.tech.redmangopos.sync.SyncFromServer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.novo.tech.redmangopos.util.AppConstant.BASE_URL;
import static com.novo.tech.redmangopos.util.AppConstant.providerID;
import static com.novo.tech.redmangopos.util.AppConstant.providerSession;

public class Login extends AppCompatActivity implements View.OnClickListener, RefreshCallBack {

    GridView loginKeyboard;
    EditText inputFiled;
    Button loginButton;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    final int WRITE_REQUEST_CODE = 100;
    public static int SCREEN_HEIGHT;
    public static int SCREEN_WIDTH;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        SCREEN_HEIGHT = displayMetrics.heightPixels;
        SCREEN_WIDTH = displayMetrics.widthPixels;

        double x = Math.pow(SCREEN_WIDTH/displayMetrics.xdpi,2);
        double y = Math.pow(SCREEN_HEIGHT/displayMetrics.ydpi,2);
        double screenInches = Math.sqrt(x+y);

        Log.d("debug","Screen inches : " + screenInches);


        /*if (SCREEN_HEIGHT==752 && SCREEN_WIDTH==1280)
            setContentView(R.layout.activity_login10);
        else*/
            setContentView(R.layout.activity_login);

        loginKeyboard = findViewById(R.id.loginInputGridVIew);
        inputFiled = findViewById(R.id.loginInputField);
        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        inputFiled.setShowSoftInputOnFocus(false);
        inputFiled.setFocusable(false);
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        setUpGridView();
        requestPermissions(permissions, WRITE_REQUEST_CODE);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
    private void setUpGridView() {
        String[] data = {"1", "2", "3","4", "5", "6","7", "8", "9","","0","."};
        LoginKeyboardAdapter adapter = new LoginKeyboardAdapter(Login.this,data);
        loginKeyboard.setAdapter(adapter);
    }
    public void onInput(String value){
        String str = inputFiled.getText().toString();
        if(!value.equals("")){
                str+=value;
        }else{
            if(str.length() != 0){
                str = str.substring(0,str.length()-1);
            }
        }
        inputFiled.setText(str);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == loginButton.getId()){

            if(SharedPrefManager.getSessionToken(Login.this)== null){
                login();
            }else if(inputFiled.getText().toString().equals("0000")){
                login();
            }else{
                Toast.makeText(Login.this,"Login Failed",Toast.LENGTH_LONG).show();
            }

        }
    }
    void login(){
        SharedPrefManager.setUserId(Login.this,providerID);
        SharedPrefManager.setSessionToken(Login.this,providerSession);
        openDashboard();
    }


    void openDashboard(){
        Intent intent = new Intent(Login.this, TableBookingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void onChanged() {

    }
}