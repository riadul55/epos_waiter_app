package com.novo.tech.redmangopos.view.fragment;

import static android.content.Context.INPUT_METHOD_SERVICE;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.novo.tech.redmangopos.model.Product;
import com.novo.tech.redmangopos.model.Property;
import com.novo.tech.redmangopos.model.PropertyItem;
import com.novo.tech.redmangopos.view.activity.OrderCreate;
import com.novo.tech.redmangopos.R;
import com.novo.tech.redmangopos.adapter.CategoryListAdapter;
import com.novo.tech.redmangopos.adapter.ProductListAdapter;
import com.novo.tech.redmangopos.adapter.TopCategoryListAdapter;
import com.novo.tech.redmangopos.callback.PropertyCallBack;
import com.novo.tech.redmangopos.extra.GsonParser;
import com.novo.tech.redmangopos.extra.MediaSpaceDecoration;
import com.novo.tech.redmangopos.retrofit2.DashboardApi;
import com.novo.tech.redmangopos.retrofit2.ProductApi;
import com.novo.tech.redmangopos.retrofit2.RetrofitClient;
import com.novo.tech.redmangopos.storage.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CategoryAndProduct extends Fragment implements PropertyCallBack {
    RecyclerView recyclerViewCategory;
    RecyclerView recyclerViewCategoryProduct;
    RecyclerView topCategoryRecyclerView;
    FrameLayout categoryAndProduct,componentLayout;
    OrderCartList cartListFragment;
    CategoryListAdapter categoryListAdapter;
    ProductListAdapter productListAdapter;
    TopCategoryListAdapter propertyAdapter;
    String category,productName="";
    List<Product> productList = new ArrayList<>();
    List<Product> allProducts=new ArrayList<>();
    List<Property> propertyList = new ArrayList<>();
    Property selectedProperty;
    View view;
    OrderCreate orderCreate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_category_and_product, container, false);
        categoryAndProduct = v.findViewById(R.id.frameLayoutOrderCreateCategory);
        componentLayout = v.findViewById(R.id.frameLayoutOrderCreateComponents);
        recyclerViewCategory = v.findViewById(R.id.categoryRecyclerView);
        recyclerViewCategory.setAlpha(0f);
        recyclerViewCategory.setTranslationY(50);
        recyclerViewCategory.animate().alpha(1f).translationYBy(-50).setDuration(700);

        recyclerViewCategoryProduct = v.findViewById(R.id.categoryProductRecyclerView);
        recyclerViewCategoryProduct.setAlpha(0f);
        recyclerViewCategoryProduct.setTranslationY(50);
        recyclerViewCategoryProduct.animate().alpha(1f).translationYBy(-50).setDuration(700);

        recyclerViewCategory.setLayoutManager(new GridLayoutManager(getContext(),6));
        recyclerViewCategory.addItemDecoration(new MediaSpaceDecoration(14,2));
        category = requireArguments().getString("category");
        allProducts = SharedPrefManager.getAvailableProduct(getContext());
        propertyList = SharedPrefManager.getAllProperty(getContext());
        orderCreate = ((OrderCreate) getActivity());
        loadTopCategory(v);
        setProductListView(v);
        view = v;
        orderCreate.searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                productName = s.toString();
                showSearchProduct();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        orderCreate.searchBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                productName = orderCreate.searchBox.getText().toString();
                showSearchProduct();
            }
        });
        return  v;
    }


    void showSearchProduct(){
        if(productName.isEmpty()){
            recyclerViewCategory.setVisibility(View.VISIBLE);
            orderCreate.setCategory("all");
            displayCategoryRecyclerView();
        }else{
            orderCreate.setCategory("all");
            recyclerViewCategory.setVisibility(View.GONE);
            productList = new ArrayList<>();
            allProducts.forEach((a)->{
                if(a.shortName.toUpperCase().contains(productName.toUpperCase()))
                    productList.add(a);
            });
            productListAdapter = new ProductListAdapter(getContext(), productList,true);
            recyclerViewCategoryProduct.setAdapter(productListAdapter);
        }
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recyclerViewCategory.setAdapter(null);
        recyclerViewCategoryProduct.setAdapter(null);
        topCategoryRecyclerView.setAdapter(null);

    }
    void loadTopCategory(View v){
        topCategoryRecyclerView = (RecyclerView) v.findViewById(R.id.topCategoryRecyclerView);
        topCategoryRecyclerView.setAlpha(0f);
        topCategoryRecyclerView.setTranslationY(50);
        topCategoryRecyclerView.animate().alpha(1f).translationYBy(-50).setDuration(700);
        topCategoryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        if(propertyList.size() ==0){
            loadProperty(getContext());
        }else{
            displayProperty();
        }
    }
    void displayCategoryRecyclerView(){
        selectedProperty.items.sort((a,b)->Integer.compare(a.sort_order,b.sort_order));
        categoryListAdapter = new CategoryListAdapter(getContext(), selectedProperty.items,category);
        recyclerViewCategory.setAdapter(categoryListAdapter);
        getCategoryProduct(category);
    }
    void displayProperty(){
        propertyList.sort((a,b)->{
            a.key_name=a.key_name.replaceAll("category_","").replaceAll("category","All Category");
            b.key_name=b.key_name.replaceAll("category_","").replaceAll("category","All Category");
            return a.key_name.compareTo(b.key_name);
        });

        propertyList.removeIf((obj)->obj.key_name.toLowerCase().equals("all category"));
        if(propertyList.size()!=0){
            if(orderCreate.topCategory == null)
                selectedProperty = propertyList.get(0);
            else
                selectedProperty = orderCreate.topCategory;
        }
        propertyAdapter = new TopCategoryListAdapter(getContext(), propertyList,this);
        topCategoryRecyclerView.setAdapter(propertyAdapter);
        if(selectedProperty != null){
            displayCategoryRecyclerView();
        }
    }
    void setProductListView(View v){
        productList = allProducts;
        productList.sort((a,b)->Integer.compare(a.properties.sort_order,b.properties.sort_order));
        recyclerViewCategoryProduct.setLayoutManager(new GridLayoutManager(getContext(),6));
        recyclerViewCategoryProduct.addItemDecoration(new MediaSpaceDecoration(14,5));
    }
    public void openComponentPage(Product product) {

        try {
            ((OrderCreate) getActivity()).searchBox.setText("");
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }

        Bundle bundle = new Bundle();
        String _productData = GsonParser.getGsonParser().toJson(product);
        bundle.putString("product", _productData);
        getFragmentManager().beginTransaction()
                .replace(R.id.frameLayoutOrderCreateCategory, Components.class, bundle)
                .commit();
    }
    public void getCategoryProduct(String catName){
        category = catName;
        if(catName.equals("all")){
            if(selectedProperty.items.size()>0){
                catName = selectedProperty.items.get(0).value;
            }
        }
        if(orderCreate!=null){
            orderCreate.setCategory(catName);
        }
        if(allProducts.size() ==0){
            loadProductsFromServer(getContext());
        }
        List<Product> _products = new ArrayList<>();
        for (Product prod: allProducts) {
            if(prod.properties != null){
                if(prod.properties.category != null){
                    if(prod.properties.category.contains(catName)){
                        _products.add(prod);
                    }
                }
            }
        }
        if(!catName.equals("all")){
            productList = _products;
        }
        else{
            productList = new ArrayList<>();
        }

        productList.sort((a,b)->a.shortName.compareTo(b.shortName));
        productListAdapter = new ProductListAdapter(getContext(), productList,false);
        recyclerViewCategoryProduct.setAdapter(productListAdapter);
    }
    void loadProductsFromServer(Context context) {
        if (context == null) {
            return;
        }
        Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
        ProductApi api = retrofit.create(ProductApi.class);
        Call call = api.getProducts();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                int responseCode = response.code();
                List<Product> _productList = new ArrayList<Product>();
                if(responseCode == 200){
                    try {
                        JSONArray _data = new JSONArray(new Gson().toJson(response.body()));
                        for (int i = 0;i<_data.length();i++){
                            _productList.add(Product.fromJSON(_data.getJSONObject(i)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("MMMMMMMM", "onResponse: error "+e.getMessage() );
                    }
                }
                SharedPrefManager.setAllProduct(context,_productList);
                allProducts = SharedPrefManager.getAvailableProduct(context);
                getCategoryProduct(category);
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("MMMMMMMMM", "onFailure: "+t.getMessage() );
            }
        });
    }
    void loadProperty(Context context) {
        if (context == null) {
            return;
        }
        Retrofit retrofit = new RetrofitClient(context).getRetrofit(new GsonBuilder().create());
        DashboardApi api = retrofit.create(DashboardApi.class);
        Call call = api.getProperty();
        call.enqueue(new Callback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call call, Response response) {
                int responseCode = response.code();
                List<Property> proList = new ArrayList<Property>();
                if(responseCode == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                        jsonObject.keys().forEachRemaining(str->{
                            JSONArray jsonArray = jsonObject.optJSONArray(str);
                            List<PropertyItem> propertyItems = new ArrayList<>();
                            for (int i = 0;i<jsonArray.length();i++){
                                PropertyItem _proItem = GsonParser.getGsonParser().fromJson(jsonArray.optJSONObject(i).toString(),PropertyItem.class);
                                propertyItems.add(_proItem);
                            }
                            Property prop = new Property(str,propertyItems);

                                proList.add(prop);

                        });
                        Log.e("mmmm", "onResponse: si  "+proList.size() );
                    } catch (JSONException e) {
                        Log.e("mmmm", "onResponse: "+e.getMessage() );
                        e.printStackTrace();
                    }
                }
                SharedPrefManager.setAllProperty(context,proList);
                propertyList = proList;
                displayProperty();
            }
            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("MMMMMMMMM", "onFailure: "+t.getMessage() );
            }
        });
    }

    @Override
    public void onSelect(Property property) {
        selectedProperty = property;
        category = "all";
        displayCategoryRecyclerView();
    }
}